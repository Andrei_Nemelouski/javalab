<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>Insert title here</title>
</head>
<body>
<div class="main">
	<table class="main_table" border=3>
		<tr>
			<td colspan=2 class="header"><tiles:insert attribute="header" />
			</td>
		</tr>
		<tr>
			<td class="side_bar"><tiles:insert attribute="side_bar" /></td>
			<td class="content" valign="top">
				<tiles:insert attribute="body" />
			</td>
		</tr>
		<tr>
			<td colspan=2 class="footer"><tiles:insert attribute="footer" />
			</td>
		</tr>
	</table>
</div>
</body>
</html>