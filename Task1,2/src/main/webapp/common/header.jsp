<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>

<div class="header_text">
	<b>News management</b>
</div>
<div class="header_links">
	<html:link action="/Locale.do?locale=en">English</html:link> <html:link action="/Locale.do?locale=ru">Russian</html:link>
</div>
