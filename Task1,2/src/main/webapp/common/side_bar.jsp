<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
 <%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<div class="menu">
         <div class="menu_title">
            <bean:message key="ttl.news.main" />
         </div>
         <div class="menu_content">
            <p><img class="menu_mark" src="img/ul.png" /><a class="list_link" href="/News"><bean:message key="bt.news.list"/></a></p>
            <p><img class="menu_mark" src="img/ul.png" /><a class="add_link" href="/News/Add.do"><bean:message key="bt.news.add" /> </a> </p>
            
         </div>
    </div>