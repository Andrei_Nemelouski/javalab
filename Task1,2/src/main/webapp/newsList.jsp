<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
    <%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css"
	href="${pageContext.servletContext.contextPath}/css/style.css" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>News management&gt&gt<bean:message key="pg.news.list" /></title>
</head>
<body>
<tiles:insert definition="common-layout">
<tiles:put name="body" value="/body/newsList_body.jsp"/>
</tiles:insert>
</body>
</html>