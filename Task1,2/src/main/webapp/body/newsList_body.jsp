<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<script src="js/script-messages.jsp" type="text/javascript"></script>
<script src="js/main.js" type="text/javascript"></script>
<style type="text/css">
.list_link{
color:orange;
}
</style>
<table class="content_table">
	<tr>
		<td class="content_table_capture">News&gt&gt<bean:message key="pg.news.list" /></td>
	</tr>
	<tr>
		<td class="content_table_body"><html:form action="/Delete">
				<bean:define id="bean" name="newsForm" />
				<logic:iterate name="newsForm" property="newsList" id="news">
					<table class="item_table">
						<tr>
							<td class="item_title"><bean:write name="news"
									property="title" /></td>
							<td class="item_title"><bean:write name="news"
									property="brief" /></td>
							<td class="item_date"><bean:write name="news"
									property="date" formatKey="date.news.format" /></td>
						</tr>
						<tr>
							<td colspan=3 class="item_content"><bean:write name="news"
									property="content" /></td>
						</tr>
						<tr>
							<td colspan=3 class="item_edit_view">
							<html:link action="/View" paramId="id" paramName="news" paramProperty="id">
									<bean:message key="bt.news.view" />
									
								</html:link> 
							<html:link action="/Edit?previousPage=main" paramId="id" paramName="news" paramProperty="id">
									<bean:message key="bt.news.edit" />
								</html:link> 
								<html:multibox name="newsForm" property="delete"
									value="${news.id}" /></td>
					</table>


				</logic:iterate>
				<logic:notEmpty name="newsForm" property="newsList">
				<div class="delete_button_list">
					<html:submit
						onclick="return show_alert(this.form)">
						<bean:message key="bt.news.delete" />
					</html:submit>
				</div>
				</logic:notEmpty>
			</html:form></td>
	</tr>
</table>