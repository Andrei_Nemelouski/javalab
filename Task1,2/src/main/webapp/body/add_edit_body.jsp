<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<link rel="stylesheet" type="text/css"
	href="${pageContext.servletContext.contextPath}/css/style.css" />
<script src="js/validation.js" type="text/javascript"></script>
<style type="text/css">
.add_link {
	color: orange;
}
</style>
<table class="content_table">
	<tr>
		<td class="content_table_capture">News&gt&gt<bean:message key="pg.news.add" /></td>
	</tr>
	<tr>
		<td class="content_table_body"><html:form
				onsubmit="return validateNewsForm(this)" action="/Save">

				<logic:notEmpty name="newsForm" property="newsMessage">
					<bean:define id="news_bean" name="newsForm" property="newsMessage" />
				</logic:notEmpty>
				<p class="error_text">
					<html:errors />
				</p>
				<table class="addEdit_table">
					<tr>
						<td class="addEdit_title"><bean:message key="ttl.news.title" /></td>
						<td><div class="text">
								<html:text property="title" size="80" maxlength="100"></html:text>
							</div></td>
					</tr>
					<tr>
						<td class="addEdit_title"><bean:message key="ttl.news.date" /></td>
						<td>
						<html:text property="date" size="10" maxlength="10"/></td>
					</tr>
					<tr>
						<td class="addEdit_title"><bean:message key="ttl.news.brief" /></td>
						<td><html:textarea property="brief" cols="80" rows="5" /></td>
					</tr>
					<tr>
						<td class="addEdit_title"><bean:message
								key="ttl.news.content" /></td>
						<td><html:textarea property="content" cols="80" rows="10" />
							<html:hidden property="id" /></td>
					</tr>
					<tr>
						<td colspan=2 align="center"><html:submit>
								<bean:message key="bt.news.save" />
							</html:submit>
							<html:cancel>
								<bean:message key="bt.news.cancel" />
							</html:cancel></td>
					</tr>
				</table>
			</html:form>
</table>
