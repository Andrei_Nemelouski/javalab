<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<script src="js/script-messages.jsp" type="text/javascript"></script>
<script src="js/main.js" type="text/javascript"></script>

<link rel="stylesheet" type="text/css"
	href="${pageContext.servletContext.contextPath}/css/style.css" />
<table class="content_table">
	<tr>
		<td class="content_table_capture">News &gt &gt <bean:message key="pg.news.view" /></td>
	</tr>
	<tr>
      <td class="content_table_body">
		<bean:define id="news_bean" name="newsForm" property="newsMessage" />
		<table class="view_table">
			<tr>
				<td class="view_title"><bean:message key="ttl.news.title" /></td>
				<td><bean:write name="news_bean" property="title" /></td>
			</tr>
			<tr>
				<td class="view_title"><bean:message key="ttl.news.date" /></td>
				<td><bean:write name="news_bean" property="date" formatKey="date.news.format"/></td>
			</tr>
			<tr>
				<td class="view_title"><bean:message key="ttl.news.brief" /></td>
				<td><bean:write name="news_bean" property="brief" /></td>
			</tr>
			<tr>
				<td class="view_title"><bean:message key="ttl.news.content" /></td>
				<td class="view_content"><bean:write name="news_bean" property="content" /></td>
			</tr>
			<tr>
				<td colspan=2 align="right">
					<table>
						<tr>
							<td><html:form action="/Edit">
									<html:hidden property="id" value="${news_bean.id}" />
									<html:hidden property="previousPage" value="view" />
									<html:submit>
										<bean:message key="bt.news.edit" />
									</html:submit>
								</html:form></td>
							<td><html:form action="/Delete">
									<html:hidden property="delete" value="${news_bean.id}" />
									<html:submit
										onclick="return show_confirm()">
										<bean:message key="bt.news.delete" />
									</html:submit>
								</html:form></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>