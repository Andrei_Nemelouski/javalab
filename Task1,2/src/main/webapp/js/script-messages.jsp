<%@ page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>

var DELETE = '<bean:message key='err.news.delete.required'/>';
var CONFIRM =  '<bean:message key='msg.news.delete.confirm'/>';
