package com.epam.testapp.model.builder;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.epam.testapp.model.News;
import com.epam.testapp.presentation.form.NewsForm;
import com.epam.testapp.util.DateOperator;

public class NewsBuilderDirector {
	
	private static NewsBuilder getBuilder(){
		return new NewsBuilder();
	}

	public static News construct(ResultSet rs) throws SQLException {
		NewsBuilder builder = getBuilder();
		builder.buildTitle(rs.getString(1));
		builder.buildId(rs.getInt(2));
		builder.buildDate(rs.getDate(3));
		builder.buildBrief(rs.getString(4));
		builder.buildContent(rs.getString(5));
		return builder.getResult();
	}

	public static News construct(NewsForm nf) {
		NewsBuilder builder = getBuilder();
		DateOperator dateOperator = new DateOperator();
		Date date = dateOperator.convertToDate(nf.getDate());
		builder.buildTitle(nf.getTitle());
		builder.buildId(nf.getId());
		builder.buildDate(date);
		builder.buildBrief(nf.getBrief());
		builder.buildContent(nf.getContent());
		return builder.getResult();
	}

}
