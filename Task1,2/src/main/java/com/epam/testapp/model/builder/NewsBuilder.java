package com.epam.testapp.model.builder;

import java.sql.Date;

import com.epam.testapp.model.News;

public class NewsBuilder implements INewsBuilder {

	private News news = new News();
	
	
	@Override
	public void buildTitle(String title) {
		news.setTitle(title);
		
	}

	@Override
	public void buildId(int id) {
		news.setId(id);
		
	}

	@Override
	public void buildDate(Date date) {
		news.setDate(date);
		
	}

	@Override
	public void buildBrief(String brief) {
		news.setBrief(brief);
	}

	@Override
	public void buildContent(String content) {
		news.setContent(content);
		
	}

	@Override
	public News getResult() {
		return news;
	}

	

}
