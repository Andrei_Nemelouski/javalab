package com.epam.testapp.model.builder;

import java.sql.Date;

import com.epam.testapp.model.News;

public interface INewsBuilder {

	News getResult();
	void buildTitle(String title);
	void buildId(int id);
	void buildDate(Date date);
	void buildBrief(String brief);
	void buildContent(String content);
}
