package com.epam.testapp.resources;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Messages manager.
 * 
 * @author Andy
 *
 */
public enum MessageManager {
	MANAGER;
	private ResourceBundle messageBundle;
	private final String resource = "resource.messages";

	/**
	 * Constructor. Get bundles for default locale
	 */
	private MessageManager() {
		messageBundle = ResourceBundle.getBundle(resource, Locale.getDefault());
	}

	/**
	 * Gets bundle for new locale {@code locale}
	 * 
	 * @param locale
	 *            new locale
	 */
	public void changeLocale(Locale locale) {
		messageBundle = ResourceBundle.getBundle(resource, locale);
	}

	/**
	 * Get message with {@code key} from bundle
	 * 
	 * @param key
	 *            message key
	 * @return message from bundle
	 */
	public String getMessage(String key) {
		return messageBundle.getString(key);
	}

}
