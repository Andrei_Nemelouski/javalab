package com.epam.testapp.resources;

import java.util.ResourceBundle;

/**
 * Resources manager
 * 
 * @author Andy
 *
 */
public class ResourceManager {
	/**
	 * Pages path's bundle
	 */
	private static final ResourceBundle pathBundle = ResourceBundle
			.getBundle("resource.path");
	/**
	 * JDBC driver properties bundle
	 */
	private static final ResourceBundle configBundle = ResourceBundle
			.getBundle("resource.config");

	/**
	 * Returns path by the key
	 * 
	 * @param key
	 *            path key
	 * @return path from {@code pathBundle}
	 */
	public static String getPath(String key) {
		return pathBundle.getString(key);
	}

	/**
	 * Returns configuration string by the key
	 * 
	 * @param key
	 *            config key
	 * @return configuration string from {@code configBundle}
	 */
	public static String getConfig(String key) {
		return configBundle.getString(key);
	}

}
