package com.epam.testapp.presentation.form;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.validator.ValidatorForm;

import com.epam.testapp.model.News;
import com.epam.testapp.util.CurrentDate;
import com.epam.testapp.util.DateOperator;

@SuppressWarnings("serial")
public class NewsForm extends ValidatorForm {

	private News newsMessage;
	private List<News> newsList;
	private int id;
	private String title;
	private String date;
	private String brief;
	private String content;
	private int[] delete = {};
	private String previousPage;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getBrief() {
		return brief;
	}

	public void setBrief(String brief) {
		this.brief = brief;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int[] getDelete() {
		return delete;
	}

	public void setDelete(int[] delete) {
		this.delete = delete;

	}

	public String getPreviousPage() {
		return previousPage;
	}

	public void setPreviousPage(String previousPage) {
		this.previousPage = previousPage;
	}

	public News getNewsMessage() {
		return newsMessage;
	}

	public void setNewsMessage(News newsMessage) {
		this.newsMessage = newsMessage;
	}

	public List<News> getNewsList() {
		return newsList;
	}

	public void setNewsList(List<News> newsList) {
		this.newsList = newsList;
	}

	@Override
	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {
		ActionErrors errors;

		String date = getDate();
		DateOperator dateOperator = new DateOperator();
		errors = dateOperator.validate(date);
		return errors;

	}

	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		id = 0;
		title = null;
		brief = null;
		content = null;
		delete = null;
		date = CurrentDate.get();
		
	}

}
