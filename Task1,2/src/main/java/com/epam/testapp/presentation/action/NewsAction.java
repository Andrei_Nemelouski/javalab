package com.epam.testapp.presentation.action;

import java.io.IOException;
import java.sql.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.Globals;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.MappingDispatchAction;
import org.springframework.beans.BeansException;
import com.epam.testapp.database.INewsDAO;
import com.epam.testapp.exceptions.DAOException;
import com.epam.testapp.model.News;
import com.epam.testapp.model.builder.NewsBuilderDirector;
import com.epam.testapp.presentation.form.NewsForm;
import com.epam.testapp.resources.MessageManager;
import com.epam.testapp.util.CurrentDate;
import com.epam.testapp.util.DateOperator;

public class NewsAction extends MappingDispatchAction {

	private static final Logger logger = Logger.getLogger(NewsAction.class);

	private INewsDAO dao;

	public void setNewsDao(INewsDAO newsDao) {
		this.dao = newsDao;

	}

	public ActionForward list(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		NewsForm newsForm = (NewsForm) form;
		try {
			List<News> newsList = dao.getList();
			newsForm.setNewsList(newsList);
		} catch (DAOException | BeansException e) {
			logger.error(e.getMessage());
			return mapping.findForward("error");
		}
		newsForm.reset(mapping, request);
		return mapping.findForward("success");

	}

	public ActionForward view(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		NewsForm newsForm = (NewsForm) form;
		Integer id = newsForm.getId();
		News news = getNews(newsForm, id);
		if (news != null) {
			newsForm.setNewsMessage(news);
			return mapping.findForward("success");
		} else {
			return mapping.findForward("main");
		}

	}

	public ActionForward edit(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		NewsForm newsForm = (NewsForm) form;
		Integer id = newsForm.getId();
		News news = getNews(newsForm, id);
		if (news != null) {
			fillNewsForm(newsForm, news);
			return mapping.findForward("success");
		} else {
			return mapping.findForward("main");
		}

	}

	private News getNews(NewsForm newsForm, int id) {
		List<News> newsList = newsForm.getNewsList();
		for (News n : newsList) {
			if (n.getId() == id) {
				return n;
			}
		}
		return null;
	}

	private NewsForm fillNewsForm(NewsForm newsForm, News news) {
		DateOperator dateOperator = new DateOperator();
		Date date = news.getDate();
		newsForm.setTitle(news.getTitle());
		newsForm.setDate(dateOperator.convertToString(date));
		newsForm.setBrief(news.getBrief());
		newsForm.setContent(news.getContent());
		newsForm.setNewsMessage(news);
		return newsForm;
	}

	public ActionForward delete(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		NewsForm newsForm = (NewsForm) form;
		if (newsForm.getDelete().length == 0) {
			return mapping.findForward("success");
		}
		try {
			dao.remove(newsForm.getDelete());
			newsForm.setNewsList(dao.getList());
			newsForm.reset(mapping, request);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			return mapping.findForward("error");
		}
		return mapping.findForward("success");

	}

	public ActionForward cancel(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		NewsForm newsForm = (NewsForm) form;
		String previousPage = newsForm.getPreviousPage();
		if (previousPage == null) {
			previousPage = "main";
		}
		return mapping.findForward(previousPage);

	}

	public ActionForward save(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		if (isCancelled(request)) {
			return cancel(mapping, form, request, response);
		}
		NewsForm newsForm = (NewsForm) form;
		List<News> newsList = newsForm.getNewsList();
		News news = NewsBuilderDirector.construct(newsForm);
		if (!newsList.contains(news)) {
			try {
				dao.save(news);
				newsList = dao.getList();
				newsForm.setNewsList(newsList);
				for (News n : newsList) {
					if (n.getTitle().equals(newsForm.getTitle())) {
						newsForm.setNewsMessage(n);						
					}
				}
			} catch (DAOException e) {
				logger.error(e.getMessage());
				return mapping.findForward("error");
			}
		} else {
			int lastIndex = newsList.lastIndexOf(news);
			newsForm.setNewsMessage(newsList.get(lastIndex));	
		}
		return mapping.findForward("success");

	}

	public ActionForward add(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		NewsForm newsForm = (NewsForm) form;
		newsForm.setDate(CurrentDate.get());
		return mapping.findForward("success");

	}

	public ActionForward changeLocale(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String languageId = request.getParameter("locale");
		Locale locale = new Locale(languageId);
		request.getSession().setAttribute(Globals.LOCALE_KEY, locale);
		MessageManager manager = MessageManager.MANAGER;
		manager.changeLocale(locale);
		Locale.setDefault(locale);

		return mapping.findForward("main");
	}

}
