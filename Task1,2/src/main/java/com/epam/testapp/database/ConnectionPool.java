package com.epam.testapp.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.concurrent.LinkedBlockingQueue;
import org.apache.log4j.Logger;

import com.epam.testapp.exceptions.PoolException;

public final class ConnectionPool {

	private static ConnectionPool instance;
	private LinkedBlockingQueue<Connection> conn;
	
	private static final Logger logger = Logger.getLogger(ConnectionPool.class);

	private ConnectionPool(int queueSize, String databaseUrl, String username,
			String pass) throws PoolException {
		conn = new LinkedBlockingQueue<Connection>(queueSize);
		Properties prop = new Properties();
		prop.put("user", username);
		prop.put("password", pass);
		try {
			DriverManager.registerDriver(new oracle.jdbc.OracleDriver());
			for (int i = 0; i < queueSize; i++) {
				conn.add(DriverManager.getConnection(databaseUrl, prop));
			}
		} catch (SQLException e) {
			throw new PoolException(e);
		}
		instance = this;

	}

	public static ConnectionPool getInstance() throws PoolException {
		return instance;
	}

	public Connection getConnection() throws PoolException {
		try {
			return conn.take();
		} catch (InterruptedException e) {
			throw new PoolException(e);
		}

	}

	public void releaseConnection(Connection c) throws PoolException {
		try {
			conn.put(c);
		} catch (InterruptedException e) {
			throw new PoolException(e);
		}
	}

	@SuppressWarnings("unused")
	private void destroyPool(){
		for (Connection c : conn) {
			try {
				c.close();
			} catch (SQLException e) {
				logger.error(e.getMessage());
			}

		}
	}

}
