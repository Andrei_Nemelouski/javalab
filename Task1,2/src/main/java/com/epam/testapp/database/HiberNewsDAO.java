package com.epam.testapp.database;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.epam.testapp.exceptions.DAOException;
import com.epam.testapp.model.News;
import com.epam.testapp.util.hiber.SessionFactoryManager;

public final class HiberNewsDAO implements INewsDAO {

	private SessionFactory factory = SessionFactoryManager.getSessionFactory();

	@SuppressWarnings("unchecked")
	@Override
	public List<News> getList() throws DAOException {
		Session session = factory.getCurrentSession();
		List<News> newsList;
		Transaction transaction = session.beginTransaction();
		newsList = (List<News>) session.getNamedQuery("findAll").list();
		transaction.commit();
		return newsList;
	}

	@Override
	public int save(News news) throws DAOException {
		Session session = factory.getCurrentSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			if (0 != news.getId()) {
				session.update(news);
			} else {
				session.save(news);
			}
			transaction.commit();
		} catch (HibernateException e) {
			if (null != transaction) {
				transaction.rollback();
			}
			throw new DAOException(e);
		} 
		return 1;

	}

	@Override
	public int remove(int[] id) throws DAOException {
		Session session = factory.getCurrentSession();
		Transaction transaction = null;
		int result = 0;
		try {
			transaction = session.beginTransaction();

			List<Integer> idList = new ArrayList<>();
			for (int i : id) {
				idList.add(i);				
			}
			Query query = session.getNamedQuery("remove");
			query.setParameterList("ids", idList);
			result = query.executeUpdate();
			transaction.commit();
		} catch (HibernateException e) {
			if (null != transaction) {
				transaction.rollback();
			}
			throw new DAOException(e);
		} 
		return result;
	}

	@Override
	public News fetchById(int id) throws DAOException {
		Session session = factory.getCurrentSession();
		News news = (News) session.get(News.class, id);
		
		return news;
	}	
}
