package com.epam.testapp.database;

import java.util.List;

import com.epam.testapp.exceptions.DAOException;
import com.epam.testapp.model.News;

public interface INewsDAO {

	List<News> getList() throws DAOException;
	int save(News news) throws DAOException;
	int remove(int[] id) throws DAOException;
	News fetchById(int id) throws DAOException;
	
}
