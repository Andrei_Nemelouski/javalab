package com.epam.testapp.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import com.epam.testapp.exceptions.DAOException;
import com.epam.testapp.exceptions.PoolException;
import com.epam.testapp.model.News;
import com.epam.testapp.model.builder.NewsBuilderDirector;

public final class NewsDAO implements INewsDAO {

	private ConnectionPool pool;
	private String SAVE = "INSERT INTO news (TITLE, DAY, BRIEF, CONTENT) VALUES (?,?,?,?)";
	private String UPDATE = "UPDATE news SET title=?, day=?, brief=?, content=? WHERE id=?";
	private String REMOVE = "DELETE FROM news WHERE id IN ";
	private String FETCH = "SELECT * FROM news WHERE id=?";
	private String SELECT = "SELECT * FROM news ORDER BY day DESC";

	public NewsDAO() throws DAOException {
		try {
			pool = ConnectionPool.getInstance();
		} catch (PoolException e) {
			throw new DAOException(e);
		}

	}

	@Override
	public List<News> getList() throws DAOException {
		Connection c = null;
		Statement s = null;
		List<News> list = new LinkedList<>();
		ResultSet rs = null;
		try {
			c = pool.getConnection();
			s = c.createStatement();
			rs = s.executeQuery(SELECT);
			while (rs.next()) {
				News news = NewsBuilderDirector.construct(rs);
				list.add(news);
			}
		} catch (SQLException | PoolException e) {
			throw new DAOException(e);
		} finally {
			closeStatement(s);
			closeResultSet(rs);
			releaseConnection(c);
		}
		return list;

	}

	@Override
	public int save(News news) throws DAOException {
		if (news.getId() != 0) {
			return update(news);
		} else {
			return create(news);
		}

	}

	private int create(News news) throws DAOException {
		Connection c = null;
		PreparedStatement ps = null;
		try {
			c = pool.getConnection();
			ps = c.prepareStatement(SAVE);
			ps.setString(1, news.getTitle());
			ps.setDate(2, news.getDate());
			ps.setString(3, news.getBrief());
			ps.setString(4, news.getContent());
			return ps.executeUpdate();
		} catch (SQLException | PoolException e) {
			throw new DAOException(e);
		} finally {
			releaseConnection(c);
			closeStatement(ps);
		}
	}

	private int update(News news) throws DAOException {
		Connection c = null;
		PreparedStatement ps = null;
		try {
			c = pool.getConnection();
			ps = c.prepareStatement(UPDATE);
			ps.setString(1, news.getTitle());
			ps.setDate(2, news.getDate());
			ps.setString(3, news.getBrief());
			ps.setString(4, news.getContent());
			ps.setInt(5, news.getId());
			return ps.executeUpdate();
		} catch (SQLException | PoolException e) {
			throw new DAOException(e);
		} finally {
			releaseConnection(c);
			closeStatement(ps);
		}
	}

	@Override
	public int remove(int[] id) throws DAOException {
		Connection c = null;
		PreparedStatement ps = null;
		String command = createRemoveCommand(id);
		try {
			c = pool.getConnection();
			ps = c.prepareStatement(command);
			return ps.executeUpdate();
		} catch (SQLException | PoolException e) {
			throw new DAOException(e);
		} finally {
			releaseConnection(c);
			closeStatement(ps);
		}

	}

	private String createRemoveCommand(int[] id) {
		StringBuffer sb = new StringBuffer();
		sb.append(REMOVE + "(");
		for (int i = 0; i < id.length; i++) {
			sb.append(id[i]);
			if (i != (id.length - 1)) {
				sb.append(", ");
			} else {
				sb.append(")");
			}
		}
		return sb.toString();
	}

	@Override
	public News fetchById(int id) throws DAOException {
		Connection c = null;
		PreparedStatement ps = null;
		News news = null;
		ResultSet rs = null;
		try {
			c = pool.getConnection();
			ps = c.prepareStatement(FETCH);
			ps.setInt(1, id);
			rs = ps.executeQuery();
			while (rs.next()) {
				news = NewsBuilderDirector.construct(rs);
			}
		} catch (SQLException | PoolException e) {
			throw new DAOException(e);
		} finally {
			releaseConnection(c);
			closeStatement(ps);
			closeResultSet(rs);
		}
		return news;
	}

	private void releaseConnection(Connection c) throws DAOException {
		try {
			pool.releaseConnection(c);
		} catch (PoolException e) {
			throw new DAOException(e);
		}
	}

	private void closeResultSet(ResultSet rs) throws DAOException {
		try {
			rs.close();
		} catch (SQLException e) {
			throw new DAOException(e);
		}
	}

	private void closeStatement(Statement statement) throws DAOException {
		try {
			statement.close();
		} catch (SQLException e) {
			throw new DAOException(e);
		}
	}
}
