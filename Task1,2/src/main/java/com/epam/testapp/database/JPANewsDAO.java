package com.epam.testapp.database;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.epam.testapp.exceptions.DAOException;
import com.epam.testapp.model.News;

public final class JPANewsDAO implements INewsDAO {

	private EntityManagerFactory factory;

	public JPANewsDAO() {
		this.factory = Persistence.createEntityManagerFactory("NewsJPA");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<News> getList() throws DAOException {
		EntityManager entityManager = factory.createEntityManager();
		Query query = entityManager.createNamedQuery("News.findAll");
		List<News> newsList = query.getResultList();
		closeManager(entityManager);
		return newsList;

	}

	@Override
	public int save(News news) throws DAOException {
		EntityManager entityManager = factory.createEntityManager();
		entityManager.getTransaction().begin();
		if (news.getId() != 0) {
			entityManager.refresh(news);
		} else {
			entityManager.persist(news);
		}
		entityManager.getTransaction().commit();
		closeManager(entityManager);
		return 1;
	}

	@Override
	public int remove(int[] id) throws DAOException {
		EntityManager entityManager = factory.createEntityManager();
		Query query;
		List<Integer> idList = new ArrayList<>();
		for (int i : id) {
			idList.add(i);
		}
		entityManager.getTransaction().begin();
		query = entityManager.createNamedQuery("News.remove");
		query.setParameter("ids", idList);
		query.executeUpdate();
		entityManager.getTransaction().commit();
		closeManager(entityManager);
		return 0;
	}

	@Override
	public News fetchById(int id) throws DAOException {
		EntityManager entityManager = factory.createEntityManager();
		/*Query query = entityManager.createNamedQuery("News.findById");
		query.setParameter("id", id);
		News news = (News) query.getSingleResult();*/
		News news = entityManager.find(News.class, id);
		closeManager(entityManager);
		return news;
	}

	private void closeManager(EntityManager entityManager) {
		entityManager.close();
	}

	@SuppressWarnings("unused")
	private void destroyDAO() {
		this.factory.close();
	}
}
