package com.epam.testapp.exceptions;

@SuppressWarnings("serial")
public class DAOException extends Exception {
	private Throwable cause;

	public DAOException(Throwable cause) {
		this.cause = cause;
	}

	public Throwable getCause() {
		return cause;
	}

	public String getMessage() {
		return cause.getMessage();
	}

}
