package com.epam.testapp.exceptions;

@SuppressWarnings("serial")
public class PoolException extends Exception {
	private Throwable cause;

	public PoolException(Throwable cause) {
		this.cause = cause;
	}

	public Throwable getCause() {
		return cause;
	}

	public String getMessage() {
		return cause.getMessage();
	}
}
