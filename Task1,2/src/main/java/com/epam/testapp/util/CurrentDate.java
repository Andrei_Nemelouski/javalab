package com.epam.testapp.util;

import java.sql.Date;
import java.util.Calendar;

public class CurrentDate {

	public static String get() {
		DateOperator dateOperator = new DateOperator();
		long c = Calendar.getInstance().getTimeInMillis();
		Date d = new Date(c);
		return dateOperator.convertToString(d);
	}

}
