package com.epam.testapp.util.hiber;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class SessionFactoryManager {
	private static final Logger logger = Logger.getLogger(SessionFactoryManager.class);

	private static final SessionFactory sessionFactory;
	
	static {
		try {
			sessionFactory = new Configuration().configure()
					.buildSessionFactory();
		} catch (Throwable e) {
			logger.error(e.getMessage());
			throw new ExceptionInInitializerError(e);
		}
	}

	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	@SuppressWarnings("unused")
	private void destroyFactoryManager() {
		sessionFactory.close();
	}
}