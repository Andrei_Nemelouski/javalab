package com.epam.testapp.util;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Locale;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMessage;

import com.epam.testapp.resources.MessageManager;

public class DateOperator {

	ActionErrors errors;
	MessageManager manager = MessageManager.MANAGER;
	String sqlFormat = "yyyy-MM-dd";

	public ActionErrors validate(String date) {
		errors = new ActionErrors();
		String currentFormat = Locale.getDefault().getLanguage();
		System.out.println(currentFormat);
		String[] s = date.split("/");
		for (int i = 0; i < s.length; i++) {
			Integer d = Integer.parseInt(s[i]);
			if (i == 2) {
				if (d < 1970) {
					errors.add("error.year.invalid", new ActionMessage(
							"err.news.date.year.invalid"));
				}
			} else {
				if (currentFormat.equals(Locale.ENGLISH.toString())) {
					if (i == 0) {
						if (d > 12) {
							errors.add("error.month.invalid",
									new ActionMessage(
											"err.news.date.month.invalid"));
						}

					} else {
						if (d > 31) {
							errors.add("error.day.invalid", new ActionMessage(
									"err.news.date.day.invalid"));
						}

					}
				} else {
					if (i == 1) {
						if (d > 12) {
							errors.add("error.month.invalid",
									new ActionMessage(
											"err.news.date.month.invalid"));
						}

					} else {
						if (d > 31) {
							errors.add("error.day.invalid", new ActionMessage(
									"err.news.date.day.invalid"));
						}

					}
				}
			}
		}
		return errors;

	}

	public String convertToString(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat(
				manager.getMessage("date.news.format"));
		return sdf.format(date);

	}

	public Date convertToDate(String date) {
		String locale = Locale.getDefault().getLanguage();
		String[] s = date.split("/");
		StringBuffer sb = new StringBuffer(10);
		if (locale.equals(Locale.ENGLISH.toString())) {
			sb.append(s[2] + '-');
			sb.append(s[0] + '-' + s[1]);
		} else {
			sb.append(s[2] + '-' + s[1] + '-' + s[0]);
		}
		String dateString = sb.toString();
		System.out.println(dateString);
		return Date.valueOf(dateString);
	}

}
