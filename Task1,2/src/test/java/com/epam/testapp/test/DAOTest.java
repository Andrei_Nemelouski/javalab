package com.epam.testapp.test;

import java.sql.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import com.epam.testapp.database.NewsDAO;
import com.epam.testapp.exceptions.DAOException;
import com.epam.testapp.model.News;

public class DAOTest {
	
	@Test
	public void saveTest() {
		NewsDAO dao;
		int expected = 1;
		int actual = 0;
		try {
			dao = new NewsDAO();
			News news = new News();
			news.setTitle("Test444");
			news.setDate(Date.valueOf("2013-11-10"));
			news.setBrief("testtt");
			news.setContent("test");			
			actual = dao.save(news);
		} catch (DAOException e) {
			System.out.println("EXCEPTION!!!!");
		}
	
		Assert.assertEquals(expected, actual);
				
	}
	@Test 
	public void removeTest() throws DAOException{
		NewsDAO dao = new NewsDAO();
		int expected = 1;
		int actual = 0;
		List<News> news = dao.getList();
		for(News n:news){
			if("Test444".equals(n.getTitle())){
				int[] delete = {n.getId()};
				actual = dao.remove(delete);
			}
		}
		Assert.assertEquals(expected, actual);
	} 
	
	@Ignore
	@Test 
	public void fetchTest() throws DAOException{
		NewsDAO dao = new NewsDAO();
		News news = dao.fetchById(1);
		Assert.assertNotNull(news);
	}
	
	@Test
	public void getListTest() throws DAOException{
		NewsDAO dao = new NewsDAO();
		List<News> news = dao.getList();
		Assert.assertNotNull(news);
	}

}
