package com.epam.testapp.test;

import org.junit.Assert;
import org.junit.Test;

import java.sql.Connection;
import com.epam.testapp.database.ConnectionPool;
import com.epam.testapp.exceptions.PoolException;

public class ConnectionPoolTest {
	
	@Test
	public void getConnectionTest() throws PoolException{
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection conn = pool.getConnection();
		boolean expected = true;
		boolean actual = conn!=null;
		Assert.assertEquals(expected,actual);
	}
	
	
}
