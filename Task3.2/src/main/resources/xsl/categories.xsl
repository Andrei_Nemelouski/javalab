<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="products">
		<html>
			<body>
				<xsl:apply-templates />
			</body>
		</html>
	</xsl:template>

	<xsl:template match="/products/category">
		<xsl:call-template name="category-link">
			<xsl:with-param name="category-name">
				<xsl:value-of select="./@name" />
			</xsl:with-param>
			<xsl:with-param name="quantity">
				<xsl:value-of select="count(./*/*)"></xsl:value-of>
			</xsl:with-param>
		</xsl:call-template>

	</xsl:template>

	<xsl:template name="category-link">
		<xsl:param name="category-name"></xsl:param>
		<xsl:param name="quantity"></xsl:param>
		<form action="/Shop/contServ" method="post">
			<input type="hidden" name="action" value="showSubcat" />
			<input type="hidden" name="category" value="{$category-name}" />
			<input type="submit" value="{$category-name}  {$quantity}" />
		</form>

	</xsl:template>
</xsl:stylesheet>