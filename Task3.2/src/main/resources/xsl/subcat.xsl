<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:param name="category" />
	<xsl:template match="/products">
		<html>
			<body>
				<xsl:apply-templates/>
				<form action="/Shop/contServ" method="post">
					<input type="hidden" name="action" value="showCategories" />
					<input type="submit" value="back" />
				</form>
			</body>
		</html>
	</xsl:template>

	<xsl:template match="/products/category">
		<xsl:if test="./@name=$category">
			<xsl:apply-templates />
		</xsl:if>
	</xsl:template>

	<xsl:template match="/products/category/subcategory">
		<xsl:call-template name="subcat-link">
			<xsl:with-param name="subcat-name">
				<xsl:value-of select="./@name" />
			</xsl:with-param>
			<xsl:with-param name="quantity">
				<xsl:value-of select="count(./*)"></xsl:value-of>
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>

	<xsl:template name="subcat-link">
		<xsl:param name="subcat-name"></xsl:param>
		<xsl:param name="quantity"></xsl:param>
		<form action="/Shop/contServ" method="post">
			<input type="hidden" name="action" value="showProducts" />
			<input type="hidden" name="subcat" value="{$subcat-name}" />
			<input type="hidden" name="category" value="{$category}" />
			<input type="submit" value="{$subcat-name}   {$quantity}" />
		</form>
	</xsl:template>
</xsl:stylesheet>