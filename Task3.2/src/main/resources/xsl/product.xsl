<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:param name="subcat" />
	<xsl:param name="category" />

	<xsl:template match="/products">
		<html>
			<body>
				<xsl:apply-templates />
				<form action="/Shop/contServ" method="post">
					<input type="hidden" name="action" value="showAddPage" />
					<input type="hidden" name="subcat" value="{$subcat}" />
					<input type="hidden" name="category" value="{$category}" />
					<input type="submit" value="add" />
				</form>
				<form action="/Shop/contServ" method="post">
					<input type="hidden" name="action" value="showSubcat" />
					<input type="hidden" name="category" value="{$category}" />
					<input type="submit" value="back" />
				</form>
			</body>
		</html>
	</xsl:template>

	<xsl:template match="products/category/subcategory">
		<xsl:if test="./@name=$subcat">
			<xsl:apply-templates/>
		</xsl:if>
	</xsl:template>

	<xsl:template match="products/category/subcategory/product">
		<table border="1">
			<tr>
				<td colspan="3">
					<xsl:value-of select="./@name"></xsl:value-of>
				</td>
			</tr>
			<tr>
				<td>
					<xsl:value-of select="producer"></xsl:value-of>
				</td>
				<td>
					<xsl:value-of select="model"></xsl:value-of>
				</td>
				<td>
					<xsl:value-of select="dateOfIssue"></xsl:value-of>
				</td>
			</tr>
			<tr>
				<td>
					<xsl:value-of select="color"></xsl:value-of>
				</td>
				<td>
					<xsl:value-of select="concat(price,' ',price/@currency)" />
				</td>
			</tr>
		</table>
	</xsl:template>
</xsl:stylesheet>