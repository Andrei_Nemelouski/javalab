<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:validator="xalan://com.epam.shop.validation.Validator"
	xmlns:product="xalan://com.epam.shop.model.Product"
	exclude-result-prefixes="product validator">


	<xsl:param name="target-subcat" />
	<xsl:param name="category" />
	<xsl:param name="product" />
	<xsl:param name="validator" />

	<xsl:template match="/">
		<xsl:choose>
			<xsl:when test="validator:validate($validator, $product)">
				<xsl:call-template name="save" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="add" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="add">
		<head>
			<script >
				function agreeForm(f) {
				if (f.unavailable.checked){ f.price.disabled = 1; f.currency.disabled = 1;}
				else {f.price.disabled = 0; f.currency.disabled = 0;}	
				}			
			</script>
		</head>
		<html>
			<body>
				<h1>Add product</h1>
				<form action="/Shop/contServ" method="post">
					<p>
						Name:
						<xsl:variable name="name" select="product:getName($product)" />
						<input type="text" name="name" value="{$name}" />
					</p>
					<p>
						Producer:
						<xsl:variable name="producer" select="product:getProducer($product)" />
						<input type="text" name="producer" value="{$producer}" />
					</p>
					<p>
						Model:
						<xsl:variable name="model" select="product:getModel($product)" />
						<input type="text" name="model" value="{$model}" />
					</p>
					<p>
						Date of issue:
						<xsl:variable name="date" select="product:getDate($product)" />
						<input type="text" name="date" value="{$date}" />
					</p>
					<p>
						Color:
						<xsl:variable name="color" select="product:getColor($product)" />
						<input type="text" name="color" value="{$color}" />
					</p>
					<p>
						<input type="checkbox" name="unavailable" onclick="agreeForm(this.form)" />
						Not in stock
					</p>

					<p>
						Price:
						<xsl:variable name="price" select="product:getPrice($product)" />
						<input type="text" name="price" value="{$price}"/>
					</p>
					<p>
						Currency:
						<select size="1" name="currency" >
							<option value="USD" selected="true">USD</option>
							<option value="EUR">EUR</option>
							<option value="BYR">BYR</option>
						</select>
					</p>
					<p>
						<xsl:value-of select="validator:getMessages($validator)"></xsl:value-of>
					</p>
					<input type="hidden" name="action" value="save" />
					<input type="hidden" name="subcat" value="{$target-subcat}" />
					<input type="hidden" name="category" value="{$category}" />
					<input type="hidden" name="product" value="{$product}" />
					<input type="hidden" name="validator" value="{$validator}" />
					<input type="submit" value="Save" />
				</form>
				<form action="/Shop/contServ" method="post">
					<input type="hidden" name="action" value="showProducts" />
					<input type="hidden" name="subcat" value="{$target-subcat}" />
					<input type="hidden" name="category" value="{$category}" />
					<input type="submit" value="cancel" />

				</form>
			</body>
		</html>
	</xsl:template>

	<xsl:template name="save">
		<!-- <xsl:apply-templates select="/nsm:products"></xsl:apply-templates> -->
		<xsl:apply-templates></xsl:apply-templates>
	</xsl:template>

	<!-- <xsl:template match="/nsm:products"> <products xmlns="http://www.epam.com/shop" 
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.epam.com/shop 
		../xsd/shop.xsd "> <xsl:apply-templates /> </products> </xsl:template> <xsl:template 
		match="/nsm:products/nsm:category"> <xsl:choose> <xsl:when test="./@name=$category"> 
		<category name="{$category}"> <xsl:apply-templates /> </category> </xsl:when> 
		<xsl:otherwise> <xsl:variable name="name" select="./@name" /> <category name="{$name}"> 
		<xsl:apply-templates /> </category> <xsl:copy-of copy-namespaces="no" select="."></xsl:copy-of> 
		</xsl:otherwise> </xsl:choose> </xsl:template> <xsl:template match="/nsm:products/nsm:category/nsm:subcategory"> 
		<xsl:choose> <xsl:when test="./@name=$target-subcat"> <xsl:variable name="name" 
		select="product:getName($product)" /> <xsl:variable name="currency" select="product:getCurrency($product)" 
		/> <subcategory name="{$target-subcat}"> <product name="{$name}"> <producer> 
		<xsl:value-of select="product:getProducer($product)" /> </producer> <model> 
		<xsl:value-of select="product:getModel($product)" /> </model> <dateOfIssue> 
		<xsl:value-of select="product:getDate($product)" /> </dateOfIssue> <color> 
		<xsl:value-of select="product:getColor($product)" /> </color> <price currency="{$currency}"> 
		<xsl:value-of select="product:getPrice($product)" /> </price> </product> 
		<xsl:apply-templates /> </subcategory> </xsl:when> <xsl:otherwise> <xsl:variable 
		name="name" select="./@name" /> <subcategory name="{$name}"> <xsl:apply-templates 
		/> </subcategory> <xsl:copy-of copy-namespaces="no" select="."></xsl:copy-of> 
		</xsl:otherwise> </xsl:choose> </xsl:template> <xsl:template match="/nsm:products/nsm:category/nsm:subcategory/nsm:product"> 
		<xsl:variable name="name" select="./@name" /> <xsl:variable name="currency" 
		select="./nsm:price/@currency" /> <product name="{$name}"> <producer> <xsl:value-of 
		select="./nsm:producer" /> </producer> <model> <xsl:value-of select="./nsm:model" 
		/> </model> <dateOfIssue> <xsl:value-of select="./nsm:dateOfIssue" /> </dateOfIssue> 
		<color> <xsl:value-of select="./nsm:color" /> </color> <price currency="{$currency}"> 
		<xsl:value-of select="./nsm:price" /> </price> </product> <xsl:copy-of copy-namespaces="no" 
		select="."></xsl:copy-of> </xsl:template> -->

	<xsl:template match="node()|@*">
		<xsl:copy>
			<xsl:apply-templates select="node()|@*" />
		</xsl:copy>
	</xsl:template>

	<xsl:template
		match="/products/category[@name=$category]/subcategory[@name=$target-subcat]">
		<xsl:copy>
			<xsl:apply-templates select="node()|@*" />
			<!-- <xsl:element name="product"> <xsl:attribute name="name"><xsl:value-of 
				select="product:getName($product)" /></xsl:attribute> <xsl:element name="producer"> 
				<xsl:value-of select="product:getProducer($product)" /> </xsl:element> <xsl:element 
				name="model"> <xsl:value-of select="product:getModel($product)" /> </xsl:element> 
				<xsl:element name="dateOfIssue"> <xsl:value-of select="product:getDate($product)" 
				/> </xsl:element> <xsl:element name="color"> <xsl:value-of select="product:getColor($product)" 
				/> </xsl:element> <xsl:element name="price"> <xsl:attribute name="currency"><xsl:value-of 
				select="product:getCurrency($product)" /></xsl:attribute> <xsl:value-of select="product:getPrice($product)" 
				/> </xsl:element> </xsl:element> -->
			<xsl:variable name="name" select="product:getName($product)" />
			<xsl:variable name="currency" select="product:getCurrency($product)" />
			<product name="{$name}">
				<producer>
					<xsl:value-of select="product:getProducer($product)" />
				</producer>
				<model>
					<xsl:value-of select="product:getModel($product)" />
				</model>
				<dateOfIssue>
					<xsl:value-of select="product:getDate($product)" />
				</dateOfIssue>
				<color>
					<xsl:value-of select="product:getColor($product)" />
				</color>
				<price currency="{$currency}">
					<xsl:value-of select="product:getPrice($product)" />
				</price>
			</product>
		</xsl:copy>
	</xsl:template>

</xsl:stylesheet>