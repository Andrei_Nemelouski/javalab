package com.epam.shop.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.epam.shop.model.Product;

public class Validator {

	private final String DATE_PATTERN = "(\\d){2}-(\\d){2}-(\\d){4}";
	private final String PRICE_PATTERN = "(\\d)+";
	private final String NOT_IN_STOCK = "Not in stock";
	private StringBuffer messages = new StringBuffer();
	private boolean valid = true;

	public boolean validate(Product product) {
		isStringValid(product.getName(), "name");
		isDateValid(product.getDate());
		isPriceValid(product.getPrice());
		isStringValid(product.getProducer(), "producer");
		isStringValid(product.getModel(), "model");
		isStringValid(product.getName(), "color");
		return valid;
	}

	private void isStringValid(String test, String fieldName) {
		if (test == null || test.isEmpty()) {
			messages.append("Invalid field " + fieldName + ". ");
			setValid(false);
		}
	}

	private void isPriceValid(String price) {
		if (price == null || price.isEmpty()) {
			messages.append("Invalid price.");
		} else {
			if (!price.equals(NOT_IN_STOCK)) {
				Pattern p = Pattern.compile(PRICE_PATTERN);
				Matcher m = p.matcher(price);
				if (m.matches()) {
					Integer d = Integer.parseInt(price);
					if (d <= 0) {
						messages.append("Invalid price.");
					}
				} else {
					messages.append("Invalid price.");
				}
			}
		}
	}

	private void isDateValid(String date) {
		if (date == null || date.isEmpty()) {
			messages.append("Date must be in dd-MM-YYYY format. ");
			setValid(false);
		} else {
			Pattern p = Pattern.compile(DATE_PATTERN);
			Matcher m = p.matcher(date);
			if (m.matches()) {
				dateValidator(date);
			} else {
				messages.append("Date must be in dd-MM-YYYY format. ");
				setValid(false);
			}
		}
	}

	private void dateValidator(String date) {
		String[] s = date.split("-");
		for (int i = 0; i < s.length; i++) {
			Integer d = Integer.parseInt(s[i]);
			if (i == 2) {
				if (d < 1970) {
					messages.append("Year must be >1970.");
					setValid(false);
				}
			} else {
				if (i == 1) {
					if (d > 12) {
						messages.append("Invalid month value.");
						setValid(false);
					}

				} else {
					if (d > 31) {
						messages.append("Invalid day value.");
						setValid(false);
					}

				}
			}
		}
	}

	public String getMessages() {
		return messages.toString();
	}

	public boolean isValid() {
		return valid;
	}

	private void setValid(boolean valid) {
		this.valid = valid;
	}

	@Override
	public String toString() {
		return "Validator [messages=" + messages + ", valid=" + valid + "]";
	}

}
