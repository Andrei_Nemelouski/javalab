package com.epam.shop.command.impl;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.Transformer;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;

import com.epam.shop.command.ICommand;
import com.epam.shop.controller.RequestResponseContent;
import com.epam.shop.exception.TransformException;
import com.epam.shop.transform.TransformHandler;

public class SubcatCommand implements ICommand {

	private static final Logger logger = Logger.getLogger(ICommand.class);

	@Override
	public void execute(RequestResponseContent content) {
		String category = content.getParameter("category")[0];
		HttpServletResponse response = content.getResponse();
		TransformHandler handler = TransformHandler.getInstance();
		try {
			Transformer transformer = handler.getTransformer("subcat");
			transformer.setParameter("category", category);
			handler.transform(transformer,
					new StreamResult(response.getOutputStream()));
		} catch (IOException | TransformException e) {
			logger.error(e.getMessage());
			///send error
		}

	}

}
