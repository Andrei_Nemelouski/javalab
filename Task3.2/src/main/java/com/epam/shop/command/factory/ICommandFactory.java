package com.epam.shop.command.factory;

import com.epam.shop.command.ICommand;

public interface ICommandFactory {
	
	public ICommand getCommand(String command);

}
