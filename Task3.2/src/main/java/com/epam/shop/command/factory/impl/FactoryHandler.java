package com.epam.shop.command.factory.impl;

import com.epam.shop.command.factory.ICommandFactory;

public class FactoryHandler {
	
	private ICommandFactory factory;
	private static FactoryHandler instance;
	
	private FactoryHandler(){ 
		instance = this;
	}
		
	public static FactoryHandler getInstance(){
		return instance;
	}
	
	public ICommandFactory getFactory() {
		return factory;
	}

	public void setICommandFactory(ICommandFactory factory) {
		this.factory = factory;
	}
	
	

}
