package com.epam.shop.command.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.Transformer;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;

import com.epam.shop.command.ICommand;
import com.epam.shop.controller.RequestResponseContent;
import com.epam.shop.exception.TransformException;
import com.epam.shop.model.Product;
import com.epam.shop.model.builder.ProductBuilderDirector;
import com.epam.shop.transform.TransformHandler;
import com.epam.shop.validation.Validator;

public class SaveCommand implements ICommand {
	
	private static final Logger logger = Logger.getLogger(ICommand.class);

	@Override
	public void execute(RequestResponseContent content) {
		HttpServletResponse response = content.getResponse();
		String command = content.getParameter("action")[0];
		String subcat = content.getParameter("subcat")[0];
		String category = content.getParameter("category")[0];
		TransformHandler handler = TransformHandler.getInstance();
		Validator validator = new Validator();
		Product product;
		if (command.equals("save")) {
			product = ProductBuilderDirector.construct(content);
		} else {
			product = new Product();
		}
		try {
			Transformer transformer = handler.getTransformer("save");
			transformer.setParameter("target-subcat", subcat);
			transformer.setParameter("category", category);
			transformer.setParameter("product", product);
			transformer.setParameter("validator", validator);
			OutputStream sr = new ByteArrayOutputStream();
			handler.transform(transformer, new StreamResult(sr));
			if (validator.isValid()) {
				handler.write(sr);
				ICommand products = new ProductsCommand();
				products.execute(content);
			} else {
				response.getOutputStream().print(sr.toString());
			}
			logger.info(product.toString() + " " + validator.toString());
		} catch (IOException | TransformException e) {
			logger.error(e.getMessage());
			///send error
		}
	}
}
