package com.epam.shop.command.impl;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.Transformer;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;

import com.epam.shop.command.ICommand;
import com.epam.shop.controller.RequestResponseContent;
import com.epam.shop.exception.TransformException;
import com.epam.shop.transform.TransformHandler;

public class CategoriesCommand implements ICommand {

	private static final Logger logger = Logger.getLogger(ICommand.class);

	@Override
	public void execute(RequestResponseContent content) {
		HttpServletResponse response = content.getResponse();
		TransformHandler handler = TransformHandler.getInstance();
		try {
			Transformer transformer = handler.getTransformer("categories");
			handler.transform(transformer,
					new StreamResult(response.getOutputStream()));
		} catch (TransformException | IOException e) {
			try {
				response.sendError(HttpServletResponse.SC_SERVICE_UNAVAILABLE);
				logger.error(e.getMessage());
			} catch (IOException e1) {
				logger.error(e1.getMessage());
			}
		}

	}

}
