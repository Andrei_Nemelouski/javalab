package com.epam.shop.command;

import com.epam.shop.controller.RequestResponseContent;

public interface ICommand {
	
	public void execute(RequestResponseContent content);

}
