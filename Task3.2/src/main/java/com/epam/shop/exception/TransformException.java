package com.epam.shop.exception;

@SuppressWarnings("serial")
public class TransformException extends Exception {
	private Throwable cause;

	public TransformException(Throwable cause) {
		this.cause = cause;
	}

	public Throwable getCause() {
		return cause;
	}

	public String getMessage() {
		return cause.getMessage();
	}

}
