package com.epam.shop.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Handles {@code Response} and {@code Request} content
 * 
 * @author Andy
 *
 */
public class RequestResponseContent {
	private Map<String, String[]> requestParameters;
	private HttpServletRequest request;
	private HttpServletResponse response;

	public RequestResponseContent(HttpServletRequest request,
			HttpServletResponse response) {
		requestParameters = request.getParameterMap();
		this.request = request;
		this.response = response;

	}

	/**
	 * Returns request parameter by the key.
	 * 
	 * @param key
	 *            name of the request parameter.
	 * @return {@code String[]} of the parameters.
	 */
	public String[] getParameter(String key) {
		return requestParameters.get(key);
	}

	/**
	 * Sets {@code Request} attribute {@code obj} with name {@code key}.
	 * 
	 * @param key
	 *            name of the attribute
	 * @param obj
	 *            attribute value
	 */
	public void setAttribute(String key, Object obj) {
		request.setAttribute(key, obj);
	}

	/**
	 * Returns {@code Request} attribute with name {@code key}
	 * 
	 * @param key
	 *            name of the attribute
	 * @return attribute value
	 */
	public Object getAttribute(String key) {
		return request.getAttribute(key);

	}

	public HttpServletResponse getResponse() {
		return response;
	}
}
