package com.epam.shop.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.shop.command.ICommand;
import com.epam.shop.command.factory.ICommandFactory;
import com.epam.shop.command.factory.impl.FactoryHandler;

/**
 * Servlet implementation class Controller
 */
public class Controller extends HttpServlet {
	private static final long serialVersionUID = 1L;
	

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Controller() {
		super();		
	}
	
	public void init() {		
		String realPath = getServletContext().getRealPath("/");
		System.setProperty("realPath", realPath);
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {		
		operate(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {		
		operate(request, response);
	}

	private void operate(HttpServletRequest request,
			HttpServletResponse response) {
		String action = request.getParameter("action");
		RequestResponseContent rrc = new RequestResponseContent(request, response);
		//Badbadnotgood
		ICommandFactory commandFactory = FactoryHandler.getInstance().getFactory(); 
		///
		ICommand command = commandFactory.getCommand(action);
		command.execute(rrc);
	}

}
