package com.epam.shop.model.builder;

import com.epam.shop.model.Product;

public class ProductBuilder implements IProductBuilder {
	
	private Product product = new Product();

	@Override
	public Product getResult() {
		return product;
	}

	@Override
	public void buildName(String name) {
		product.setName(name);

	}

	@Override
	public void buildProducer(String producer) {
		product.setProducer(producer);

	}

	@Override
	public void buildModel(String model) {
		product.setModel(model);

	}

	@Override
	public void buildDate(String date) {
		product.setDate(date);
	}

	@Override
	public void buildColor(String color) {
		product.setColor(color);

	}

	@Override
	public void buildPrice(String price) {
		product.setPrice(price);

	}

	@Override
	public void buildCurrency(String currency) {
		product.setCurrency(currency);

	}

}
