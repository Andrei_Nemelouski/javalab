package com.epam.shop.model.builder;

import com.epam.shop.model.Product;

public interface IProductBuilder {

	Product getResult();

	void buildName(String name);

	void buildProducer(String producer);

	void buildModel(String model);

	void buildDate(String date);

	void buildColor(String color);

	void buildPrice(String price);

	void buildCurrency(String currency);
	
}
