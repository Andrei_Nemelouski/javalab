package com.epam.shop.model;

public class Product {

	private String name;
	private String producer;
	private String model;
	private String date;
	private String color;
	private String price;
	private String currency;
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProducer() {
		return producer;
	}

	public void setProducer(String producer) {
		this.producer = producer;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}	

	@Override
	public String toString() {
		return "Product [name=" + name + ", producer=" + producer + ", model="
				+ model + ", date=" + date + ", color=" + color + ", price="
				+ price + ", currency=" + currency + "]";
	}

}
