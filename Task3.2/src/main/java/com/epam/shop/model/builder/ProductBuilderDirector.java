package com.epam.shop.model.builder;

import com.epam.shop.controller.RequestResponseContent;
import com.epam.shop.model.Product;

public class ProductBuilderDirector {

	private static ProductBuilder getBuilder() {
		return new ProductBuilder();
	}

	public static Product construct(RequestResponseContent content) {
		ProductBuilder builder = getBuilder();
		builder.buildName(content.getParameter("name")[0]);
		builder.buildProducer(content.getParameter("producer")[0]);
		builder.buildModel(content.getParameter("model")[0]);
		builder.buildDate(content.getParameter("date")[0]);
		builder.buildColor(content.getParameter("color")[0]);
		if (content.getParameter("unavailable")!=null) {
			builder.buildPrice("Not in stock");
		} else {
			builder.buildPrice(content.getParameter("price")[0]);
			builder.buildCurrency(content.getParameter("currency")[0]);
		}
		return builder.getResult();
	}
}
