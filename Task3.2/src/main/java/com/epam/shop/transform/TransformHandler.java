package com.epam.shop.transform;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import javax.xml.transform.Source;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import com.epam.shop.exception.TransformException;
import com.epam.shop.resource.ResourceProvider;

public final class TransformHandler {

	private TransformerFactory factory = TransformerFactory.newInstance();
	private ReadWriteLock rwl = new ReentrantReadWriteLock();
	private Lock readLock = rwl.readLock();
	private Lock writeLock = rwl.writeLock();
	private Map<String, Templates> templates = new HashMap<>();
	private String xmlPath;
	private File xmlSource;
	private ResourceProvider resourceProvider;
	private static TransformHandler instance;

	private TransformHandler(ResourceProvider resourceProvider, String xml,
			String... args) throws TransformException {
		this.resourceProvider = resourceProvider;
		xmlPath = "classpath:xml/" + xml;
		try {
			xmlSource = resourceProvider.getResource(xmlPath).getFile();
			for (int i = 0; i < args.length; i++) {
				// /bad
				String source = "classpath:xsl/" + args[i] + ".xsl";
				Source xsltSource = new StreamSource(resourceProvider
						.getResource(source).getFile());
				Templates template = factory.newTemplates(xsltSource);
				templates.put(args[i], template);
			}
		} catch (TransformerConfigurationException | IOException e) {
			throw new TransformException(e);
		}
		instance = this;
	}

	public static TransformHandler getInstance() {
		return instance;
	}

	public Transformer getTransformer(String key) throws TransformException {
		Templates temp = templates.get(key);
		try {
			return temp.newTransformer();
		} catch (TransformerConfigurationException e) {
			throw new TransformException(e);
		}
	}

	public void transform(Transformer transformer, StreamResult result)
			throws TransformException {
		readLock.lock();
		try {
			Source xml = new StreamSource(xmlSource);
			transformer.transform(xml, result);
		} catch (TransformerException e) {
			throw new TransformException(e);
		} finally {
			readLock.unlock();
		}
	}

	public void write(OutputStream output) throws TransformException {
		writeLock.lock();
		PrintWriter pw = null;
		try {
			if (!xmlSource.exists()) {
				xmlSource.createNewFile();
			}
			pw = new PrintWriter(xmlSource.getAbsoluteFile());
			pw.print(output.toString());
		} catch (IOException e) {
			throw new TransformException(e);
		} finally {
			// //!!!!!!!
			refresh();
			pw.close();
			writeLock.unlock();
		}

	}

	private void refresh() throws TransformException {
		try {
			xmlSource = resourceProvider.getResource(xmlPath).getFile();
		} catch (IOException e) {
			throw new TransformException(e);
		}
	}
}
