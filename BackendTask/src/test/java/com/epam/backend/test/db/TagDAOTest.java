package com.epam.backend.test.db;

import java.util.List;

import org.junit.Assert;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.backend.db.dao.ITagDAO;
import com.epam.backend.entity.TagTO;
import com.epam.backend.exceptions.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

/**
 * Test class for ITagDAO implementation.
 * 
 * @author Andrei_Nemelouski
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-config.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
@DatabaseSetup("/db_test_input.xml")
public class TagDAOTest {

	private static final Logger logger = Logger.getLogger(TagDAOTest.class);
	@Autowired
	@Qualifier("tagDao")
	private ITagDAO dao;
	/*@Autowired
	private DataSource dataSource;
	private Connection conn;*/

	/*@Before
	public void setUp() {
		try {
			conn = dataSource.getConnection();
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
		dao.setConnection(conn);
	}

	@After
	public void teardown() {
		try {
			conn.close();
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}

	}*/

	/**
	 * Create test
	 */
	@Test
	public void testCreate() {
		TagTO result = null;
		TagTO tto = new TagTO();
		tto.setNewsId(1);
		tto.setTagName("Good");
		int resultId = 0;
		try {
			resultId = dao.create(tto);
			List<TagTO> resultList = dao.read();
			for(TagTO i:resultList){
				if(i.getId() == resultId) result = i;
			}
		} catch (DAOException e) {
			logger.error(e);
		}
		Assert.assertNotEquals(tto, result);

	}

	/**
	 * Read test
	 */
	@Test
	public void testRead() {
		List<TagTO> result = null;
		try {
			result = dao.read();
		} catch (DAOException e) {
			logger.error(e.getMessage());
		}
		Assert.assertEquals(3, result.size());
		//Assert.assertEquals("not bad", result.get(0).getTagName());
	}

	/**
	 * Update test
	 */
	@Test
	@ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT, value = "/tagTest/update_result.xml")	
	public void testUpdate() {
		TagTO tag = new TagTO();
		tag.setId(1);
		tag.setTagName("Good");
		try {
			dao.update(tag);
		} catch (DAOException e) {
			logger.error(e.getMessage());
		} 
	}

	/**
	 * Delete test
	 */
	@Test
	@ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT, value = "/tagTest/delete_result.xml")	
	public void testDelete() {
		int[] id = { 1 };
		try {
			dao.delete(id);			
		} catch (DAOException e) {
			logger.error(e.getMessage());
		} 
	}

}
