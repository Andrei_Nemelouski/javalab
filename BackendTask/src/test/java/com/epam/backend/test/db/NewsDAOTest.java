package com.epam.backend.test.db;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.backend.db.dao.INewsDAO;
import com.epam.backend.entity.NewsTO;
import com.epam.backend.exceptions.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

/**
 * Test class for INewsDAO interface implementation
 * 
 * @author Andrei_Nemelouski
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-config.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
@DatabaseSetup("/db_test_input.xml")
public class NewsDAOTest {

	private static final Logger logger = Logger.getLogger(NewsDAOTest.class);
	@Autowired
	private INewsDAO dao;
	/*@Autowired
	private DataSource dataSource;
	private Connection conn;*/

	/*@Before
	public void setUp() {
		try {
			conn = dataSource.getConnection();
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
		dao.setConnection(conn);
	}

	@After
	public void teardown() {
		try {
			conn.close();
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}

	}*/

	/**
	 * Create test
	 */
	@Test
	public void testCreate() {
		int resultId = 0;
		NewsTO result = null;
		NewsTO news = new NewsTO();
		news.setShortText("Bye");
		news.setFullText("Bye bye!");
		news.setTitle("Another greeting");
		news.setCreationDate(Timestamp.valueOf("2015-03-24 19:12:12.0"));
		news.setModificationDate(Date.valueOf("2012-02-12"));
		try {
			resultId = dao.create(news);
			result = dao.read(resultId);
		} catch (DAOException e) {
			logger.error(e.getMessage());
		}
		Assert.assertNotEquals(news, result);

	}

	/**
	 * News list read test 
	 */
	@Test
	public void testReadAll() {
		List<NewsTO> result = null;
		try {
			result = dao.read();
		} catch (DAOException e) {
			logger.error(e.getMessage());
		}
		Assert.assertEquals(2, result.size());
	}

	/**
	 * Single news read test
	 */
	@Test
	public void testReadSingle() {
		NewsTO news = null;
		try {
			news = dao.read(1);
		} catch (DAOException e) {
			logger.error(e.getMessage());
		}
		Assert.assertEquals("Greeting", news.getTitle());

	}

	/**
	 * Test for fetching {@code News} by author id
	 */
	@Test
	public void testFetchByAuthorId() {
		List<NewsTO> result = null;
		try {
			result = dao.fetchByAuthorId(1);
		} catch (DAOException e) {
			logger.error(e.getMessage());
		}
		Assert.assertEquals("Greeting", result.get(0).getTitle());

	}
	
	/**
	 * Test for fetching {@code News} by tag id
	 */
	@Test
	public void testFetchByTagId() {
		List<NewsTO> result = null;
		try {
			result = dao.fetchByTagId(1);
		} catch (DAOException e) {
			logger.error(e.getMessage());
		}
		//String title = result.get(0).getTitle();
		Assert.assertNotEquals(0, result.size());
	}
	
	/**
	 * Delete test
	 */
	@Test
	@ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT, value = "/newsTest/delete_result.xml")		
	public void testDelete() {
		int[] test = { 1 };
		try {
			dao.delete(test);
		} catch (DAOException e) {
			logger.error(e.getMessage());
		}
	}

	@Test
	@ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT, value = "/newsTest/update_result.xml")		
	public void testUpdate() {
		NewsTO news = new NewsTO();
		news.setId(1);
		news.setShortText("Goodbye");
		news.setFullText("Have a nice day!");
		news.setTitle("Greeting");
		news.setCreationDate(Timestamp.valueOf("2015-03-24 19:12:12.0"));
		news.setModificationDate(Date.valueOf("2012-02-12"));
		try {
			dao.update(news);
		} catch (DAOException e) {
			logger.error(e.getMessage());
		} 

	}

}
