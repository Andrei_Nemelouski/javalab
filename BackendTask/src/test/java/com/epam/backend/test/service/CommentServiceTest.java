package com.epam.backend.test.service;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.backend.db.dao.ICommentDAO;
import com.epam.backend.entity.CommentTO;
import com.epam.backend.exceptions.DAOException;
import com.epam.backend.exceptions.ServiceException;
import com.epam.backend.service.ICommentService;

/**
 * Test class for Comment service class
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:**/test-config.xml" })
public class CommentServiceTest {

	private final static Logger logger = Logger
			.getLogger(CommentServiceTest.class);

	@Mock
	private ICommentDAO dao;
	@Mock
	private CommentTO cto;
	@InjectMocks
	@Autowired
	@Qualifier("commentService")
	private ICommentService serv;

	/**
	 * Init mocks
	 */
	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Add test
	 */
	@Test
	public void testAdd() {
		try {
			Mockito.doReturn(1).when(dao).create(cto);
			int expected = 1;
			int actual = serv.create(cto);
			Mockito.verify(dao).create(cto);
			Assert.assertEquals(expected, actual);
		} catch (DAOException e) {
			logger.error(e.getMessage());
		} catch (ServiceException e) {
			logger.error(e.getMessage());
		}
	}

	/**
	 * Delete test
	 */
	@Test
	public void testDelete() {
		int[] del = { 1 };
		//Mockito.doReturn(del).when(cto).getDelete();
		try {
			serv.delete(del);
			//sMockito.verify(cto).getDelete();
			Mockito.verify(dao).delete(del);
		} catch (DAOException e) {
			logger.error(e.getMessage());
		} catch (ServiceException e) {
			logger.error(e.getMessage());
		}
	}
}
