package com.epam.backend.test.service;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.backend.db.dao.IAuthorDAO;
import com.epam.backend.entity.AuthorTO;
import com.epam.backend.exceptions.DAOException;
import com.epam.backend.exceptions.ServiceException;
import com.epam.backend.service.IAuthorService;
import com.epam.backend.service.impl.AuthorService;

/**
 * Test class for Author service class.
 * 
 * @author Andrei_Nemelouski
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:**/test-config.xml" })
public class AuthorServiceTest {

	@Spy
	@Autowired
	@Qualifier("authorDao")
	private IAuthorDAO dao;
	@Mock
	private AuthorTO aut;
	@InjectMocks
	@Autowired
	@Qualifier("authorService")
	private IAuthorService serv;

	private final static Logger logger = Logger.getLogger(AuthorService.class);

	/**
	 * Mocks initiation
	 */
	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);		
	}

	/**
	 * Add method test
	 */
	@Test
	public void testAdd() {
		try {
			Mockito.doReturn(1).when(dao).create(aut);
			int actual = serv.create(aut);
			int expected = 1;
			Assert.assertEquals(actual, expected);
			Mockito.verify(dao).create(aut);
		} catch (DAOException e) {
			logger.error(e.getMessage());
		} catch (ServiceException e) {
			logger.error(e.getMessage());
		}
	}
}
