package com.epam.backend.test.service;

import java.util.List;

import org.junit.Assert;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.backend.db.dao.INewsDAO;
import com.epam.backend.entity.AuthorTO;
import com.epam.backend.entity.NewsTO;
import com.epam.backend.entity.TagTO;
import com.epam.backend.exceptions.DAOException;
import com.epam.backend.exceptions.ServiceException;
import com.epam.backend.service.INewsService;
import com.epam.backend.service.dto.CompleteNewsDataDTO;
/**
 * News service test class
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:**/test-config.xml" })
public class NewsServiceTest {

	private final static Logger logger = Logger
			.getLogger(NewsServiceTest.class);
	@Spy
	@Autowired
	@Qualifier("newsDao")
	private INewsDAO dao;
	@Mock
	private TagTO tag;
	@Mock
	private AuthorTO aut;	
	@Mock
	private List<NewsTO> list;
	@Mock
	private CompleteNewsDataDTO dto;
	@Mock
	private NewsTO news;
	@InjectMocks
	@Autowired
	@Qualifier("newsService")
	private INewsService serv;

	/**
	 * Init mocks
	 */
	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Add test
	 */
	@Test
	public void testCreate() {
		try {
			Mockito.doReturn(1).when(dao).create(news);
			serv.create(news);			
			Mockito.verify(dao).create(news);					
		} catch (DAOException e) {
			logger.error(e.getMessage());
		} catch (ServiceException e) {
			logger.error(e.getMessage());
		}

	}

	/**
	 * Edit test
	 */
	@Test
	public void testEdit() {
		try {
			Mockito.doReturn(1).when(dao).update(news);
			int expected = 1;
			int actual = serv.update(news);
			Mockito.verify(dao).update(news);
			Assert.assertEquals(expected, actual);
		} catch (DAOException e) {
			logger.error(e.getMessage());
		} catch (ServiceException e) {
			logger.error(e.getMessage());
		}

	}

	/**
	 * Delete test
	 */
	@Test
	public void testDelete() {
		int[] del = { 1 };
		try {
			serv.delete(del);
			Mockito.verify(dao).delete(del);			
		} catch (DAOException e) {
			logger.error(e.getMessage());
		} catch (ServiceException e) {
			logger.error(e.getMessage());
		}

	}

	/**
	 * News list obtaining test
	 */
	@Test
	public void testReadAll() {
		try {
			Mockito.doReturn(list).when(dao).read();
			List<NewsTO> actual = serv.readAllNews();
			Mockito.verify(dao).read();
			Assert.assertEquals(list, actual);
		} catch (DAOException e) {
			logger.error(e.getMessage());
		} catch (ServiceException e) {
			logger.error(e.getMessage());
		}
	}

	/**
	 * Single news obtaining test
	 */
	@Test
	public void testReadSingleNews() {
		try {
			Mockito.doReturn(news).when(dao).read(1);
			NewsTO actual = serv.readSingleNews(1);
			Mockito.verify(dao).read(1);
			Assert.assertEquals(news, actual);
		} catch (DAOException e) {
			logger.error(e.getMessage());
		} catch (ServiceException e) {
			logger.error(e.getMessage());
		}

	}
	
	/**
	 * Test method for getting all news by author
	 */
	@Test
	public void testFetchByAuthorId() {
		/*List<NewsTO> result = new ArrayList<>();
		result.add(new NewsTO());*/
		try {
			Mockito.doReturn(1).when(aut).getId();
			Mockito.doReturn(list).when(dao).fetchByAuthorId(1);
			List<NewsTO> actual = serv.fetchByAuthorId(aut);
			Assert.assertEquals(list, actual);
			Mockito.verify(aut).getId();
			Mockito.verify(dao).fetchByAuthorId(1);
		} catch (DAOException e) {
			logger.error(e.getMessage());
		} catch (ServiceException e) {
			logger.error(e.getMessage());
		}
	}

	/**
	 * Test method for getting all news by tag
	 */
	@Test
	public void testFetchByTagId() {
		/*List<NewsTO> result = new ArrayList<>();
		result.add(new NewsTO());*/
		try {
			Mockito.doReturn(1).when(tag).getId();
			Mockito.doReturn(list).when(dao).fetchByTagId(1);
			List<NewsTO> actual = serv.fetchByTagId(tag);
			Mockito.verify(dao).fetchByTagId(1);
			Assert.assertEquals(list, actual);
		} catch (DAOException e) {
			logger.error(e.getMessage());
		} catch (ServiceException e) {
			logger.error(e.getMessage());
		}
	}
}
