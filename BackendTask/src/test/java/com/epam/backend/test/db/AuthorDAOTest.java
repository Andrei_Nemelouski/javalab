package com.epam.backend.test.db;

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;

import com.epam.backend.db.dao.IAuthorDAO;
import com.epam.backend.entity.AuthorTO;
import com.epam.backend.exceptions.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

/**
 * Test class for IAuthorDAO implementation.
 * 
 * @author Andrei_Nemelouski
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-config.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
@DatabaseSetup("/db_test_input.xml")
public class AuthorDAOTest {

	private static final Logger logger = Logger.getLogger(AuthorDAOTest.class);
	@Autowired
	@Qualifier("authorDao")
	private IAuthorDAO dao;
	/**
	 * Read method test
	 */
	@Test
	public void testRead() {
		List<AuthorTO> result = null;
		try {
			result = dao.read();
		} catch (DAOException e) {
			logger.error(e.getMessage());
		}
		Assert.assertEquals(3, result.size());
		//Assert.assertEquals("Andy", result.get(0).getName());

	}

	/**
	 * Create test
	 */
	@Test
	public void testCreate() {
		AuthorTO aut = new AuthorTO();
		aut.setName("Mighty BLABL!!!");
		aut.setNewsId(2);
		int resultId = 0;
		AuthorTO result = null;
		try {
			resultId = dao.create(aut);			
			aut.setId(resultId);
			List<AuthorTO> resultList = dao.read();
			for(AuthorTO i:resultList){
				if(i.getId()==resultId) result = i;
			}
			if(result!=null){
				result.setNewsId(2);
			}
		} catch (DAOException e) {
			logger.error(e.getMessage());
		}
		
		Assert.assertEquals(aut,result);

	}

	/**
	 * Update test
	 */
	@Test
	@ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT, value = "/authorTest/update_result.xml")
	public void testUpdate() {
		AuthorTO aut = new AuthorTO();
		aut.setId(1);
		aut.setName("Pandy");
		try {
			dao.update(aut);
		} catch (DAOException e) {
			logger.error(e.getMessage());
		}
	}

	/**
	 * Delete test
	 */
	@Test
	@ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT, value = "/authorTest/delete_result.xml")
	public void testDelete() {
		int[] id = { 1 };
		try {
			dao.delete(id);
		} catch (DAOException e) {
			logger.error(e.getMessage());
		}

	}
}
