package com.epam.backend.test.service;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.backend.db.dao.ITagDAO;
import com.epam.backend.entity.TagTO;
import com.epam.backend.exceptions.DAOException;
import com.epam.backend.exceptions.ServiceException;
import com.epam.backend.service.ITagService;
import com.epam.backend.service.impl.TagService;

/**
 * Test class for tag service class
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:**/test-config.xml" })
public class TagServiceTest {
	
	@Spy
	@Autowired
	@Qualifier("tagDao")
	private ITagDAO dao;
	@Mock
	private TagTO tag;
	@InjectMocks
	@Autowired
	@Qualifier("tagService")
	private ITagService serv;

	private final static Logger logger = Logger.getLogger(TagService.class);

	/**
	 * Mocks initiation
	 */
	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Add method test
	 */
	@Test
	public void testAdd() {
		try {
			Mockito.doReturn(1).when(dao).create(tag);
			int actual = serv.create(tag);
			int expected = 1;
			Mockito.verify(dao).create(tag);
			Assert.assertEquals(actual, expected);
		} catch (DAOException e) {
			logger.error(e.getMessage());
		} catch (ServiceException e) {
			logger.error(e.getMessage());
		}
	}

	

}
