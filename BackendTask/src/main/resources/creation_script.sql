CREATE TABLE News (
news_id NUMBER(20) NOT NULL,
short_text VARCHAR2(100) NOT NULL,
full_text VARCHAR2(2000) NOT NULL,
title VARCHAR2(30) NOT NULL,
creation_date TIMESTAMP NOT NULL,
modification_date DATE NOT NULL,PRIMARY KEY(news_id)); 
CREATE TABLE Author (
author_id NUMBER(20) NOT NULL,
name VARCHAR(30) NOT NULL,PRIMARY KEY(author_id)); 
CREATE TABLE Comments (
comment_id NUMBER(20) NOT NULL,
comment_text VARCHAR2(100) NOT NULL,
creation_date TIMESTAMP NOT NULL,news_id NUMBER(20) NULL,
PRIMARY KEY(comment_id),
FOREIGN KEY(news_id) REFERENCES News(news_id)); 
CREATE TABLE Tag (
tag_id NUMBER(20) NOT NULL,
tag_name VARCHAR(20) NOT NULL,PRIMARY KEY(tag_id)); 
CREATE TABLE News_Author (
news_author_id NUMBER(20) NOT NULL,
news_id NUMBER(20) NOT NULL,
author_id NUMBER(20) NOT NULL,
PRIMARY KEY(news_author_id),
FOREIGN KEY(news_id) REFERENCES News(news_id),
FOREIGN KEY(author_id) REFERENCES Author(author_id)); 
CREATE TABLE News_Tag (
news_tag_id NUMBER(20) NOT NULL,
news_id NUMBER(20) NOT NULL,
tag_id NUMBER(20) NOT NULL,
PRIMARY KEY(news_tag_id),
FOREIGN KEY(news_id) REFERENCES News(news_id),
FOREIGN KEY(tag_id) REFERENCES Tag(tag_id));
CREATE SEQUENCE news_id_seq MINVALUE 1;
CREATE SEQUENCE news_auth_id_seq MINVALUE 1;
CREATE SEQUENCE news_tag_id_seq MINVALUE 1;
CREATE SEQUENCE tag_id_seq MINVALUE 1;
CREATE SEQUENCE author_id_seq MINVALUE 1;
CREATE SEQUENCE comment_id_seq MINVALUE 1;
