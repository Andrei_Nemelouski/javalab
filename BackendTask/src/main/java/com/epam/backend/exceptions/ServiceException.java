package com.epam.backend.exceptions;

/**
 * Service layer exception
 * 
 * @author Andrei_Nemelouski
 *
 */
@SuppressWarnings("serial")
public class ServiceException extends Exception {
	/**
	 * Exception layer
	 */
	private Throwable cause;

	public ServiceException(Throwable cause) {
		this.cause = cause;
	}

	public Throwable getCause() {
		return cause;
	}

	public String getMessage() {
		return cause.getMessage();
	}

}
