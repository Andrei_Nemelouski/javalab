package com.epam.backend.exceptions;

/**
 * DAO layer exception
 * @author Andrei_Nemelouski
 *
 */
@SuppressWarnings("serial")
public class DAOException extends Exception {
	
	/**
	 * Exception cause
	 */
	private Throwable cause;

	public DAOException(Throwable cause) {
		this.cause = cause;
	}

	public Throwable getCause() {
		return cause;
	}

	public String getMessage() {
		return cause.getMessage();
	}

}
