package com.epam.backend.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.epam.backend.db.dao.INewsDAO;
import com.epam.backend.entity.AuthorTO;
import com.epam.backend.entity.NewsTO;
import com.epam.backend.entity.TagTO;
import com.epam.backend.exceptions.DAOException;
import com.epam.backend.exceptions.ServiceException;
import com.epam.backend.service.INewsService;

/**
 * Service class for operations with {@code News}
 * 
 * @author Andrei_Nemelouski
 *
 */
public class NewsService implements INewsService{

	@Autowired
	@Qualifier("newsDao")
	private INewsDAO newsDao;
	
	/**
	 * Add News with author and tags using appropriate DAO classes
	 * 
	 * @param dto
	 *            Service layer transfer object
	 * @return ServiceDTO completes with id's of added rows
	 * @throws ServiceException
	 *             wrapper exception for exceptions from DAO layer
	 */
	public int create(NewsTO nto) throws ServiceException {
		try {
			return newsDao.create(nto);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}		
	}

	/**
	 * Change news according to information received in {@code NewsTO}
	 * 
	 * @param nto
	 *            News transfer object
	 * @return number of updated rows
	 * @throws ServiceException
	 *             wrapper exception for exceptions from DAO layer
	 */
	public int update(NewsTO nto) throws ServiceException {
		try {
			return newsDao.update(nto);
		} catch (DAOException e) {
			throw new ServiceException(e);
		} 
	}

	/**
	 * Delete news according to id's array
	 * 
	 * @param delete
	 *            array of id's to delete
	 * @throws ServiceException
	 *             wrapper exception for exceptions from DAO layer
	 */
	public void delete(int[] delete) throws ServiceException {
		try {
			newsDao.delete(delete);
		} catch (DAOException e) {
			throw new ServiceException(e);
		} 
	}

	/**
	 * Return's all news from database
	 * 
	 * @return List<NewsTO> list of all news
	 * @throws ServiceException
	 *             wrapper exception for exceptions from DAO layer
	 */
	public List<NewsTO> readAllNews() throws ServiceException {
		List<NewsTO> newsList = null;
		try {
			newsList = newsDao.read();
		} catch (DAOException e) {
			throw new ServiceException(e);
		} 
		return newsList;

	}

	/**
	 * Return's single news record by id
	 * 
	 * @param id
	 *            id of needed news
	 * @return NewsTO news transfer object
	 * @throws ServiceException
	 *             wrapper exception for exceptions from DAO layer
	 */
	public NewsTO readSingleNews(int id) throws ServiceException {
		NewsTO news = null;
		try {
			news = newsDao.read(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		} 
		return news;
	}
	
	/**
	 * Fetching all news by author
	 * 
	 * @param aut
	 *            Author transfer object
	 * @return List of all news related with author {@code aut}
	 * @throws ServiceException
	 *             wrapper exception for exceptions from DAO layer
	 */
	public List<NewsTO> fetchByAuthorId(AuthorTO aut) throws ServiceException {
		try {
			return newsDao.fetchByAuthorId(aut.getId());
		} catch (DAOException e) {
			throw new ServiceException(e);
		} 
	}
	
	/**
	 * Fetching all news by tag
	 * 
	 * @param tto
	 *            Tag transfer object
	 * @return List of all news related with tag
	 * @throws ServiceException
	 *             wrapper exception for exceptions from DAO layer
	 */
	public List<NewsTO> fetchByTagId(TagTO tto) throws ServiceException {
		try {
			return newsDao.fetchByTagId(tto.getId());
		} catch (DAOException e) {
			throw new ServiceException(e);
		} 
	}

}
