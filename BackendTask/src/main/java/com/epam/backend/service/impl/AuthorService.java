package com.epam.backend.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.epam.backend.db.dao.IAuthorDAO;
import com.epam.backend.entity.AuthorTO;
import com.epam.backend.exceptions.DAOException;
import com.epam.backend.exceptions.ServiceException;
import com.epam.backend.service.IAuthorService;

/**
 * Service class for operations using {@code author} information
 * 
 * @author Andrei_Nemelouski
 *
 */
public class AuthorService implements IAuthorService{

	@Autowired
	@Qualifier("authorDao")
	private IAuthorDAO authorDao;
	
	/**
	 * Creating new {@author}
	 * 
	 * @param aut
	 *            Author transfer object
	 * @return added Author id
	 * @throws ServiceException
	 *             wrapper exception for exceptions from DAO layer
	 */
	@Override
	public int create(AuthorTO aut) throws ServiceException {
		try {
			return authorDao.create(aut);
		} catch (DAOException e) {
			throw new ServiceException(e);
		} 
	}

	@Override
	public int update(AuthorTO obj) throws ServiceException {
		throw new UnsupportedOperationException();		
	}

	@Override
	public void delete(int[] delete) throws ServiceException {
		throw new UnsupportedOperationException();			
	}

	
}
