package com.epam.backend.service;

import com.epam.backend.exceptions.ServiceException;

public interface IService<T> {
	
	int create(T obj) throws ServiceException;
	
	int update(T obj) throws ServiceException;
	
	void delete(int[] delete) throws ServiceException;
	
	

}
