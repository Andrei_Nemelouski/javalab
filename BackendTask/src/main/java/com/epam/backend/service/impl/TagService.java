package com.epam.backend.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.epam.backend.db.dao.ITagDAO;
import com.epam.backend.entity.TagTO;
import com.epam.backend.exceptions.DAOException;
import com.epam.backend.exceptions.ServiceException;
import com.epam.backend.service.ITagService;

/**
 * Service class for operations using {@code ITagDAO} implementations 
 * @author Andrei_Nemelouski
 *
 */
public class TagService implements ITagService{
	
	@Autowired
	@Qualifier("tagDao")
	private ITagDAO tagDao;
	

	/**
	 * Creating new {@tag}
	 * 
	 * @param aut
	 *            Tag transfer object
	 * @return added Tag id
	 * @throws ServiceException
	 *             wrapper exception for exceptions from DAO layer
	 */
	
	public int create(TagTO tto) throws ServiceException {
		try {
			return tagDao.create(tto);
		} catch (DAOException e) {
			throw new ServiceException(e);
		} 
	}


	@Override
	public int update(TagTO obj) throws ServiceException {
		throw new UnsupportedOperationException();	
	}


	@Override
	public void delete(int[] delete) throws ServiceException {
		throw new UnsupportedOperationException();			
	}
}
