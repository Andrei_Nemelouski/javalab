package com.epam.backend.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.epam.backend.db.dao.ICommentDAO;
import com.epam.backend.entity.CommentTO;
import com.epam.backend.exceptions.DAOException;
import com.epam.backend.exceptions.ServiceException;
import com.epam.backend.service.ICommentService;

/**
 * Service class for news comments
 * 
 * @author Andrei_Nemelouski
 *
 */
public class CommentService implements ICommentService {

	@Autowired
	@Qualifier("commentDao")
	private ICommentDAO commentDao;

	/**
	 * Add new comment news
	 * 
	 * @param cto
	 *            Comment transfer object
	 * @return added comment id
	 * @throws ServiceException
	 *             wrapper exception for exceptions from DAO layer
	 */
	@Override
	public int create(CommentTO cto) throws ServiceException {
		try {
			return commentDao.create(cto);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * Delete comments by id according to {@code delete} array
	 * 
	 * @param delete
	 *            array of rows to delete
	 * @throws ServiceException
	 *             wrapper exception for exceptions from DAO layer
	 */
	@Override
	public void delete(int[] delete) throws ServiceException {
		try {
			commentDao.delete(delete);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public int update(CommentTO obj) throws ServiceException {
		throw new UnsupportedOperationException();
	}

}
