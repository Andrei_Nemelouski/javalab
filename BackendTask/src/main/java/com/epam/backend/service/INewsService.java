package com.epam.backend.service;

import java.util.List;

import com.epam.backend.entity.AuthorTO;
import com.epam.backend.entity.NewsTO;
import com.epam.backend.entity.TagTO;
import com.epam.backend.exceptions.ServiceException;

public interface INewsService extends IService<NewsTO> {

	List<NewsTO> readAllNews() throws ServiceException;

	NewsTO readSingleNews(int id) throws ServiceException;

	List<NewsTO> fetchByAuthorId(AuthorTO aut) throws ServiceException;

	List<NewsTO> fetchByTagId(TagTO tto) throws ServiceException;

}
