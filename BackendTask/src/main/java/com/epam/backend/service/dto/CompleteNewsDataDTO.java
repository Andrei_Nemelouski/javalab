package com.epam.backend.service.dto;

import java.util.Arrays;
import java.util.List;

import com.epam.backend.entity.AuthorTO;
import com.epam.backend.entity.CommentTO;
import com.epam.backend.entity.NewsTO;
import com.epam.backend.entity.TagTO;

/**
 * Service layer transfer object
 * 
 * @author Andrei_Nemelouski
 *
 */
public class CompleteNewsDataDTO {
	/**
	 * News transfer object
	 */
	private NewsTO news;
	/**
	 * Related author transfer object
	 */
	private AuthorTO author;
	/**
	 * List of related comments
	 */
	private List<CommentTO> comments;
	/**
	 * Related tags
	 */
	private TagTO tag;
	/**
	 *id's for delete 
	 */
	private int[] delete;

	// Getters and setters

	public NewsTO getNews() {
		return news;
	}

	public void setNews(NewsTO news) {
		this.news = news;
	}

	public AuthorTO getAuthor() {
		return author;
	}

	public void setAuthor(AuthorTO author) {
		this.author = author;
	}

	public List<CommentTO> getComments() {
		return comments;
	}

	public void setComments(List<CommentTO> comments) {
		this.comments = comments;
	}

	public TagTO getTag() {
		return tag;
	}

	public void setTags(TagTO tag) {
		this.tag = tag;
	}

	public int[] getDelete() {
		return delete;
	}

	public void setDelete(int[] delete) {
		this.delete = delete;
	}

	@Override
	public String toString() {
		return "CompleteNewsDataDTO [news=" + news + ", author=" + author
				+ ", comments=" + comments + ", tag=" + tag + ", delete="
				+ Arrays.toString(delete) + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((author == null) ? 0 : author.hashCode());
		result = prime * result
				+ ((comments == null) ? 0 : comments.hashCode());
		result = prime * result + Arrays.hashCode(delete);
		result = prime * result + ((news == null) ? 0 : news.hashCode());
		result = prime * result + ((tag == null) ? 0 : tag.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof CompleteNewsDataDTO))
			return false;
		CompleteNewsDataDTO other = (CompleteNewsDataDTO) obj;
		if (author == null) {
			if (other.author != null)
				return false;
		} else if (!author.equals(other.author))
			return false;
		if (comments == null) {
			if (other.comments != null)
				return false;
		} else if (!comments.equals(other.comments))
			return false;
		if (!Arrays.equals(delete, other.delete))
			return false;
		if (news == null) {
			if (other.news != null)
				return false;
		} else if (!news.equals(other.news))
			return false;
		if (tag == null) {
			if (other.tag != null)
				return false;
		} else if (!tag.equals(other.tag))
			return false;
		return true;
	}

	

	

}
