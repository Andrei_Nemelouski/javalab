package com.epam.backend.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.epam.backend.entity.AuthorTO;
import com.epam.backend.entity.NewsTO;
import com.epam.backend.entity.TagTO;
import com.epam.backend.exceptions.ServiceException;
import com.epam.backend.service.IAuthorService;
import com.epam.backend.service.ICommentService;
import com.epam.backend.service.INewsService;
import com.epam.backend.service.ITagService;
import com.epam.backend.service.dto.CompleteNewsDataDTO;

/**
 * Service layer director class
 * 
 * @author Andrei_Nemelouski
 *
 */
public class NewsManager {
	@Autowired
	@Qualifier("authorService")
	private IAuthorService author;
	@Autowired
	@Qualifier("commentService")
	private ICommentService comment;
	@Autowired
	@Qualifier("newsService")
	private INewsService news;
	@Autowired
	@Qualifier("tagService")
	private ITagService tag;

	/**
	 * Call's {@code AuthorService} add method
	 * 
	 * @param ato
	 *            Author transfer object
	 * @throws ServiceException
	 *             wrapper exception for exceptions from DAO layer
	 *//*
	public void createAuthor(AuthorTO ato) throws ServiceException {
		author.create(ato);
	}

	*//**
	 * Call's {@code AuthorService} getNewsList method
	 * 
	 * @param List
	 *            <NewsTO> list of related news
	 * @throws ServiceException
	 *             wrapper exception for exceptions from DAO layer
	 *//*
	public List<NewsTO> getNewsByAuthor(AuthorTO ato) throws ServiceException {
		return news.fetchByAuthorId(ato);
	}

	*//**
	 * Call's {@code CommentService} add method
	 * 
	 * @param cto
	 *            Comment transfer object
	 * @throws ServiceException
	 *             wrapper exception for exceptions from DAO layer
	 *//*
	public void createComment(CommentTO cto) throws ServiceException {
		comment.create(cto);
	}
*/
	/**
	 * Call's {@code CommentService} delete method
	 * 
	 * @param dto
	 *            Complete news data transfer object
	 * @throws ServiceException
	 *             wrapper exception for exceptions from DAO layer
	 */
	public void deleteComments(CompleteNewsDataDTO dto) throws ServiceException {
		int[] delete = dto.getDelete();
		comment.delete(delete);
	}

	/**
	 * Call's {@code NewsService} add method
	 * 
	 * @param dto
	 *            Service layer transfer object
	 * @throws ServiceException
	 *             wrapper exception for exceptions from DAO layer
	 */
	public void createNews(CompleteNewsDataDTO dto) throws ServiceException {
		NewsTO nto = dto.getNews();
		AuthorTO ato = dto.getAuthor();
		TagTO tto = dto.getTag();
		int news_id = news.create(nto);
		ato.setNewsId(news_id);
		tto.setNewsId(news_id);
		author.create(ato);
		tag.create(tto);
		
	}

	/**
	 * Call's {@code NewsService} edit method
	 * 
	 * @param dto
	 *            Service layer transfer object
	 * @throws ServiceException
	 *             wrapper exception for exceptions from DAO layer
	 */
	public void updateNews(CompleteNewsDataDTO dto) throws ServiceException {
		NewsTO nto = dto.getNews();
		news.update(nto);
	}

	/**
	 * Call's {@code NewsService} delete method
	 * 
	 * @param dto
	 *            Service layer transfer object
	 * @throws ServiceException
	 *             wrapper exception for exceptions from DAO layer
	 */
	public void deleteNews(CompleteNewsDataDTO dto) throws ServiceException {
		int[] delete = dto.getDelete();
		news.delete(delete);
	}

	/**
	 * Call's {@code TagService} add method
	 * 
	 * @param tto
	 *            Tag transfer object
	 * @throws ServiceException
	 *             wrapper exception for exceptions from DAO layer
	 *//*
	public void createTag(TagTO tto) throws ServiceException {
		tag.create(tto);
	}

	*//**
	 * Call's {@code TagService} getNewsList method
	 * 
	 * @param List
	 *            <NewsTO> list of related news
	 * @throws ServiceException
	 *             wrapper exception for exceptions from DAO layer
	 *//*
	public List<NewsTO> getNewsByTag(TagTO tto) throws ServiceException {
		return news.fetchByTagId(tto);

	}*/

}
