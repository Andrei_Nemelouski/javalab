package com.epam.backend.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface IMapper<T>{
	
	<F extends ResultSet> T construct(F from) throws SQLException;
	

}
