package com.epam.backend.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.epam.backend.entity.TagTO;

public class TagTOMapper implements IMapper<TagTO> {

	@Override
	public <F extends ResultSet> TagTO construct(F from) throws SQLException {
		TagTO tag = new TagTO();
		tag.setId(from.getInt("tag_id"));
		tag.setTagName(from.getString("tag_name"));
		return tag;
	}

}
