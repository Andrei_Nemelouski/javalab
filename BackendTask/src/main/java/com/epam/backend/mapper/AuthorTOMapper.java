package com.epam.backend.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.epam.backend.entity.AuthorTO;

public class AuthorTOMapper implements IMapper<AuthorTO> {

	@Override
	public <F extends ResultSet> AuthorTO construct(F from) throws SQLException {
		AuthorTO aut = new AuthorTO();
		aut.setId(from.getInt("author_id"));
		aut.setName(from.getString("name"));
		return aut;
	}

}
