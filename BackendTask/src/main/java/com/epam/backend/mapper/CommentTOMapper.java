package com.epam.backend.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.epam.backend.entity.CommentTO;

public class CommentTOMapper implements IMapper<CommentTO> {

	@Override
	public <F extends ResultSet> CommentTO construct(F from)
			throws SQLException {
		CommentTO com = new CommentTO();
		com.setId(from.getInt("comment_id"));
		com.setText(from.getString("comment_text"));
		com.setCreationDate(from.getTimestamp("creation_date"));
		com.setNewsId(from.getInt("news_id"));
		return com;
		
	}

}
