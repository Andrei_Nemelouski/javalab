package com.epam.backend.db.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.epam.backend.db.dao.ICommentDAO;
import com.epam.backend.entity.CommentTO;
import com.epam.backend.exceptions.DAOException;
import com.epam.backend.mapper.IMapper;

public class CommentDAO implements ICommentDAO {

	private final String ADD = "INSERT INTO Comments(comment_id,comment_text,creation_date,news_id) VALUES (comment_id_seq.nextval, ?,?,?)";
	private final String DELETE = "DELETE FROM Comments WHERE comment_id IN ";
	private final String READ = "SELECT comments.comment_id, comments.comment_text, comments.creation_date, comments.news_id FROM comments";
	private final String UPDATE = "UPDATE comments SET comment_text=?, creation_date=? WHERE comment_id=?";
	private final String[] GENERATED_ID = {"COMMENT_ID"};	
	@Autowired
	@Qualifier("dataSource")
	private DataSource dataSource;	
	@Autowired
	private IMapper<CommentTO> commentMapper;

	/**
	 * Creates new record in the {@code Comments} table
	 * 
	 * @param obj
	 *            Entity transfer object
	 * @return id of created {@code Comment}
	 * @throws DAOException
	 *             exception that handles SQLException from database
	 */
	@Override
	public int create(CommentTO obj) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int result = 0;
		try {
			conn = dataSource.getConnection();
			ps = conn.prepareStatement(ADD, GENERATED_ID);
			ps.setString(1, obj.getText());
			ps.setTimestamp(2, obj.getCreationDate());
			ps.setInt(3, obj.getNewsId());
			ps.executeUpdate();
			rs = ps.getGeneratedKeys();
			while(rs.next()){
				result = rs.getInt(1);
			}			
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DaoUtil.closeResources(conn,ps,rs);
		}
		return result;
	}

	/**
	 * Returns all records from {@code Comments} table
	 * 
	 * @return {@code List} of entities
	 * @throws DAOException
	 *             exception that handles SQLException from database
	 */
	@Override
	public List<CommentTO> read() throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<CommentTO> list = new ArrayList<>();
		try {
			conn = dataSource.getConnection();
			ps = conn.prepareStatement(READ);
			rs = ps.executeQuery();
			while (rs.next()) {
				list.add(commentMapper.construct(rs));
			}
			return list;
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DaoUtil.closeResources(conn,ps,rs);
		}
	}

	/**
	 * Updates table {@code Comments} according to {@code obj} contents
	 * 
	 * @param obj
	 *            Entity transfer object
	 * @return number of updated rows
	 * @throws DAOException
	 *             exception that handles SQLException from database
	 */
	@Override
	public int update(CommentTO obj) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = dataSource.getConnection();
			ps = conn.prepareStatement(UPDATE);
			ps.setString(1, obj.getText());
			ps.setTimestamp(2, obj.getCreationDate());
			ps.setInt(3, obj.getId());
			return ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DaoUtil.closeResources(conn,ps,null);
		}

	}

	/**
	 * 
	 * Deletes all rows with id from {@code id} array
	 * 
	 * @param id
	 *            array of rows id's
	 * @throws DAOException
	 *             exception that handles SQLException from database
	 */
	@Override
	public void delete(int[] id) throws DAOException {
		Connection conn = null;
		Statement st = null;		
		String script = DaoUtil.createRemoveCommand(DELETE, id);
		try {
			conn = dataSource.getConnection();
			st = conn.createStatement();
			st.executeUpdate(script);
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DaoUtil.closeResources(conn,st,null);
		}

	}

}
