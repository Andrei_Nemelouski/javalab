package com.epam.backend.db.dao;

import com.epam.backend.entity.TagTO;
/**
 * DAO interface for operations with Tag table
 * 
 * @author Andrei_Nemelouski
 *
 */
public interface ITagDAO extends IDao<TagTO> {
	

}
