package com.epam.backend.db.dao.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

/**
 * Util class for all DAO classes common operations
 * 
 * @author Andrei_Nemelouski
 *
 */
public class DaoUtil {

	private static Logger logger = Logger.getLogger(DaoUtil.class);

	/**
	 * Method for sql delete command creation
	 * 
	 * @param command
	 *            SQL command ended with 'IN'
	 * @param id
	 *            id's array
	 * @return completed SQL command
	 */
	static String createRemoveCommand(String command, int[] id) {
		StringBuffer sb = new StringBuffer(command);
		sb.append("(");
		for (int i = 0; i < id.length; i++) {
			sb.append(id[i]);
			if (i != (id.length - 1)) {
				sb.append(", ");
			} else {
				sb.append(")");
			}
		}
		return sb.toString();
	}

	/**
	 * Frees resources.
	 * @param conn {@code Connection} to close. Can be null.
	 * @param st {@code Statement} to close. Can be null.
	 * @param rs {@code ResultSet} to close. Can be null.
	 */
	static void closeResources(Connection conn, Statement st, ResultSet rs) {
		if (rs != null) closeResultSet(rs);
		if (st != null) closeStatement(st);
		if (conn != null) closeConnection(conn);			
	}
	
	static void closeResources(Connection conn, Statement st) {
		closeResources(conn, st, null);
	}

	/**
	 * Closes {@code ResultSet}
	 * 
	 * @param rs
	 *            {@code ResultSet} to close
	 *
	 */
	private static void closeResultSet(ResultSet rs){
		try {
			rs.close();
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
	}

	/**
	 * Closes {@code Statement}
	 * 
	 * @param statement
	 *            {@code Statement} to close
	 * 
	 */
	private static void closeStatement(Statement statement){
		try {
			statement.close();
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
	}
	/**
	 * Closes {@code Connection}
	 * 
	 * @param conn
	 *            {@code Connection} to close
	 * 
	 */
	private static void closeConnection(Connection conn){
		try {
			conn.close();
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
	}
	
	

}
