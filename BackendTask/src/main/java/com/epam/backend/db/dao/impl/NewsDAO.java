package com.epam.backend.db.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.epam.backend.db.dao.INewsDAO;
import com.epam.backend.entity.NewsTO;
import com.epam.backend.exceptions.DAOException;
import com.epam.backend.mapper.IMapper;

/**
 * INewsDAO interface implementation class for operations with News table.
 * 
 * @author Andrei_Nemelouski
 *
 */
public class NewsDAO implements INewsDAO {

	private final String CREATE = "INSERT INTO news (NEWS_ID, SHORT_TEXT, FULL_TEXT, TITLE, CREATION_DATE, MODIFICATION_DATE) VALUES (NEWS_ID_SEQ.nextval, ?, ?, ?, ?, ?)";
	private final String UPDATE = "UPDATE news SET SHORT_TEXT=?, FULL_TEXT=?, TITLE=?, CREATION_DATE=?, MODIFICATION_DATE=? WHERE news_id=?";
	private final String DELETE = "DELETE FROM news WHERE news_id IN ";
	private final String DELETE_NEWS_AUTHOR = "DELETE FROM news_author WHERE news_id IN";
	private final String DELETE_NEWS_TAG = "DELETE FROM news_tag WHERE news_id IN";
	private final String DELETE_COMMENTS = "DELETE FROM comments WHERE news_id IN";
	private final String READ_BY_ID = "SELECT news.news_id, news.short_text, news.full_text, news.title, news.creation_date, news.modification_date FROM news WHERE news_id=?";
	private final String READ_ALL = "select n.news_id, n.short_text, n.full_text, n.title, n.creation_date, n.modification_date, count(c.news_id) AS cntd FROM news n "
			+ "left join comments c ON n.news_id = c.news_id "
			+ "group by n.news_id, n.creation_date, n.full_text, n.modification_date, n.short_text, n.title, c.news_id  "
			+ "order by cntd Desc";
	private final String READ_BY_AUTHOR_ID = "SELECT n.news_id, n.short_text, n.full_text, n.title, n.creation_date, n.modification_date FROM News n, News_Author na WHERE n.news_id=na.news_id AND na.author_id=?";
	private final String READ_BY_TAG_ID = "SELECT n.news_id, n.short_text, n.full_text, n.title, n.creation_date, n.modification_date FROM News n, News_Tag nt WHERE n.news_id=nt.news_id AND nt.tag_id=?";
	private final String[] GENERATED_ID = { "NEWS_ID" };
	@Autowired
	@Qualifier("dataSource")
	private DataSource dataSource;
	@Autowired
	private IMapper<NewsTO> newsMapper;

	/**
	 * Creates new record in the {@code News} table
	 * 
	 * @param obj
	 *            Entity transfer object
	 * @return id of created {@code News}
	 * @throws DAOException
	 *             exception that handles SQLException from database
	 */
	@Override
	public int create(NewsTO news) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int result = 0;
		try {
			conn = dataSource.getConnection();
			ps = conn.prepareStatement(CREATE, GENERATED_ID);
			ps.setString(1, news.getShortText());
			ps.setString(2, news.getFullText());
			ps.setString(3, news.getTitle());
			ps.setTimestamp(4, news.getCreationDate());
			ps.setDate(5, news.getModificationDate());
			ps.executeUpdate();
			rs = ps.getGeneratedKeys();
			while (rs.next()) {
				result = rs.getInt(1);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DaoUtil.closeResources(conn,ps,rs);
		}
		return result;
	}

	/**
	 * Returns all records from {@code News} table
	 * 
	 * @return {@code List} of entities
	 * @throws DAOException
	 *             exception that handles SQLException from database
	 */
	@Override
	public List<NewsTO> read() throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;		
		List<NewsTO> list = new LinkedList<>();
		try {
			conn = dataSource.getConnection();
			ps = conn.prepareStatement(READ_ALL);
			rs = ps.executeQuery();
			while (rs.next()) {
				NewsTO news = newsMapper.construct(rs);
				list.add(news);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DaoUtil.closeResources(conn,ps,rs);
		}
		return list;

	}

	/**
	 * Fetch news by id
	 * 
	 * @param id
	 *            news id
	 * @return News transfer object
	 * @throws DAOException
	 *             exception that handles SQLException from database.
	 */
	@Override
	public NewsTO read(int id) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;		
		NewsTO news = null;
		try {
			conn = dataSource.getConnection();
			ps = conn.prepareStatement(READ_BY_ID);
			ps.setInt(1, id);
			rs = ps.executeQuery();
			while (rs.next()) {
				news = newsMapper.construct(rs);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DaoUtil.closeResources(conn,ps,rs);
		}
		return news;
	}

	/**
	 * Updates table {@code News} according to {@code obj} contents
	 * 
	 * @param obj
	 *            Entity transfer object
	 * @return number of updated rows
	 * @throws DAOException
	 *             exception that handles SQLException from database
	 */
	@Override
	public int update(NewsTO news) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = dataSource.getConnection();
			ps = conn.prepareStatement(UPDATE);
			ps.setString(1, news.getShortText());
			ps.setString(2, news.getFullText());
			ps.setString(3, news.getTitle());
			ps.setTimestamp(4, news.getCreationDate());
			ps.setDate(5, news.getModificationDate());
			ps.setInt(6, news.getId());
			return ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DaoUtil.closeResources(conn,ps,null);
		}
	}

	/**
	 * 
	 * Deletes all rows with id from {@code id} array
	 * 
	 * @param id
	 *            array of rows id's
	 * @throws DAOException
	 *             exception that handles SQLException from database.
	 */
	@Override
	public void delete(int[] id) throws DAOException {
		Connection conn = null;
		Statement st = null;
		String news_command = DaoUtil.createRemoveCommand(DELETE, id);
		String news_aut_command = DaoUtil.createRemoveCommand(
				DELETE_NEWS_AUTHOR, id);
		String news_tag_command = DaoUtil.createRemoveCommand(DELETE_NEWS_TAG,
				id);
		String news_comments = DaoUtil.createRemoveCommand(DELETE_COMMENTS, id);
		try {
			conn = dataSource.getConnection();
			st = conn.createStatement();
			st.addBatch(news_tag_command);
			st.addBatch(news_aut_command);
			st.addBatch(news_comments);
			st.addBatch(news_command);
			st.executeBatch();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DaoUtil.closeResources(conn,st,null);
		}

	}

	/**
	 * Returns all {@code News} of author with {@code id}
	 * 
	 * @param id
	 *            Author id in table
	 * @return All authors news
	 * @throws DAOException
	 *             exception that handles SQLException from database
	 */
	@Override
	public List<NewsTO> fetchByAuthorId(int id) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = dataSource.getConnection();
			ps = conn.prepareStatement(READ_BY_AUTHOR_ID);
			ps.setInt(1, id);
			rs = ps.executeQuery();
			List<NewsTO> news = new ArrayList<>();
			while (rs.next()) {
				news.add(newsMapper.construct(rs));
			}
			return news;
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DaoUtil.closeResources(conn,ps,rs);
		}
	}

	/**
	 * Returns all {@code News} related with tag with {@code id}
	 * 
	 * @param id
	 *            Tag id in table
	 * @return All news related with tag
	 * @throws DAOException
	 *             exception that handles SQLException from database.
	 */
	@Override
	public List<NewsTO> fetchByTagId(int id) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = dataSource.getConnection();
			ps = conn.prepareStatement(READ_BY_TAG_ID);
			ps.setInt(1, id);
			rs = ps.executeQuery();
			List<NewsTO> news = new ArrayList<>();
			while (rs.next()) {
				news.add(newsMapper.construct(rs));
			}
			return news;
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DaoUtil.closeResources(conn,ps,rs);
		}
	}

}
