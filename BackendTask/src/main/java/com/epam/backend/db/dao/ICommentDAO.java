package com.epam.backend.db.dao;

import com.epam.backend.entity.CommentTO;

/**
 * DAO interface for operations with Comments table
 * @author Andrei_Nemelouski
 *
 */
public interface ICommentDAO extends IDao<CommentTO> {
	
	

}
