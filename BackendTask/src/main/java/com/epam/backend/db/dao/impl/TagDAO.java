package com.epam.backend.db.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.epam.backend.db.dao.ITagDAO;
import com.epam.backend.entity.TagTO;
import com.epam.backend.exceptions.DAOException;
import com.epam.backend.mapper.IMapper;

public class TagDAO implements ITagDAO {

	//private final String ADD_TAG = "INSERT INTO tag(tag_id,tag_name) VALUES (tag_id_seq.nextval, ?)";
	//private final String ADD_NEWS_TAG = "INSERT INTO News_Tag(news_tag_id,news_id,tag_id) VALUES (news_tag_id_seq.nextval, ?, ?)";
	private final String READ = "SELECT tag.tag_id, tag.tag_name FROM tag";
	private final String UPDATE = "UPDATE tag SET tag_name=? WHERE tag_id=?";
	private final String DELETE_TAG = "DELETE FROM tag WHERE tag_id IN ";
	private final String DELETE_NEWS_TAG = "DELETE FROM news_tag WHERE tag_id IN ";
	//private final String[] GENERATED_ID = { "TAG_ID" };
	private final String CREATE = "declare "
			+ "tagName NVARCHAR2(20) := ?; "
			+ "cur_state NVARCHAR2(20); "
			+ "resultId NUMBER(10); "
			+ "newsId NUMBER(10) := ?; "
			+ "begin "
			+ "select tag.tag_name INTO cur_state FROM tag WHERE tag_name=tagName; "
			+ "exception "
			+ "WHEN NO_DATA_FOUND THEN " 
			+ "cur_state := null; "
			+ "if (cur_state is NULL) then "
			+ "begin "
			+ "insert into tag(tag_id, tag_name) values (tag_id_seq.nextval, tagName); "
			+ "INSERT INTO News_Tag(news_tag_id, news_id, tag_id) VALUES (news_tag_id_seq.nextval, newsId, tag_id_seq.currval); "
			+ "end; " + "end if; " 
			+ "select tag.tag_id INTO resultId FROM tag WHERE tag_name=tagName; "
			+ "? := resultId; "			
			+ "end;";
	@Autowired
	@Qualifier("dataSource")
	private DataSource dataSource;
	@Autowired
	private IMapper<TagTO> tagMapper;

	/**
	 * Creates new record in the {@code Tag} table
	 * 
	 * @param obj
	 *            Entity transfer object
	 * @return id of created {@code Tag}
	 * @throws DAOException
	 *             exception that handles SQLException from database.
	 */
	@Override
	public int create(TagTO obj) throws DAOException {
		Connection conn = null;
		//PreparedStatement ps = null;
		//ResultSet rs = null;
		CallableStatement cs = null;
		int result = 0;
		try {
			conn = dataSource.getConnection();
			cs = conn.prepareCall(CREATE);
			cs.setString(1, obj.getTagName());
			cs.setInt(2, obj.getNewsId());
			cs.registerOutParameter(3, Types.INTEGER);
			cs.execute();
			result = cs.getInt(3);
			/*result = isPresent(obj.getTagName());
			if (result == 0) {
				ps = conn.prepareStatement(ADD_TAG, GENERATED_ID);
				ps.setString(1, obj.getTagName());
				ps.executeUpdate();
				rs = ps.getGeneratedKeys();
				while (rs.next()) {
					result = rs.getInt(1);
				}
				DaoUtil.closeResources(null, ps, null);
				ps = conn.prepareStatement(ADD_NEWS_TAG);
				ps.setInt(1, obj.getNewsId());
				ps.setInt(2, result);
				ps.executeUpdate();
			}*/
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DaoUtil.closeResources(conn, cs);
		}
		return result;
	}

	/*private int isPresent(String name) throws DAOException {
		List<TagTO> result = read();
		for (TagTO i : result) {
			if (name.equals(i.getTagName()))
				return i.getId();
		}
		return 0;
	}*/

	/**
	 * Returns all records from {@code Tag} table
	 * 
	 * @return {@code List} of entities
	 * @throws DAOException
	 *             exception that handles SQLException from database.
	 */
	@Override
	public List<TagTO> read() throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<TagTO> list = new ArrayList<>();
		try {
			conn = dataSource.getConnection();
			ps = conn.prepareStatement(READ);
			rs = ps.executeQuery();
			while (rs.next()) {
				list.add(tagMapper.construct(rs));
			}
			return list;
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DaoUtil.closeResources(conn, ps, rs);
		}

	}

	/**
	 * Updates table {@code Tag} according to {@code obj} contents
	 * 
	 * @param obj
	 *            Entity transfer object
	 * @return number of updated rows
	 * @throws DAOException
	 *             exception that handles SQLException from database.
	 */
	@Override
	public int update(TagTO obj) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = dataSource.getConnection();
			ps = conn.prepareStatement(UPDATE);
			ps.setString(1, obj.getTagName());
			ps.setInt(2, obj.getId());
			return ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DaoUtil.closeResources(conn, ps, null);
		}

	}

	/**
	 * 
	 * Deletes all rows with id from {@code id} array
	 * 
	 * @param id
	 *            array of rows id's
	 * @throws DAOException
	 *             exception that handles SQLException from database.
	 */
	@Override
	public void delete(int[] id) throws DAOException {
		Connection conn = null;
		Statement st = null;
		String tag = DaoUtil.createRemoveCommand(DELETE_TAG, id);
		String news_tag = DaoUtil.createRemoveCommand(DELETE_NEWS_TAG, id);
		try {
			conn = dataSource.getConnection();
			st = conn.createStatement();
			st.addBatch(news_tag);
			st.addBatch(tag);
			st.executeBatch();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DaoUtil.closeResources(conn, st, null);
		}

	}

}
