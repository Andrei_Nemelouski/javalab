package com.epam.backend.db.dao;

import java.util.List;

import com.epam.backend.entity.NewsTO;
import com.epam.backend.exceptions.DAOException;

/**
 * DAO interface for operations with News table
 * 
 * @author Andrei_Nemelouski
 *
 */
public interface INewsDAO extends IDao<NewsTO> {

	/**
	 * Fetch news by id
	 * 
	 * @param id
	 *            news id
	 * @return News transfer object
	 * @throws DAOException
	 *             exception that handles SQLException from database.
	 */
	NewsTO read(int id) throws DAOException;

	/**
	 * Returns all {@code News} with tag with {@code id}
	 * 
	 * @param id
	 *            Tag id in table
	 * @return All news related with tag
	 * @throws DAOException
	 *             exception that handles SQLException from database.
	 */
	List<NewsTO> fetchByTagId(int id) throws DAOException;

	/**
	 * Returns all {@code News} of author with {@code id}
	 * 
	 * @param id
	 *            Author id in table
	 * @return All authors news
	 * @throws DAOException
	 *             exception that handles SQLException from database.
	 */
	List<NewsTO> fetchByAuthorId(int id) throws DAOException;

}
