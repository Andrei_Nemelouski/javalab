package com.epam.backend.db.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.epam.backend.db.dao.IAuthorDAO;
import com.epam.backend.entity.AuthorTO;
import com.epam.backend.exceptions.DAOException;
import com.epam.backend.mapper.IMapper;

/**
 * DAO class for operations with Authors table.
 * 
 * @author Andrei_Nemelouski
 *
 */
public class AuthorDAO implements IAuthorDAO {

	private final String READ = "SELECT author.author_id, author.name FROM Author";
	private final String UPDATE = "UPDATE Author SET name=? WHERE author_id=?";
	private final String DELETE_AUTHOR = "DELETE FROM author WHERE author_id IN ";
	private final String DELETE_NEWS_AUTHOR = "DELETE FROM news_author WHERE author_id IN ";
	private final String CREATE = "declare "
			+ "aut_name NVARCHAR2(20) := ?; "
			+ "cur_state NVARCHAR2(20); "
			+ "resultId NUMBER(10); "
			+ "newsId NUMBER(10) := ?; "
			+ "begin "
			+ "select author.name INTO cur_state FROM author WHERE name=aut_name; "
			+ "exception "
			+ "WHEN NO_DATA_FOUND THEN " 
			+ "cur_state := null; "
			+ "if (cur_state is NULL) then "
			+ "begin "
			+ "insert into author(author_id, name) values (author_id_seq.nextval, aut_name); "
			+ "INSERT INTO News_Author(news_author_id, news_id, author_id) VALUES (news_auth_id_seq.nextval, newsId, author_id_seq.currval); "
			+ "end; " + "end if; " 
			+ "select author.author_id INTO resultId FROM author WHERE name=aut_name; "
			+ "? := resultId; "			
			+ "end;";
	@Autowired
	@Qualifier("dataSource")
	private DataSource dataSource;
	@Autowired
	private IMapper<AuthorTO> authorMapper;

	/**
	 * Creates new record in the {@code Author, News_Author} table
	 * 
	 * @param obj
	 *            Entity transfer object
	 * @return id of created {@code Author}
	 * @throws DAOException
	 *             exception that handles SQLException from database
	 */
	@Override
	public int create(AuthorTO obj) throws DAOException {
		Connection conn = null;
		CallableStatement cs = null;
		int result = 0;
		try {
			conn = dataSource.getConnection();
			cs = conn.prepareCall(CREATE);
			cs.setString(1, obj.getName());
			cs.setInt(2, obj.getNewsId());
			cs.registerOutParameter(3, Types.INTEGER);
			cs.execute();
			result = cs.getInt(3);
			} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DaoUtil.closeResources(conn, cs);
		}
		return result;
	}

	/**
	 * Returns all records from {@code Author} table
	 * 
	 * @return {@code List} of entities
	 * @throws DAOException
	 *             exception that handles SQLException from database
	 */
	@Override
	public List<AuthorTO> read() throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<AuthorTO> list = new ArrayList<>();
		try {
			conn = dataSource.getConnection();
			ps = conn.prepareStatement(READ);
			rs = ps.executeQuery();
			while (rs.next()) {
				list.add(authorMapper.construct(rs));
			}
			return list;
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DaoUtil.closeResources(conn, ps, rs);
		}

	}

	/**
	 * Updates table {@code Author} according to {@code obj} contents
	 * 
	 * @param obj
	 *            Entity transfer object
	 * @return number of updated rows
	 * @throws DAOException
	 *             exception that handles SQLException from database
	 */
	@Override
	public int update(AuthorTO obj) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = dataSource.getConnection();
			ps = conn.prepareStatement(UPDATE);
			ps.setString(1, obj.getName());
			ps.setInt(2, obj.getId());
			return ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DaoUtil.closeResources(conn, ps, null);
		}

	}

	/**
	 * 
	 * Deletes all rows with id from {@code id} array
	 * 
	 * @param id
	 *            array of rows id's
	 * @throws DAOException
	 *             exception that handles SQLException from database
	 */
	@Override
	public void delete(int[] id) throws DAOException {
		Connection conn = null;
		Statement st = null;
		String author_in = DaoUtil.createRemoveCommand(DELETE_AUTHOR, id);
		String news_author_in = DaoUtil.createRemoveCommand(DELETE_NEWS_AUTHOR,
				id);
		try {
			conn = dataSource.getConnection();
			st = conn.createStatement();
			st.addBatch(news_author_in);
			st.addBatch(author_in);
			st.executeBatch();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DaoUtil.closeResources(conn, st, null);
		}

	}
}
