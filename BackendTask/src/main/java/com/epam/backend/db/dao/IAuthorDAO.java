package com.epam.backend.db.dao;

import com.epam.backend.entity.AuthorTO;

/**
 * DAO interface for operations with Authors table
 * 
 * @author Andrei_Nemelouski
 *
 */
public interface IAuthorDAO extends IDao<AuthorTO> {
	

}
