package com.epam.backend.entity;

/**
 * Tag's table row representation
 * 
 * @author Andrei_Nemelouski
 *
 */
public class TagTO {

	/**
	 * Table's primary key
	 */
	private int id;
	/**
	 * related news id
	 */
	private int newsId;
	/**
	 * tag_name column
	 */
	private String tagName;
	
	// Getters and setters

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getNewsId() {
		return newsId;
	}

	public void setNewsId(int newsId) {
		this.newsId = newsId;
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	@Override
	public String toString() {
		return "TagTO [id=" + id + ", newsId=" + newsId + ", tagName="
				+ tagName + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + newsId;
		result = prime * result + ((tagName == null) ? 0 : tagName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TagTO other = (TagTO) obj;
		if (id != other.id)
			return false;
		if (newsId != other.newsId)
			return false;
		if (tagName == null) {
			if (other.tagName != null)
				return false;
		} else if (!tagName.equals(other.tagName))
			return false;
		return true;
	}

}
