$(document)
		.ready(
				function() {
					var expanded = false;
					var edit_expanded = false;
					$('#delete')
							.on(
									'click',
									function() {
										var field = $('.delete-check');
										for (var loop = 0; loop < field.length; loop++) {
											if (field[loop].checked) {
												return confirm("Are you sure you want to delete selected articles?");
											}
										}
										alert("At least one checkbox must be selected!");
										return false;

									});

					$('.selectBox').on('click', function() {
						if (!expanded) {
							$('#checkboxes').css('display', 'block');
							expanded = true;
						} else {
							$('#checkboxes').css('display', 'none');
							expanded = false;
						}
					});
					
					$('#edit').on('click', function(){
						$('#edit').hide();
						$('.edit-block').show();
						});
				});
