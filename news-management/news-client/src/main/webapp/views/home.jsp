<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:set var="pattern">
	<s:message code="date.pattern" />
</c:set>
<div class="filter-wrapper">
	<sf:form action="/NewsManagement/list/filter" method="POST">
		<%-- <select name="authorId" class="author-filter">
			<option value="0"><s:message code="filter.author"/></option>
			<c:forEach var="author" items="${authors}">
				<c:choose>
					<c:when test="${author.id == filter.author.id}">
						<option value="${author.id}" selected><c:out
								value="${author.name}"></c:out></option>
					</c:when>
					<c:otherwise>
						<option value="${author.id}"><c:out
								value="${author.name}"></c:out></option>
					</c:otherwise>
				</c:choose>
			</c:forEach>
		</select>
		<div class="multiselect">
			<div class="selectBox">
				<select>
					<option><s:message code="filter.tag" /></option>
				</select>
				<div class="overSelect"></div>
			</div>
			<div id="checkboxes" class="checkboxes">
				<c:forEach var="tag" items="${tags}">
					<c:forEach var="filter_tag" items="${filter.tag}">
						<c:choose>
							<c:when test="${tag.id == filter_tag.id}">
								<label><input type="checkbox" name="tagId"
									value="${tag.id}" checked /> <c:out value="${tag.tagName}"></c:out></label>
							</c:when>
							<c:otherwise>
								<label><input type="checkbox" name="tagId"
									value="${tag.id}" /> <c:out value="${tag.tagName}"></c:out></label>
							</c:otherwise>
						</c:choose>

					</c:forEach>
				</c:forEach>
			</div>
		</div> --%>
		<select name="authorId" class="author-filter">
			<option value="0" selected><s:message code="filter.author"></s:message></option>
			<c:forEach var="author" items="${authors}">
				<option value="${author.id}"><c:out value="${author.name}"></c:out></option>
			</c:forEach>
		</select>
		<div class="multiselect">
			<div class="selectBox">
				<select >
					<option ><s:message code="filter.tag" /></option>
				</select>
				<div class="overSelect"></div>
			</div>

			<div id="checkboxes" class="checkboxes">
				<c:forEach var="tag" items="${tags}">
					<label><input type="checkbox" name="tagId"
						value="${tag.id}" /> <c:out value="${tag.tagName}"></c:out></label>
				</c:forEach>
			</div>
		</div>
		<input type="submit" value="<s:message code="filter.apply"/>">
	</sf:form>
</div>
<c:forEach var="newsList" items="${page.news}">
	<article class="short-desc">
		<div class="article-head">
			<h3>
				<c:out value="${newsList.title}"></c:out>
			</h3>
			<span class="article-author">(<s:message code="content.author"></s:message>
				<c:out value="${newsList.author.name}"></c:out>)
			</span> <span class="article-head-date"><fmt:formatDate
					pattern="${pattern}" value="${newsList.creationDate}" /></span>
		</div>
		<div class="article-content">
			<p>
				<c:out value="${newsList.shortText}"></c:out>
			</p>
		</div>
		<div class="article-info">
			<div class="article-tags">
				<c:forEach var="tags" items="${newsList.tag}">
					<div class="tag">
						<form action="/NewsManagement/list/filter" method="POST">
							<input type="hidden" name="tagId" value="${tags.id}" /> <input
								type="submit" value="${tags.tagName}" />
						</form>
					</div>
				</c:forEach>
			</div>
			<span class="article-comments-counter"><s:message
					code="content.comments"></s:message>(<c:out
					value="${newsList.commentsNumber}"></c:out>) </span> <a
				href="/NewsManagement/article/${newsList.id}"><s:message
					code="content.view"></s:message></a>
		</div>
	</article>
</c:forEach>

<div class="pagination-block">
	<c:forEach var="i" begin="1" end="${page.numberOfPages}">
		<a href="/NewsManagement/list/${i}"><c:out value="${i}" /></a>
	</c:forEach>
</div>
