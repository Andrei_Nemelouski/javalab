package com.epam.news.client.utils;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import com.epam.news.entity.Author;
import com.epam.news.entity.Tag;
import com.epam.news.entity.representation.Filter;

public class FilterUtils {

	public static Filter populateFilter(HttpSession session, long authorId,
			long[] tagId) {
		Filter filter = (Filter) session.getAttribute("filter");
		if (filter == null)
			filter = new Filter();
		if (authorId != 0) {
			Author auth = new Author();
			auth = new Author();
			auth.setId(authorId);
			filter.setAuthor(auth);
		} else {
			filter.setAuthor(null);
		}
		if (tagId != null) {
			List<Tag> tags = new ArrayList<>();
			for (int i = 0; i < tagId.length; i++) {
				Tag tag = new Tag();
				tag.setId(tagId[i]);
				tags.add(tag);
			}
			filter.setTag(tags);
		} else {
			filter.setTag(null);
		}
		return filter;
	}

	public static Filter populateFilter(HttpSession session) {
		Filter filter = (Filter) session.getAttribute("filter");
		if (filter == null)
			filter = new Filter();
		return filter;
	}

}
