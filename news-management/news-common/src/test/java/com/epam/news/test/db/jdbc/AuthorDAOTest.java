package com.epam.news.test.db.jdbc;

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;

import com.epam.news.db.dao.AuthorDAO;
import com.epam.news.entity.Author;
import com.epam.news.exceptions.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

/**
 * Test class for IAuthorDAO implementation.
 * 
 * @author Andrei_Nemelouski
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-config.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
@DatabaseSetup("/db_test_input.xml")
public class AuthorDAOTest {

	private static final Logger logger = Logger.getLogger(AuthorDAOTest.class);
	@Autowired
	@Qualifier("authorDao")
	private AuthorDAO dao;
	/**
	 * Read method test
	 */
	@Test
	public void testRead() {
		List<Author> result = null;
		try {
			result = dao.read();
		} catch (DAOException e) {
			logger.error(e.getMessage());
		}
		Assert.assertEquals(3, result.size());
	}
	
	@Test
	public void testReadById(){
		Author a = null;
		try {
			a = dao.readById(1);			
		} catch (DAOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Assert.assertEquals("Andy", a.getName());
	}
	
	@Test
	public void testReadByNewsId(){
		Author a = null;
		try {
			a = dao.readByNewsId(1);			
		} catch (DAOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Assert.assertEquals("Andy", a.getName());
		
	}

	/**
	 * Create test
	 */
	@Test
	public void testCreate() {
		Author aut = new Author();
		aut.setName("Mighty BLABL!!!");
		aut.setNewsId(2);
		long resultId = 0;
		Author result = null;
		try {
			resultId = dao.create(aut);			
			aut.setId(resultId);
			List<Author> resultList = dao.read();
			for(Author i:resultList){
				if(i.getId()==resultId) result = i;
			}
			if(result!=null){
				result.setNewsId(2);
			}
		} catch (DAOException e) {
			logger.error(e.getMessage());
		}
		
		Assert.assertEquals(aut,result);

	}

	/**
	 * Update test
	 */
	@Test
	@ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT, value = "/authorTest/update_result.xml")
	public void testUpdate() {
		Author aut = new Author();
		aut.setId(1);
		aut.setName("Pandy");
		try {
			dao.update(aut);
		} catch (DAOException e) {
			logger.error(e.getMessage());
		}
	}

	/**
	 * Delete test
	 */
	@Test
	public void testExpired() {
		long id = 1L;
		try {
			dao.setExpired(id);
			Author a = dao.readById(id);
			Assert.assertNull(a);
		} catch (DAOException e) {
			logger.error(e.getMessage());
		}

	}
}
