package com.epam.news.test.db.jdbc;

import java.sql.Timestamp;
import java.util.List;

import org.junit.Assert;
import org.junit.Ignore;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.news.db.dao.CommentDAO;
import com.epam.news.entity.Comment;
import com.epam.news.exceptions.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

/**
 * Test class for ICommentDAO implementation class
 * @author Andrei_Nemelouski
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-config.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
@DatabaseSetup("/db_test_input.xml")
public class CommentDAOTest{

	private static final Logger logger = Logger.getLogger(CommentDAOTest.class);
	@Autowired
	@Qualifier("commentDao")
	private CommentDAO dao;
	
	
	/**
	 * Create test
	 */
	@Test
	public void testCreate() {
		long resultId = 0;
		Comment com = new Comment();
		Comment result = null;
		com.setCreationDate(Timestamp.valueOf("2015-03-24 19:12:12.0"));
		com.setText("Comment");
		com.setNewsId(1);
		try {
			resultId = dao.create(com);
			com.setId(resultId);
			List<Comment> resultList = dao.read();
			for(Comment i:resultList){
				if(i.getId()==resultId) result = i;
			}
		} catch (DAOException e) {
			logger.error(e.getMessage());
		}
		Assert.assertEquals(com,result);
	}

	/**
	 * Read test
	 */
	@Test
	public void testRead() {
		List<Comment> result = null;
		try {
			result = dao.read();
		} catch (DAOException e) {
			logger.error(e.getMessage());
		}
		Assert.assertEquals(4, result.size());
	}
	
	@Test
	@Ignore
	public void testReadById(){
		
	}
	
	@Test
	public void testReadByNewsId(){
		List<Comment> result = null;
		try {
			result = dao.readByNewsId(1l);
		} catch (DAOException e) {
			logger.error(e.getMessage());
		}
		Assert.assertEquals(2, result.size());
	}

	/**
	 * Update test
	 */
	@Test
	@ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT, value = "/commentTest/update_result.xml")	
	public void testUpdate() {
		Comment c = new Comment();
		c.setId(1);
		c.setText("Goodbye");
		c.setCreationDate(Timestamp.valueOf("2015-03-24 19:12:12.0"));
		try {
			dao.update(c);
		} catch (DAOException e) {
			logger.error(e.getMessage());
		} 
	}

	/**
	 * Delete test
	 */
	@Test
	@ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT, value = "/commentTest/delete_result.xml")	
	public void testDelete() {
		long id = 1;
		try {
			dao.delete(id);
		} catch (DAOException e) {
			logger.error(e.getMessage());
		} 
	}

}
