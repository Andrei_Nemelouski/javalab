package com.epam.news.test.db.hiber;

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.news.db.dao.NewsDAO;
import com.epam.news.entity.dto.NewsTO;
import com.epam.news.exceptions.DAOException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-config.xml" })
public class HiberNewsDaoTest {

	private static final Logger logger = Logger.getLogger(HiberNewsDaoTest.class);
	@Autowired
	@Qualifier("hiberNewsDao")
	private NewsDAO hiberDao;
	
	@Test
	public void readByIdTest() throws DAOException{
		NewsTO m = hiberDao.readById(1);
		System.out.println(m.toString());
	}
	
	@Test
	public void readTest() throws DAOException{
		List<NewsTO> m = hiberDao.read();
		System.out.println(m.get(1).toString());
	}

}
