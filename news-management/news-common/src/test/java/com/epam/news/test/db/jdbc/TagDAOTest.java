package com.epam.news.test.db.jdbc;

import java.util.List;

import org.junit.Assert;
import org.junit.Ignore;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.news.db.dao.TagDAO;
import com.epam.news.entity.Tag;
import com.epam.news.exceptions.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

/**
 * Test class for ITagDAO implementation.
 * 
 * @author Andrei_Nemelouski
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-config.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
@DatabaseSetup("/db_test_input.xml")
public class TagDAOTest {

	private static final Logger logger = Logger.getLogger(TagDAOTest.class);
	@Autowired
	@Qualifier("tagDao")
	private TagDAO dao;
	

	/**
	 * Create test
	 */
	@Test
	public void testCreate() {
		Tag result = null;
		Tag tto = new Tag();
		tto.setNewsId(1);
		tto.setTagName("Good");
		long resultId = 0;
		try {
			resultId = dao.create(tto);
			List<Tag> resultList = dao.read();
			for(Tag i:resultList){
				if(i.getId() == resultId) result = i;
			}
		} catch (DAOException e) {
			logger.error(e);
		}
		Assert.assertNotEquals(tto, result);

	}

	/**
	 * Read test
	 */
	@Test
	public void testRead() {
		List<Tag> result = null;
		try {
			result = dao.read();
		} catch (DAOException e) {
			logger.error(e.getMessage());
		}
		Assert.assertEquals(3, result.size());
	}
	
	@Test
	@Ignore
	public void testReadById(){
		
	}
	
	@Test
	public void testReadByNewsId(){
		List<Tag> result = null;
		try {
			result = dao.readByNewsId(1l);
		} catch (DAOException e) {
			logger.error(e.getMessage());
		}
		Assert.assertEquals(2, result.size());
	}

	/**
	 * Update test
	 */
	@Test
	@ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT, value = "/tagTest/update_result.xml")	
	public void testUpdate() {
		Tag tag = new Tag();
		tag.setId(1);
		tag.setTagName("Good");
		try {
			dao.update(tag);
		} catch (DAOException e) {
			logger.error(e.getMessage());
		} 
	}

	/**
	 * Delete test
	 */
	@Test
	@ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT, value = "/tagTest/delete_result.xml")	
	public void testDelete() {
		long id = 1;
		try {
			dao.delete(id);			
		} catch (DAOException e) {
			logger.error(e.getMessage());
		} 
	}

}
