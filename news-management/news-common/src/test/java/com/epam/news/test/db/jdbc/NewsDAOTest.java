package com.epam.news.test.db.jdbc;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.news.db.dao.NewsDAO;
import com.epam.news.entity.dto.NewsTO;
import com.epam.news.entity.representation.Filter;
import com.epam.news.exceptions.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

/**
 * Test class for INewsDAO interface implementation
 * 
 * @author Andrei_Nemelouski
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-config.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
@DatabaseSetup("/db_test_input.xml")
public class NewsDAOTest {

	private static final Logger logger = Logger.getLogger(NewsDAOTest.class);
	@Autowired
	private NewsDAO dao;
	private Filter filter;

	{
		long[] tags = { 1 };
		filter = new Filter();
		//filter.setAuthorId(1l);
		// filter.setCommentsNumber(1);
		//filter.setTagId(tags);
		/*filter.setCurrentPage(1);
		filter.setNewsPerPage(2);*/

	}

	/**
	 * Create test
	 */
	@Test
	public void testCreate() {
		long resultId = 0;
		NewsTO result = null;
		NewsTO news = new NewsTO();
		news.setShortText("Bye");
		news.setFullText("Bye bye!");
		news.setTitle("Another greeting");
		news.setCreationDate(Timestamp.valueOf("2015-03-24 19:12:12.0"));
		news.setModificationDate(Date.valueOf("2012-02-12"));
		try {
			resultId = dao.create(news);
			result = dao.readById(resultId);
		} catch (DAOException e) {
			logger.error(e.getMessage());
		}
		Assert.assertNotEquals(news, result);

	}

	/**
	 * News list read test
	 */
	@Test
	public void testRead() {
		List<NewsTO> result = null;
		try {
			result = dao.read();
		} catch (DAOException e) {
			logger.error(e.getMessage());
		}
		Assert.assertEquals(2, result.size());
	}

	/**
	 * Single news read test
	 */
	@Test
	public void testReadById() {
		NewsTO news = null;
		try {
			news = dao.readById(1);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		Assert.assertEquals("Greeting", news.getTitle());

	}

	@Test
	public void testFilteredRead() {
		List<NewsTO> news = null;
		try {
			news = dao.read(3,1,filter);
			System.out.println(news.toString());
		} catch (DAOException e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		Assert.assertEquals("Greeting", news.get(0).getTitle());

	}

	@Test
	public void testReadNext() {
		/*long id = 1;
		long result = 0l;
		try {
			result = dao.readNext(id, filter);
			//System.out.println(result);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		Assert.assertEquals(result, 2l);*/

	}

	@Test
	public void testReadPrev() {
		/*long id = 1;
		long result = 0l;
		try {
			result = dao.readPrev(id, filter);
			//System.out.println(result);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		Assert.assertEquals(result, 0l);*/

	}

	@Test
	public void testCountNews() {
		int result = 0;
		try {
			result = dao.countNews(filter);
			System.out.println(result);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}		
		Assert.assertEquals(result, 2);

	}

	/**
	 * Delete test
	 */
	@Test
	@ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT, value = "/newsTest/delete_result.xml")
	public void testDelete() {
		long[] test = { 1 };
		try {
			dao.delete(test);
		} catch (DAOException e) {
			logger.error(e.getMessage());
		}
	}

	@Test
	@ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT, value = "/newsTest/update_result.xml")
	public void testUpdate() {
		NewsTO news = new NewsTO();
		news.setId(1);
		news.setShortText("Goodbye");
		news.setFullText("Have a nice day!");
		news.setTitle("Greeting");
		news.setCreationDate(Timestamp.valueOf("2015-03-24 19:12:12.0"));
		news.setModificationDate(Date.valueOf("2012-02-12"));
		try {
			dao.update(news);
		} catch (DAOException e) {
			logger.error(e.getMessage());
		}

	}

}
