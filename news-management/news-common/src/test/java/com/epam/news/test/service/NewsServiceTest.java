package com.epam.news.test.service;

import java.util.List;

import org.junit.Assert;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.news.db.dao.NewsDAO;
import com.epam.news.entity.Author;
import com.epam.news.entity.dto.NewsTO;
import com.epam.news.entity.representation.Filter;
import com.epam.news.entity.Tag;
import com.epam.news.exceptions.DAOException;
import com.epam.news.exceptions.ServiceException;
import com.epam.news.service.INewsService;
/**
 * News service test class
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:**/test-config.xml" })
public class NewsServiceTest {

	private final static Logger logger = Logger
			.getLogger(NewsServiceTest.class);
	@Spy
	@Autowired
	@Qualifier("newsDao")
	private NewsDAO dao;
	@Mock
	private Tag tag;
	@Mock
	private Author aut;	
	@Mock
	private List<NewsTO> list;
	@Mock
	private NewsTO news;
	@InjectMocks
	@Autowired
	@Qualifier("newsService")
	private INewsService serv;

	/**
	 * Init mocks
	 */
	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Add test
	 */
	@Test
	public void testCreate() {
		try {
			Mockito.doReturn(1L).when(dao).create(news);
			serv.create(news);			
			Mockito.verify(dao).create(news);					
		} catch (DAOException e) {
			logger.error(e.getMessage());
		} catch (ServiceException e) {
			logger.error(e.getMessage());
		}

	}

	/**
	 * Edit test
	 */
	@Test
	public void testEdit() {
		try {
			serv.update(news);
			Mockito.verify(dao).update(news);
		} catch (DAOException e) {
			logger.error(e.getMessage());
		} catch (ServiceException e) {
			logger.error(e.getMessage());
		}

	}

	/**
	 * Delete test
	 */
	@Test
	public void testDelete() {
		long[] del = { 1 };
		try {
			serv.delete(del);
			Mockito.verify(dao).delete(del);			
		} catch (DAOException e) {
			logger.error(e.getMessage());
		} catch (ServiceException e) {
			logger.error(e.getMessage());
		}

	}
	
    /*List<NewsTO> read(Filter filter) throws ServiceException;
	
	long readNext(long id, Filter filter) throws ServiceException;
	
	long readPrev(long id, Filter filter) throws ServiceException;
	
	int countNews(Filter filter) throws ServiceException;
	
	void delete(long[] delete) throws ServiceException;
	
	NewsTO readById(long id) throws ServiceException;

	*//**
	 * News list obtaining test
	 *//*
	@Test
	public void testReadAll() {
		try {
			Mockito.doReturn(list).when(dao).read();
			List<NewsTO> actual = serv.read();
			Mockito.verify(dao).read();
			Assert.assertEquals(list, actual);
		} catch (DAOException e) {
			logger.error(e.getMessage());
		} catch (ServiceException e) {
			logger.error(e.getMessage());
		}
	}

	*//**
	 * Single news obtaining test
	 *//*
	@Test
	public void testReadSingleNews() {
		try {
			Mockito.doReturn(news).when(dao).readById(1);
			NewsTO actual = serv.readSingleNews(1);
			Mockito.verify(dao).readById(1);
			Assert.assertEquals(news, actual);
		} catch (DAOException e) {
			logger.error(e.getMessage());
		} catch (ServiceException e) {
			logger.error(e.getMessage());
		}

	}
	
	*//**
	 * Test method for getting all news by author
	 *//*
	@Test
	public void testFetchByAuthorId() {
		List<NewsTO> result = new ArrayList<>();
		result.add(new NewsTO());
		try {
			Mockito.doReturn(1).when(aut).getId();
			Mockito.doReturn(list).when(dao).readByAuthorId(1);
			List<NewsTO> actual = serv.readByAuthorId(aut.getId());
			Assert.assertEquals(list, actual);
			Mockito.verify(aut).getId();
			Mockito.verify(dao).readByAuthorId(1);
		} catch (DAOException e) {
			logger.error(e.getMessage());
		} catch (ServiceException e) {
			logger.error(e.getMessage());
		}
	}

	*//**
	 * Test method for getting all news by tag
	 *//*
	@Test
	public void testFetchByTagId() {
		int[] id = {1};
		try {
			Mockito.doReturn(list).when(dao).readByTagId(id);
			List<NewsTO> actual = serv.readByTagId(id);
			Mockito.verify(dao).readByTagId(id);
			Assert.assertEquals(list, actual);
		} catch (DAOException e) {
			logger.error(e.getMessage());
		} catch (ServiceException e) {
			logger.error(e.getMessage());
		}
	}*/
}
