package com.epam.news.test.service;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.news.db.dao.AuthorDAO;
import com.epam.news.entity.Author;
import com.epam.news.exceptions.DAOException;
import com.epam.news.exceptions.ServiceException;
import com.epam.news.service.IAuthorService;
import com.epam.news.service.impl.AuthorService;

/**
 * Test class for Author service class.
 * 
 * @author Andrei_Nemelouski
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:**/test-config.xml" })
public class AuthorServiceTest {

	@Spy
	@Autowired
	@Qualifier("authorDao")
	private AuthorDAO dao;
	@Mock
	private Author aut;
	@InjectMocks
	@Autowired
	@Qualifier("authorService")
	private IAuthorService serv;

	private final static Logger logger = Logger.getLogger(AuthorService.class);

	/**
	 * Mocks initiation
	 */
	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);		
	}

	/**
	 * Add method test
	 */
	@Test
	public void testAdd() {
		try {
			Mockito.doReturn(1L).when(dao).create(aut);
			long actual = serv.create(aut);
			long expected = 1L;
			Assert.assertEquals(actual, expected);
			Mockito.verify(dao).create(aut);
		} catch (DAOException e) {
			logger.error(e.getMessage());
		} catch (ServiceException e) {
			logger.error(e.getMessage());
		}
	}	
}
