package com.epam.news.test.service;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.news.db.dao.TagDAO;
import com.epam.news.entity.Tag;
import com.epam.news.exceptions.DAOException;
import com.epam.news.exceptions.ServiceException;
import com.epam.news.service.ITagService;
import com.epam.news.service.impl.TagService;

/**
 * Test class for tag service class
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:**/test-config.xml" })
public class TagServiceTest {
	
	@Spy
	@Autowired
	@Qualifier("tagDao")
	private TagDAO dao;
	@Mock
	private Tag tag;
	@InjectMocks
	@Autowired
	@Qualifier("tagService")
	private ITagService serv;

	private final static Logger logger = Logger.getLogger(TagService.class);

	/**
	 * Mocks initiation
	 */
	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Add method test
	 */
	@Test
	public void testAdd() {
		try {
			Mockito.doReturn(1L).when(dao).create(tag);
			long actual = serv.create(tag);
			long expected = 1L;
			Mockito.verify(dao).create(tag);
			Assert.assertEquals(actual, expected);
		} catch (DAOException e) {
			logger.error(e.getMessage());
		} catch (ServiceException e) {
			logger.error(e.getMessage());
		}
	}

	

}
