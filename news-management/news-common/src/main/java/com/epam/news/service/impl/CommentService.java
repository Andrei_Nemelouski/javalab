package com.epam.news.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.epam.news.db.dao.CommentDAO;
import com.epam.news.entity.Comment;
import com.epam.news.exceptions.DAOException;
import com.epam.news.exceptions.ServiceException;
import com.epam.news.service.ICommentService;

/**
 * Service class for news comments
 * 
 * @author Andrei_Nemelouski
 *
 */
public class CommentService implements ICommentService {

	@Autowired
	@Qualifier("commentDao")
	private CommentDAO commentDao;

	/**
	 * Add new comment news
	 * 
	 * @param cto
	 *            Comment transfer object
	 * @return added comment id
	 * @throws ServiceException
	 *             wrapper exception for exceptions from DAO layer
	 */
	@Override
	public long create(Comment cto) throws ServiceException {
		try {
			return commentDao.create(cto);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	@Override
	public Comment readById(long id) throws ServiceException {
		try {
			return commentDao.readById(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * Delete comments by id according to {@code delete} array
	 * 
	 * @param delete
	 *            array of rows to delete
	 * @throws ServiceException
	 *             wrapper exception for exceptions from DAO layer
	 */
	@Override
	public void delete(long delete) throws ServiceException {
		try {
			commentDao.delete(delete);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void update(Comment obj) throws ServiceException {
		throw new UnsupportedOperationException();
	}


	@Override
	public List<Comment> read() throws ServiceException {
		throw new UnsupportedOperationException();
	}


	@Override
	public List<Comment> readByNewsId(long id) throws ServiceException {
		try {
			return commentDao.readByNewsId(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

}
