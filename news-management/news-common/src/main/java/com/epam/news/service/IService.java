package com.epam.news.service;

import java.util.List;

import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.epam.news.exceptions.ServiceException;

@Transactional(readOnly = true, propagation = Propagation.SUPPORTS, isolation = Isolation.READ_COMMITTED, rollbackFor = {
		ServiceException.class, RuntimeException.class })
public interface IService<T> {

	/**
	 * Create new record in database table corresponding to the {@code T}
	 * 
	 * @param obj
	 *            Entity transfer object
	 * @return created record Id
	 * @throws ServiceException
	 *             wrapper exception for exceptions from DAO layer
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	long create(T obj) throws ServiceException;

	/**
	 * Returns all records from table corresponding to the {@code T}
	 * 
	 * @return {@code List} of Entity transfer objects
	 * @throws ServiceException
	 *             wrapper exception for exceptions from DAO layer
	 */
	List<T> read() throws ServiceException;

	/**
	 * Return record with primary key {@code id} from table related to the
	 * {@code T}
	 * 
	 * @param id
	 *            needed record id
	 * @return Entity transfer object
	 * @throws ServiceException
	 *             wrapper exception for exceptions from DAO layer
	 */
	T readById(long id) throws ServiceException;

	/**
	 * Updates existing record in the table related with {@code T} with content
	 * of {@code obj}
	 * 
	 * @param obj
	 *            Entity transfer object
	 * @throws ServiceException
	 *             wrapper exception for exceptions from DAO layer
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	void update(T obj) throws ServiceException;

	/**
	 * Delete record with primary key {@code delete} from table related with
	 * {@code T}
	 * 
	 * @param delete
	 *            record id
	 * @throws ServiceException
	 *             wrapper exception for exceptions from DAO layer
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	void delete(long delete) throws ServiceException;

}
