package com.epam.news.exceptions;

/**
 * DAO layer exception
 * @author Andrei_Nemelouski
 *
 */
public class DAOException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7044052559536903866L;
	/**
	 * Exception cause
	 */
	private Throwable cause;

	public DAOException(Throwable cause) {
		this.cause = cause;
	}

	public Throwable getCause() {
		return cause;
	}

	public String getMessage() {
		return cause.getMessage();
	}

}
