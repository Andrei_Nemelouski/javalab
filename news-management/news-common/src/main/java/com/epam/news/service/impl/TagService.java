package com.epam.news.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.epam.news.db.dao.TagDAO;
import com.epam.news.entity.Tag;
import com.epam.news.exceptions.DAOException;
import com.epam.news.exceptions.ServiceException;
import com.epam.news.service.ITagService;

/**
 * Service class for operations using {@code ITagDAO} implementations 
 * @author Andrei_Nemelouski
 *
 */
public class TagService implements ITagService{
	
	@Autowired
	@Qualifier("tagDao")
	private TagDAO tagDao;
	

	/**
	 * Creating new {@tag}
	 * 
	 * @param aut
	 *            Tag transfer object
	 * @return added Tag id
	 * @throws ServiceException
	 *             wrapper exception for exceptions from DAO layer
	 */
	@Override
	public long create(Tag tto) throws ServiceException {
		try {
			return tagDao.create(tto);
		} catch (DAOException e) {
			throw new ServiceException(e);
		} 
	}
	
	@Override
	public void update(Tag obj) throws ServiceException {
		try {
			tagDao.update(obj);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	@Override
	public long createOrUpdate(Tag obj) throws ServiceException {
		long result = obj.getId();
		try {
			if (result == 0) {
				result = tagDao.create(obj);
			} else {
				tagDao.update(obj);
			}
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return result;
	}


	@Override
	public void delete(long delete) throws ServiceException {
		try {
			tagDao.delete(delete);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}		
	}

	@Override
	public List<Tag> read() throws ServiceException {
		try {
			return tagDao.read();
		} catch (DAOException e) {
			throw new ServiceException(e);
		} 	
	}

	@Override
	public List<Tag> readByNewsId(long id) throws ServiceException {
		try {
			return tagDao.readByNewsId(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		} 
	}

	@Override
	public void bindToNews(long newsId, long[] tagId) throws ServiceException {
		try {
			tagDao.bindToNews(newsId, tagId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		
	}

	@Override
	public Tag readById(long id) throws ServiceException {
		throw new UnsupportedOperationException();	
	}
}
