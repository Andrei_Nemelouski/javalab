package com.epam.news.service;

import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.epam.news.entity.dto.NewsTO;
import com.epam.news.entity.representation.Filter;
import com.epam.news.entity.representation.Page;
import com.epam.news.exceptions.ServiceException;

@Transactional(readOnly = true, propagation = Propagation.REQUIRES_NEW, isolation = Isolation.READ_COMMITTED, rollbackFor = {
		ServiceException.class, RuntimeException.class })
public interface IPageService {

	/**
	 * Read content for News List page
	 * 
	 * @param newsPerPage
	 *            number of news presented on single page
	 * @param currentPage
	 *            current page number
	 * @param filter
	 *            filtration info handler
	 * @return page content transfer object
	 * @throws ServiceException
	 *             exception that handles exceptions from DAO layer.
	 */
	Page readNewsList(int newsPerPage, int currentPage, Filter filter)
			throws ServiceException;

	/**
	 * Read content for News View page
	 * 
	 * @param id
	 *            article id
	 * @param filter
	 *            filtration info handler
	 * @return page content transfer object
	 * @throws ServiceException
	 *             exception that handles exceptions from DAO layer.
	 */
	Page readNewsPage(long id, Filter filter) throws ServiceException;

	/**
	 * Read news record for editing.
	 * 
	 * @param id
	 *            needed news id
	 * @return news transfer object.
	 * @throws ServiceException
	 *             exception that handles exceptions from DAO layer.
	 */
	NewsTO readNewsEditor(long id) throws ServiceException;

}
