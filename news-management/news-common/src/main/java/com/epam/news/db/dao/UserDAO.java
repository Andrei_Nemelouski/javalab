package com.epam.news.db.dao;

import com.epam.news.entity.dto.UserTO;
import com.epam.news.exceptions.DAOException;

public interface UserDAO extends Dao<UserTO> {

	/**
	 * Read single {@code User} by login.
	 * 
	 * @param login
	 *            User login
	 * @return {@code User} entity
	 * @throws DAOException
	 *             exception that handles SQLException from database and
	 *             PoolException from pool.
	 */
	UserTO readByLogin(String login) throws DAOException;

}
