package com.epam.news.db.dao;

import com.epam.news.entity.Author;
import com.epam.news.exceptions.DAOException;

/**
 * DAO interface for operations with Authors table
 * 
 * @author Andrei_Nemelouski
 *
 */
public interface AuthorDAO extends Dao<Author> {

	/**
	 * Read single {@code Author} by News id.
	 * 
	 * @param id
	 *            News id
	 * @return {@code Author} entity
	 * @throws DAOException
	 *             exception that handles SQLException from database and
	 *             PoolException from pool.
	 */
	Author readByNewsId(long id) throws DAOException;

	/**
	 * Bind author with {@code id} to news record with {@code id}
	 * @param newsId news id
	 * @param authorId author to bind id
	 * @throws DAOException
	 *             exception that handles SQLException from database and
	 *             PoolException from pool.
	 */
	void bindToNews(long newsId, long authorId) throws DAOException;

	/**
	 * Set author with {@code id} as expired. After this operation author will
	 * be no longer available to create news.
	 * 
	 * @param id
	 *            author Id
	 * @throws DAOException
	 *             exception that handles SQLException from database and
	 *             PoolException from pool.
	 */
	void setExpired(long id) throws DAOException;

}
