package com.epam.news.db.dao;

import java.util.List;

import com.epam.news.entity.Tag;
import com.epam.news.exceptions.DAOException;

/**
 * DAO interface for operations with Tag table
 * 
 * @author Andrei_Nemelouski
 *
 */
public interface TagDAO extends Dao<Tag> {

	
	/**
	 * Bind tags with id's form {@code tagId} array to news record with
	 * {@code id}
	 * 
	 * @param newsId
	 *            news id
	 * @param tagId
	 *            tags to bind ids
	 * @throws DAOException
	 *             exception that handles SQLException from database and
	 *             PoolException from pool.
	 */
		void bindToNews(long newsId, long[] tagId) throws DAOException;

	/**
	 * Read single {@code Tag} by News id.
	 * 
	 * @param id
	 *            News id
	 * @return {@code Tag} entity
	 * @throws DAOException
	 *             exception that handles SQLException from database and
	 *             PoolException from pool.
	 */
	
	List<Tag> readByNewsId(long id) throws DAOException;

}
