package com.epam.news.service;

import java.util.List;

import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.epam.news.entity.Comment;
import com.epam.news.exceptions.ServiceException;

@Transactional(readOnly = true, propagation = Propagation.SUPPORTS, isolation = Isolation.READ_COMMITTED, rollbackFor = {
		ServiceException.class, RuntimeException.class })
public interface ICommentService extends IService<Comment> {

	/**
	 * Read {@code Comments} by News id.
	 * 
	 * @param id
	 *            News id
	 * @return {@code List<Comment>}
	 * @throws ServiceException
	 *             wrapper exception for exceptions from DAO layer
	 */
	List<Comment> readByNewsId(long id) throws ServiceException;

}
