package com.epam.news.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.epam.news.entity.dto.UserTO;

public class UserTOMapper implements IMapper<UserTO> {

	@Override
	public <F extends ResultSet> UserTO construct(F from) throws SQLException {
		UserTO result = new UserTO();
		result.setName(from.getString("user_name"));
		result.setPassword(from.getString("password"));
		result.setRole(from.getString("role_name"));
		return result;
	}

}
