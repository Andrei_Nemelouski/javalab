package com.epam.news.service;

import java.util.List;

import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.epam.news.entity.Tag;
import com.epam.news.exceptions.ServiceException;

@Transactional(readOnly = true, propagation = Propagation.SUPPORTS, isolation = Isolation.READ_COMMITTED, rollbackFor = {
		ServiceException.class, RuntimeException.class })
public interface ITagService extends IService<Tag> {

	/**
	 * Read single {@code Tag} by News id.
	 * 
	 * @param id
	 *            News id
	 * @return {@code Tag} entity
	 * @throws ServiceException
	 *             wrapper exception for exceptions from DAO layer
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	List<Tag> readByNewsId(long id) throws ServiceException;

	/**
	 * Bind tags with id's form {@code tagId} array to news record with
	 * {@code id}
	 * 
	 * @param newsId
	 *            news id
	 * @param tagId
	 *            tags to bind ids
	 * @throws ServiceException
	 *             wrapper exception for exceptions from DAO layer
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	void bindToNews(long newsId, long[] tagId) throws ServiceException;

	/**
	 * Creates new record or updates existing using {@code obj} content
	 * 
	 * @param obj
	 *            Tag entity
	 * @return tag id if record was created and 0 if it was updated
	 * @throws ServiceException
	 *             wrapper exception for exceptions from DAO layer
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	long createOrUpdate(Tag obj) throws ServiceException;
}
