package com.epam.news.entity.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Entity;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import com.epam.news.entity.Author;
import com.epam.news.entity.Comment;
import com.epam.news.entity.Tag;

@Entity
@Table(name = ("News"))
@NamedQuery(name = "findAll", query = "FROM News n ORDER BY n.date DESC")
public class NewsTO implements Serializable {

	/**
	 * 
	 */
	public static final long serialVersionUID = -6533715073155606796L;
	/**
	 * News table primary key
	 */
	@Id
	@GeneratedValue
	@Column(name = "NEWS_ID")
	private long id;
	/**
	 * short_text column
	 */
	@NotNull
	@NotEmpty
	@NotBlank
	@Column(name = "SHORT_TEXT")
	private String shortText;
	/**
	 * full_text column
	 */
	@NotNull
	@NotEmpty
	@NotBlank
	@Column(name = "FULL_TEXT")
	private String fullText;
	/**
	 * title column
	 */
	@NotNull
	@NotEmpty
	@NotBlank
	@Column(name = "TITLE")
	private String title;
	/**
	 * creation_date column
	 */
	@Column(name = "CREATION_DATE")
	private Date creationDate;
	/**
	 * modification_date column
	 */
	@NotNull
	@Column(name = "MODIFICATION_DATE")
	private Date modificationDate;
	@OneToOne(cascade = CascadeType.ALL)
	@JoinTable(name = "NEWS_AUTHOR", 
	joinColumns = { @JoinColumn(name = "NEWS_ID") }, 
	inverseJoinColumns = { @JoinColumn(name = "AUTHOR_ID") })
	private Author author;
	
	private List<Tag> tag;
	private List<Comment> comments;
	private int commentsNumber;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getShortText() {
		return shortText;
	}

	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

	public String getFullText() {
		return fullText;
	}

	public void setFullText(String fullText) {
		this.fullText = fullText;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public List<Tag> getTag() {
		return tag;
	}

	public void setTag(List<Tag> tag) {
		this.tag = tag;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public int getCommentsNumber() {
		return commentsNumber;
	}

	public void setCommentsNumber(int commentsNumber) {
		this.commentsNumber = commentsNumber;
	}

	@Override
	public String toString() {
		return "NewsTO [id=" + id + ", shortText=" + shortText + ", fullText="
				+ fullText + ", title=" + title + ", creationDate="
				+ creationDate + ", modificationDate=" + modificationDate
				+ ", author=" + author + ", tag=" + tag + ", comments="
				+ comments + ", commentsNumber=" + commentsNumber + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((author == null) ? 0 : author.hashCode());
		result = prime * result
				+ ((comments == null) ? 0 : comments.hashCode());
		result = prime * result + commentsNumber;
		result = prime * result
				+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result
				+ ((fullText == null) ? 0 : fullText.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime
				* result
				+ ((modificationDate == null) ? 0 : modificationDate.hashCode());
		result = prime * result
				+ ((shortText == null) ? 0 : shortText.hashCode());
		result = prime * result + ((tag == null) ? 0 : tag.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NewsTO other = (NewsTO) obj;
		if (author == null) {
			if (other.author != null)
				return false;
		} else if (!author.equals(other.author))
			return false;
		if (comments == null) {
			if (other.comments != null)
				return false;
		} else if (!comments.equals(other.comments))
			return false;
		if (commentsNumber != other.commentsNumber)
			return false;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (fullText == null) {
			if (other.fullText != null)
				return false;
		} else if (!fullText.equals(other.fullText))
			return false;
		if (id != other.id)
			return false;
		if (modificationDate == null) {
			if (other.modificationDate != null)
				return false;
		} else if (!modificationDate.equals(other.modificationDate))
			return false;
		if (shortText == null) {
			if (other.shortText != null)
				return false;
		} else if (!shortText.equals(other.shortText))
			return false;
		if (tag == null) {
			if (other.tag != null)
				return false;
		} else if (!tag.equals(other.tag))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

}
