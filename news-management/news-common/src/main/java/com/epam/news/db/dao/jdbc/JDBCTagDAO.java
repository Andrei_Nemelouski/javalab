package com.epam.news.db.dao.jdbc;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.news.db.dao.TagDAO;
import com.epam.news.db.dao.util.QueryBuilder;
import com.epam.news.db.dao.util.ResourceManager;
import com.epam.news.entity.Tag;
import com.epam.news.exceptions.DAOException;
import com.epam.news.mapper.IMapper;

public class JDBCTagDAO implements TagDAO {

	private final String READ = "SELECT tag.tag_id, tag.tag_name FROM tag";
	private final String READ_BY_NEWS_ID = "SELECT t.tag_id, t.tag_name FROM tag t INNER JOIN news_tag nt ON t.tag_id=nt.tag_id WHERE nt.news_id=?";
	private final String UPDATE = "UPDATE tag SET tag_name=? WHERE tag_id=?";
	private final String DELETE_TAG = "DELETE FROM tag WHERE tag_id=";
	private final String DELETE_NEWS_TAG = "DELETE FROM news_tag WHERE tag_id=";
	private final String CREATE = "declare "
			+ "tagName NVARCHAR2(20) := ?; "
			+ "cur_state NVARCHAR2(20); "
			+ "resultId NUMBER(10); "
			+ "begin "
			+ "select tag.tag_name INTO cur_state FROM tag WHERE tag_name=tagName; "
			+ "exception "
			+ "WHEN NO_DATA_FOUND THEN "
			+ "cur_state := null; "
			+ "if (cur_state is NULL) then "
			+ "begin "
			+ "insert into tag(tag_id, tag_name) values (tag_id_seq.nextval, tagName); "
			+ "end; "
			+ "end if; "
			+ "select tag.tag_id INTO resultId FROM tag WHERE tag_name=tagName; "
			+ "? := resultId; " + "end;";
	@Autowired
	@Qualifier("dataSource")
	private DataSource dataSource;
	@Autowired
	private IMapper<Tag> tagMapper;

	/**
	 * Creates new record in the {@code Tag} table
	 * 
	 * @param obj
	 *            Entity transfer object
	 * @return id of created {@code Tag}
	 * @throws DAOException
	 *             exception that handles SQLException from database.
	 */
	@Override
	public long create(Tag obj) throws DAOException {
		Connection conn = DataSourceUtils.getConnection(dataSource);
		CallableStatement cs = null;
		long result = 0;
		try {
			cs = conn.prepareCall(CREATE);
			cs.setString(1, obj.getTagName());
			cs.registerOutParameter(2, Types.INTEGER);
			cs.execute();
			result = cs.getInt(2);
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			ResourceManager.closeResources(dataSource, conn, cs);
		}
		return result;
	}

	@Override
	public void bindToNews(long id, long[] tagId) throws DAOException {
		Connection conn = DataSourceUtils.getConnection(dataSource);
		CallableStatement cs = null;
		try {
			int paramIdx = 1;
			cs = conn.prepareCall(QueryBuilder.constructTagBindCallable(tagId));
			cs.setLong(paramIdx++, id);
			for(int i=0;i<tagId.length;i++){
				cs.setLong(paramIdx++, tagId[i]);
			}
			cs.execute();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			ResourceManager.closeResources(dataSource, conn, cs);
		}

	}	

	/**
	 * Returns all records from {@code Tag} table
	 * 
	 * @return {@code List} of entities
	 * @throws DAOException
	 *             exception that handles SQLException from database.
	 */
	@Override
	public List<Tag> read() throws DAOException {
		Connection conn = DataSourceUtils.getConnection(dataSource);
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Tag> list = new ArrayList<>();
		try {
			ps = conn.prepareStatement(READ);
			rs = ps.executeQuery();
			while (rs.next()) {
				list.add(tagMapper.construct(rs));
			}
			return list;
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			ResourceManager.closeResources(dataSource, conn, ps, rs);
		}

	}

	@Override
	public Tag readById(long id) throws DAOException {
		// TODO implement method
		throw new UnsupportedOperationException();

	}

	@Override
	public List<Tag> readByNewsId(long id) throws DAOException {
		Connection conn = DataSourceUtils.getConnection(dataSource);
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Tag> tag = new ArrayList<>();
		try {
			ps = conn.prepareStatement(READ_BY_NEWS_ID);
			ps.setLong(1, id);
			rs = ps.executeQuery();
			while (rs.next()) {
				tag.add(tagMapper.construct(rs));
			}
			return tag;
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			ResourceManager.closeResources(dataSource, conn, ps, rs);
		}
	}

	/**
	 * Updates table {@code Tag} according to {@code obj} contents
	 * 
	 * @param obj
	 *            Entity transfer object
	 * @return number of updated rows
	 * @throws DAOException
	 *             exception that handles SQLException from database.
	 */
	@Override
	public long update(Tag obj) throws DAOException {
		Connection conn = DataSourceUtils.getConnection(dataSource);
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(UPDATE);
			ps.setString(1, obj.getTagName());
			ps.setLong(2, obj.getId());
			return ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			ResourceManager.closeResources(dataSource, conn, ps);
		}

	}

	/**
	 * 
	 * Deletes all rows with id from {@code id} array
	 * 
	 * @param id
	 *            array of rows id's
	 * @throws DAOException
	 *             exception that handles SQLException from database.
	 */
	@Override
	public void delete(long id) throws DAOException {
		Connection conn = DataSourceUtils.getConnection(dataSource);
		Statement st = null;
		String tag = DELETE_TAG + "'" + id + "'";
		String news_tag = DELETE_NEWS_TAG + "'" + id + "'";
		try {
			st = conn.createStatement();
			st.addBatch(news_tag);
			st.addBatch(tag);
			st.executeBatch();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			ResourceManager.closeResources(dataSource, conn, st);
		}

	}

}
