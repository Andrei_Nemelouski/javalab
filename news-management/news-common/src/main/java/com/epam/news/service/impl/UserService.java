package com.epam.news.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.epam.news.db.dao.UserDAO;
import com.epam.news.entity.dto.UserTO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.epam.news.exceptions.DAOException;
import com.epam.news.exceptions.ServiceException;
import com.epam.news.service.IUserService;

public class UserService implements IUserService, UserDetailsService {

	@Autowired
	@Qualifier("userDao")
	private UserDAO userDao;

	@Override
	public UserDetails loadUserByUsername(String login)
			throws UsernameNotFoundException {
		UserTO user = null;
		try {
			user = userDao.readByLogin(login);
			if (user == null)
				throw new UsernameNotFoundException("user name not found");
		} catch (DAOException e) {
			throw new UsernameNotFoundException("db error");
		}
		return buildUser(user);
	}

	private User buildUser(UserTO user) {
		String username = user.getName();
		String password = user.getPassword();
		boolean enabled = true;
		boolean accountNonExpired = true;
		boolean credentialsNonExpired = true;
		boolean accountNonLocked = true;
		List<GrantedAuthority> authorities = new ArrayList<>();
		SimpleGrantedAuthority g = new SimpleGrantedAuthority(user.getRole());
		authorities.add(g);
		User springUser = new User(username, password, enabled,
				accountNonExpired, credentialsNonExpired, accountNonLocked,
				authorities);
		return springUser;

	}

	@Override
	public long create(UserTO obj) throws ServiceException {
		throw new UnsupportedOperationException();	
	}

	@Override
	public List<UserTO> read() throws ServiceException {
		throw new UnsupportedOperationException();	
	}

	@Override
	public void update(UserTO obj) throws ServiceException {
		throw new UnsupportedOperationException();	
		
	}

	@Override
	public void delete(long delete) throws ServiceException {
		throw new UnsupportedOperationException();	
		
	}

	@Override
	public UserTO readById(long id) throws ServiceException {
		throw new UnsupportedOperationException();	
	}

}
