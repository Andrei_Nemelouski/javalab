package com.epam.news.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.epam.news.entity.Comment;

public class CommentMapper implements IMapper<Comment> {

	@Override
	public <F extends ResultSet> Comment construct(F from)
			throws SQLException {
		Comment com = new Comment();
		com.setId(from.getLong("comment_id"));
		com.setText(from.getString("comment_text"));
		com.setCreationDate(from.getTimestamp("creation_date"));
		com.setNewsId(from.getInt("news_id"));
		return com;
		
	}

}
