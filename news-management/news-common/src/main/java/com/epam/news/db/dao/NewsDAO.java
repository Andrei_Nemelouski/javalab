package com.epam.news.db.dao;

import java.util.List;

import com.epam.news.entity.dto.NewsTO;
import com.epam.news.entity.representation.Filter;
import com.epam.news.exceptions.DAOException;

/**
 * DAO interface for operations with News table
 * 
 * @author Andrei_Nemelouski
 *
 */
public interface NewsDAO extends Dao<NewsTO> {
	
	/**
	 * Read news according to filtration, current page and news number per page.
	 * @param newsPerPage number of news presented on single page
	 * @param currentPage current page number
	 * @param filter filtration info handler 
	 * @return List of news transfer objects
	 * @throws DAOException
	 *             exception that handles SQLException from database and
	 *             PoolException from pool.
	 */
	List<NewsTO> read(int newsPerPage, int currentPage, Filter filter) throws DAOException;
		
	/**
	 * Fetch next and previous news id for article with {@code id}
	 * 
	 * @param id
	 *            news id
	 * @param filter
	 *            filtration info handler class
	 * @return long[] where [0] element is previous news id and [1] element is
	 *         next news id
	 * @throws DAOException
	 *             exception that handles SQLException from database and
	 *             PoolException from pool.
	 */
	long[] readNextPrev(long id, Filter filter) throws DAOException;
	
	/**
	 * Count all news according to filtration info in {@code Filter}
	 * @param filter filtration info handler
	 * @return number of news
	 * @throws DAOException
	 *             exception that handles SQLException from database and
	 *             PoolException from pool.
	 */
	int countNews(Filter filter) throws DAOException;
	
	/**
	 * Delete news according to id array {@code delete}
	 * @param delete array of news id
	 * @throws DAOException
	 *             exception that handles SQLException from database and
	 *             PoolException from pool.
	 */	
	void delete(long[] delete) throws DAOException;
}
