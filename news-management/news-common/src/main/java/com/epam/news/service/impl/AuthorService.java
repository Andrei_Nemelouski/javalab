package com.epam.news.service.impl;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.epam.news.db.dao.AuthorDAO;
import com.epam.news.entity.Author;
import com.epam.news.exceptions.DAOException;
import com.epam.news.exceptions.ServiceException;
import com.epam.news.service.IAuthorService;

/**
 * Service class for operations using {@code author} information
 * 
 * @author Andrei_Nemelouski
 *
 */
@WebService(serviceName = "authorService")
public class AuthorService implements IAuthorService{

	@Autowired
	@Qualifier("authorDao")
	private AuthorDAO authorDao;
	
	/**
	 * Creating new {@author}
	 * 
	 * @param aut
	 *            Author transfer object
	 * @return added Author id
	 * @throws ServiceException
	 *             wrapper exception for exceptions from DAO layer
	 */
	@Override
	public long create(Author aut) throws ServiceException {
		try {
			return authorDao.create(aut);
		} catch (DAOException e) {
			throw new ServiceException(e);
		} 
	}

	@Override
	public void update(Author obj) throws ServiceException {
		try {
			authorDao.update(obj);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}	
	}
	
	@Override
	public long createOrUpdate(Author obj) throws ServiceException {
		long result = obj.getId();
		try {
			if (result == 0) {
				result = authorDao.create(obj);
			} else {
				authorDao.update(obj);
			}
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return result;
	}
	
	@Override
	public void setExpired(long id) throws ServiceException {
		try {
			authorDao.setExpired(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		
	}

	@Override
	public void delete(long delete) throws ServiceException {
		throw new UnsupportedOperationException();			
	}
	
	

	@Override
	public List<Author> read() throws ServiceException {
		try {
			return authorDao.read();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}	
	}

	@Override
	@WebMethod(operationName = "readByNewsId")
	public Author readByNewsId(long id) throws ServiceException {
		try {
			return authorDao.readByNewsId(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void bindToNews(long newsId, long authorId) throws ServiceException{
		try {
			authorDao.bindToNews(newsId, authorId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		
	}

	@Override
	public Author readById(long id) throws ServiceException {
		throw new UnsupportedOperationException();	
	}	
}
