package com.epam.news.exceptions;

/**
 * Service layer exception
 * 
 * @author Andrei_Nemelouski
 *
 */
public class ServiceException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3111251350583224076L;
	/**
	 * Exception layer
	 */
	private Throwable cause;

	public ServiceException(Throwable cause) {
		this.cause = cause;
	}

	public Throwable getCause() {
		return cause;
	}

	public String getMessage() {
		return cause.getMessage();
	}

}
