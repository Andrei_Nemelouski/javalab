package com.epam.news.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.epam.news.entity.dto.NewsTO;

/**
 * {@code NewsTO} builders director class
 * 
 * @author Andrei_Nemelouski
 * @param <F>
 * @param <T>
 *
 */
public class NewsTOMapper implements IMapper<NewsTO>{
	
	/**
	 * Builds {@code NewsTO} from {@code ResultSet}
	 * 
	 * @param rs
	 *            incoming ResultSet
	 * @return {@code NewsTO}
	 * @throws SQLException
	 *             An exception that provides information on a database access
	 *             error or other errors.
	 */
	public NewsTO construct(ResultSet rs) throws SQLException {
		NewsTO result = new NewsTO();
		result.setCommentsNumber(rs.getInt("cntd"));
		result.setId(rs.getLong("news_id"));
		result.setShortText(rs.getString("short_text"));
		result.setFullText(rs.getString("full_text"));
		result.setTitle(rs.getString("title"));
		result.setCreationDate(rs.getTimestamp("creation_date"));
		result.setModificationDate(rs.getDate("modification_date"));
		return result;
	}

	

	

	

}
