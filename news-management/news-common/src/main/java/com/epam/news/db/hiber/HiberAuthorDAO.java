package com.epam.news.db.hiber;

import java.util.List;

import org.hibernate.SessionFactory;

import com.epam.news.db.dao.AuthorDAO;
import com.epam.news.db.dao.util.SessionFactoryManager;
import com.epam.news.entity.Author;
import com.epam.news.exceptions.DAOException;

public class HiberAuthorDAO implements AuthorDAO {

	private SessionFactory factory = SessionFactoryManager.getSessionFactory(); 
	
	@Override
	public long create(Author obj) throws DAOException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<Author> read() throws DAOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Author readById(long id) throws DAOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long update(Author obj) throws DAOException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void delete(long id) throws DAOException {
		// TODO Auto-generated method stub

	}

	@Override
	public Author readByNewsId(long id) throws DAOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void bindToNews(long newsId, long authorId) throws DAOException {
		// TODO Auto-generated method stub

	}

	@Override
	public void setExpired(long id) throws DAOException {
		// TODO Auto-generated method stub

	}

}
