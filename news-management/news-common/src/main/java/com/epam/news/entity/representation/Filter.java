package com.epam.news.entity.representation;

import java.io.Serializable;
import java.util.List;

import com.epam.news.entity.Author;
import com.epam.news.entity.Tag;

public class Filter implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4913244950510494952L;
	private Author author;
	private List<Tag> tag;
	
	public Author getAuthor() {
		return author;
	}
	public void setAuthor(Author author) {
		this.author = author;
	}
	public List<Tag> getTag() {
		return tag;
	}
	public void setTag(List<Tag> tag) {
		this.tag = tag;
	}
	@Override
	public String toString() {
		return "Filter [author=" + author + ", tag=" + tag + "]";
	}


	
}
