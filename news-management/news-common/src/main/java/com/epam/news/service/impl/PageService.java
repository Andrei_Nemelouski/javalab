package com.epam.news.service.impl;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.epam.news.entity.dto.NewsTO;
import com.epam.news.entity.representation.Filter;
import com.epam.news.entity.representation.Page;
import com.epam.news.exceptions.ServiceException;
import com.epam.news.service.IAuthorService;
import com.epam.news.service.ICommentService;
import com.epam.news.service.INewsService;
import com.epam.news.service.IPageService;
import com.epam.news.service.ITagService;

/**
 * Service layer director class
 * 
 * @author Andrei_Nemelouski
 *
 */
public class PageService implements IPageService {
	@Autowired
	@Qualifier("authorService")
	private IAuthorService author;
	@Autowired
	@Qualifier("commentService")
	private ICommentService comment;
	@Autowired
	@Qualifier("newsService")
	private INewsService news;
	@Autowired
	@Qualifier("tagService")
	private ITagService tag;
	
	@Override
	public Page readNewsList(int newsPerPage, int currentPage, Filter filter) throws ServiceException {
		Page page = new Page();
		//TODO possible performance leak
		List<NewsTO> newsList = news.read(newsPerPage, currentPage, filter);
		for (NewsTO item : newsList) {
			long id = item.getId();
			item.setAuthor(author.readByNewsId(id));
			item.setTag(tag.readByNewsId(id));
		}
		page.setNews(newsList);
		int newsNumber = news.countNews(filter);
		int pages = newsNumber/newsPerPage;
		if(newsNumber%newsPerPage != 0){
			pages++;
		}
		page.setCurrentPage(currentPage);
		page.setNewsPerPage(newsPerPage);
		page.setNumberOfPages(pages);
		///
		return page;
		
	}

	@Override
	public Page readNewsPage(long id, Filter filter) throws ServiceException {
		Page page = new Page();
		//TODO possible performance leak
		NewsTO article = news.readById(id);
		article.setAuthor(author.readByNewsId(id));
		//page.setArticle(article);
		article.setComments(comment.readByNewsId(id));
		List<NewsTO> result = new ArrayList<>();
		result.add(article);
		page.setNews(result);
		return page;
	}

	@Override
	public NewsTO readNewsEditor(long id) throws ServiceException {
		NewsTO article;
		if(id != 0){
		article = news.readById(id);
		article.setAuthor(author.readByNewsId(id));
		article.setTag(tag.readByNewsId(id));
		} else {
			article = new NewsTO();
			article.setCreationDate(new Timestamp(System.currentTimeMillis()));
			article.setModificationDate(new Date(System.currentTimeMillis()));
		}
		return article;
	}
	
}
