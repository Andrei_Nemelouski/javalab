package com.epam.news.db.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.news.db.dao.CommentDAO;
import com.epam.news.db.dao.util.ResourceManager;
import com.epam.news.entity.Comment;
import com.epam.news.exceptions.DAOException;
import com.epam.news.mapper.IMapper;

public class JDBCCommentDAO implements CommentDAO {

	private final String ADD = "INSERT INTO Comments(comment_id,comment_text,creation_date,news_id) VALUES (comment_id_seq.nextval, ?,?,?)";
	private final String DELETE = "DELETE FROM Comments WHERE comment_id=?";
	private final String READ = "SELECT comments.comment_id, comments.comment_text, comments.creation_date, comments.news_id FROM comments ORDER BY comments.creation_date ASC";
	private final String READ_BY_ID = "SELECT comments.comment_id, comments.comment_text, comments.creation_date, comments.news_id FROM comments WHERE comment_id=? ";
	private final String READ_BY_NEWS_ID = "SELECT comments.comment_id, comments.comment_text, comments.creation_date, comments.news_id FROM comments WHERE news_id=? ORDER BY comments.creation_date ASC";
	private final String UPDATE = "UPDATE comments SET comment_text=?, creation_date=? WHERE comment_id=?";
	private final String[] GENERATED_ID = {"COMMENT_ID"};	
	@Autowired
	@Qualifier("dataSource")
	private DataSource dataSource;	
	@Autowired
	private IMapper<Comment> commentMapper;

	/**
	 * Creates new record in the {@code Comments} table
	 * 
	 * @param obj
	 *            Entity transfer object
	 * @return id of created {@code Comment}
	 * @throws DAOException
	 *             exception that handles SQLException from database
	 */
	@Override
	public long create(Comment obj) throws DAOException {
		Connection conn = DataSourceUtils.getConnection(dataSource);
		PreparedStatement ps = null;
		ResultSet rs = null;
		long result = 0;
		try {
			ps = conn.prepareStatement(ADD, GENERATED_ID);
			ps.setString(1, obj.getText());
			ps.setTimestamp(2, new Timestamp(obj.getCreationDate().getTime()));
			ps.setLong(3, obj.getNewsId());
			ps.executeUpdate();
			rs = ps.getGeneratedKeys();
			while(rs.next()){
				result = rs.getInt(1);
			}			
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			ResourceManager.closeResources(dataSource, conn, ps, rs);
		}
		return result;
	}

	/**
	 * Returns all records from {@code Comments} table
	 * 
	 * @return {@code List} of entities
	 * @throws DAOException
	 *             exception that handles SQLException from database
	 */
	@Override
	public List<Comment> read() throws DAOException {
		Connection conn = DataSourceUtils.getConnection(dataSource);
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Comment> list = new ArrayList<>();
		try {
			ps = conn.prepareStatement(READ);
			rs = ps.executeQuery();
			while (rs.next()) {
				list.add(commentMapper.construct(rs));
			}
			return list;
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			ResourceManager.closeResources(dataSource, conn, ps, rs);
		}
	}
	
	@Override
	public Comment readById(long id) throws DAOException {
		Connection conn = DataSourceUtils.getConnection(dataSource);
		PreparedStatement ps = null;
		ResultSet rs = null;
		Comment result = null;
		try {
			ps = conn.prepareStatement(READ_BY_ID);
			ps.setLong(1, id);
			rs = ps.executeQuery();
			if (rs.next()) {
				result = commentMapper.construct(rs);
			}
			return result;
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			ResourceManager.closeResources(dataSource, conn, ps, rs);
		}
	}
	
	@Override
	public List<Comment> readByNewsId(long id) throws DAOException {
		Connection conn = DataSourceUtils.getConnection(dataSource);
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Comment> list = new ArrayList<>();
		try {
			ps = conn.prepareStatement(READ_BY_NEWS_ID);
			ps.setLong(1, id);
			rs = ps.executeQuery();
			while (rs.next()) {
				list.add(commentMapper.construct(rs));
			}
			return list;
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			ResourceManager.closeResources(dataSource, conn, ps, rs);
		}
	}

	/**
	 * Updates table {@code Comments} according to {@code obj} contents
	 * 
	 * @param obj
	 *            Entity transfer object
	 * @return number of updated rows
	 * @throws DAOException
	 *             exception that handles SQLException from database
	 */
	@Override
	public long update(Comment obj) throws DAOException {
		Connection conn = DataSourceUtils.getConnection(dataSource);
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(UPDATE);
			ps.setString(1, obj.getText());
			ps.setTimestamp(2, new Timestamp(obj.getCreationDate().getTime()));
			ps.setLong(3, obj.getId());
			return ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			ResourceManager.closeResources(dataSource, conn, ps);
		}

	}

	/**
	 * 
	 * Deletes all rows with id from {@code id} array
	 * 
	 * @param id
	 *            array of rows id's
	 * @throws DAOException
	 *             exception that handles SQLException from database
	 */
	@Override
	public void delete(long id) throws DAOException {
		Connection conn = DataSourceUtils.getConnection(dataSource);
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(DELETE);
			ps.setLong(1, id);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			ResourceManager.closeResources(dataSource, conn, ps);
		}

	}	
}
