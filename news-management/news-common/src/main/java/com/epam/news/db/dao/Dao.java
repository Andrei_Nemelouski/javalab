package com.epam.news.db.dao;

import java.util.List;

import com.epam.news.exceptions.DAOException;

/**
 * Generic DAO interface for all tables
 * 
 * @author Andrei_Nemelouski
 *
 * @param <T>
 *            entity that represents database table
 */
public interface Dao<T> {
	/**
	 * Creates new record in the {@code <T>} table
	 * 
	 * @param obj
	 *            Entity transfer object
	 * @return id of created {@code <T>}
	 * @throws DAOException
	 *             exception that handles SQLException from database and
	 *             PoolException from pool.
	 */
	long create(T obj) throws DAOException;

	/**
	 * Returns all records from {@code <T>} table
	 * 
	 * @return {@code List} of entities
	 * @throws DAOException
	 *             exception that handles SQLException from database and
	 *             PoolException from pool.
	 */
	List<T> read() throws DAOException;

	/**
	 * Read single {@code T} by id.
	 * 
	 * @param id
	 *            T id
	 * @return {@code T} entity
	 * @throws DAOException
	 *             exception that handles SQLException from database and
	 *             PoolException from pool.
	 */
	T readById(long id) throws DAOException;

	/**
	 * Updates table {@code <T>} according to {@code obj} contents
	 * 
	 * @param obj
	 *            Entity transfer object
	 * @return number of updated rows
	 * @throws DAOException
	 *             exception that handles SQLException from database and
	 *             PoolException from pool.
	 */
	long update(T obj) throws DAOException;

	/**
	 * 
	 * Deletes all rows with id from {@code id} array
	 * 
	 * @param id
	 *            array of rows id's
	 * @throws DAOException
	 *             exception that handles SQLException from database and
	 *             PoolException from pool.
	 */
	void delete(long id) throws DAOException;

}
