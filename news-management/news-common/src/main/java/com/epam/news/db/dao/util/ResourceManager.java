package com.epam.news.db.dao.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.datasource.DataSourceUtils;

/**
 * Resource manager class for DAO classes
 * 
 * @author Andrei_Nemelouski
 *
 */
public class ResourceManager {

	private static Logger logger = Logger.getLogger(ResourceManager.class);

	/**
	 * Frees resources.
	 * @param conn {@code Connection} to close. Can be null.
	 * @param st {@code Statement} to close. Can be null.
	 * @param rs {@code ResultSet} to close. Can be null.
	 */
	public static void closeResources(DataSource ds, Connection conn, Statement st, ResultSet rs) {
		if (rs != null) closeResultSet(rs);
		if (st != null) closeStatement(st);
		if (conn != null) DataSourceUtils.releaseConnection(conn, ds);		
	}
	
	public static void closeResources(DataSource ds, Connection conn, Statement st) {
		closeResources(ds, conn, st, null);
	}

	/**
	 * Closes {@code ResultSet}
	 * 
	 * @param rs
	 *            {@code ResultSet} to close
	 *
	 */
	private static void closeResultSet(ResultSet rs){
		try {
			rs.close();
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
	}

	/**
	 * Closes {@code Statement}
	 * 
	 * @param statement
	 *            {@code Statement} to close
	 * 
	 */
	private static void closeStatement(Statement statement){
		try {
			statement.close();
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
	}
	
}
