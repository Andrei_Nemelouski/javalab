package com.epam.news.entity.representation;

import java.io.Serializable;
import java.util.List;

import com.epam.news.entity.dto.NewsTO;

public class Page implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3014524527411562337L;
	private List<NewsTO> news;
	private int currentPage;
	private int numberOfPages;
	private int newsPerPage;	
	
	public List<NewsTO> getNews() {
		return news;
	}
	public void setNews(List<NewsTO> news) {
		this.news = news;
	}
	public int getCurrentPage() {
		return currentPage;
	}
	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}	
	public int getNumberOfPages() {
		return numberOfPages;
	}
	public void setNumberOfPages(int numberOfPages) {
		this.numberOfPages = numberOfPages;
	}
	public int getNewsPerPage() {
		return newsPerPage;
	}
	public void setNewsPerPage(int newsPerPage) {
		this.newsPerPage = newsPerPage;
	}
	
	

}
