package com.epam.news.service;

import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.epam.news.entity.Author;
import com.epam.news.exceptions.ServiceException;

@Transactional(readOnly = true, propagation = Propagation.SUPPORTS, isolation = Isolation.READ_COMMITTED, rollbackFor = {
		ServiceException.class, RuntimeException.class })
public interface IAuthorService extends IService<Author> {

	/**
	 * Set author with {@code id} as expired. After this operation author will
	 * be no longer available to create news.
	 * 
	 * @param id
	 *            author Id
	 * @throws ServiceException
	 *             wrapper exception for exceptions from DAO layer
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	void setExpired(long id) throws ServiceException;

	/**
	 * Read single {@code Author} by News id.
	 * 
	 * @param id
	 *            News id
	 * @return {@code Author} entity
	 * @throws ServiceException
	 *             wrapper exception for exceptions from DAO layer
	 */
	Author readByNewsId(long id) throws ServiceException;

	/**
	 * Creates new record or updates existing using {@code obj} content
	 * 
	 * @param obj
	 *            Author entity
	 * @return author id if record was created and 0 if it was updated
	 * @throws ServiceException
	 *             wrapper exception for exceptions from DAO layer
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	long createOrUpdate(Author obj) throws ServiceException;

	/**
	 * Bind author with {@code id} to news record with {@code id}
	 * 
	 * @param newsId
	 *            news id
	 * @param authorId
	 *            author to bind id
	 * @throws ServiceException
	 *             wrapper exception for exceptions from DAO layer
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	void bindToNews(long newsId, long authorId) throws ServiceException;

}
