package com.epam.news.db.dao.jdbc;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.news.db.dao.NewsDAO;
import com.epam.news.db.dao.util.QueryBuilder;
import com.epam.news.db.dao.util.ResourceManager;
import com.epam.news.entity.Tag;
import com.epam.news.entity.dto.NewsTO;
import com.epam.news.entity.representation.Filter;
import com.epam.news.exceptions.DAOException;
import com.epam.news.mapper.IMapper;

/**
 * INewsDAO interface implementation class for operations with News table.
 * 
 * @author Andrei_Nemelouski
 *
 */
public class JDBCNewsDAO implements NewsDAO {
	// Queries
	private final String CREATE = "INSERT INTO news (NEWS_ID, SHORT_TEXT, FULL_TEXT, TITLE, CREATION_DATE, MODIFICATION_DATE) VALUES (NEWS_ID_SEQ.nextval, ?, ?, ?, ?, ?)";
	private final String UPDATE = "UPDATE news SET SHORT_TEXT=?, FULL_TEXT=?, TITLE=?, MODIFICATION_DATE=? WHERE news_id=?";
	private final String DELETE = "DELETE FROM news WHERE news_id IN ";
	private final String DELETE_NEWS_AUTHOR = "DELETE FROM news_author WHERE news_id IN";
	private final String DELETE_NEWS_TAG = "DELETE FROM news_tag WHERE news_id IN";
	private final String DELETE_COMMENTS = "DELETE FROM comments WHERE news_id IN";
	private final String READ_BY_ID = "SELECT n.news_id, n.short_text, n.full_text, n.title, n.creation_date, n.modification_date FROM news n WHERE n.news_id=?";
	private final String READ_ALL = "select n.news_id, n.short_text, n.full_text, n.title, n.creation_date, n.modification_date FROM news n ";
	private final String READ_NEWS_ID = "SELECT n.news_id FROM News n";
	private final String[] GENERATED_ID = { "NEWS_ID" };
	@Autowired
	@Qualifier("dataSource")
	private DataSource dataSource;
	@Autowired
	private IMapper<NewsTO> newsMapper;

	/**
	 * Creates new record in the {@code News} table
	 * 
	 * @param obj
	 *            Entity transfer object
	 * @return id of created {@code News}
	 * @throws DAOException
	 *             exception that handles SQLException from database
	 */
	@Override
	public long create(NewsTO news) throws DAOException {
		Connection conn = DataSourceUtils.getConnection(dataSource);
		PreparedStatement ps = null;
		ResultSet rs = null;
		long result = 0L;
		try {
			ps = conn.prepareStatement(CREATE, GENERATED_ID);
			ps.setString(1, news.getShortText());
			ps.setString(2, news.getFullText());
			ps.setString(3, news.getTitle());
			ps.setTimestamp(4, new Timestamp(news.getCreationDate().getTime()));
			ps.setDate(5, new Date(news.getModificationDate().getTime()));
			ps.executeUpdate();
			rs = ps.getGeneratedKeys();
			while (rs.next()) {
				result = rs.getLong(1);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			ResourceManager.closeResources(dataSource, conn, ps, rs);
		}
		return result;
	}

	/**
	 * Returns all records from {@code News} table
	 * 
	 * @return {@code List} of entities
	 * @throws DAOException
	 *             exception that handles SQLException from database
	 */
	@Override
	public List<NewsTO> read() throws DAOException {
		Connection conn = DataSourceUtils.getConnection(dataSource);
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<NewsTO> news = new LinkedList<>();
		NewsTO n = null;
		try {
			ps = conn.prepareStatement(QueryBuilder.construct(READ_ALL));
			rs = ps.executeQuery();
			while (rs.next()) {
				n = newsMapper.construct(rs);
				news.add(n);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			ResourceManager.closeResources(dataSource, conn, ps, rs);
		}
		return news;

	}

	@Override
	public List<NewsTO> read(int newsPerPage, int currentPage, Filter filter)
			throws DAOException {
		Connection conn = DataSourceUtils.getConnection(dataSource);
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<NewsTO> news = new LinkedList<>();
		NewsTO n = null;
		int maxRow = newsPerPage * currentPage;
		int minRow = maxRow - newsPerPage + 1;

		try {
			ps = conn
					.prepareStatement(QueryBuilder.construct(READ_ALL, filter));

			int paramIdx = 1;
			if (filter.getAuthor() != null) {
				ps.setLong(paramIdx++, filter.getAuthor().getId());
			}
			if (filter.getTag() != null) {
				for (Tag tag : filter.getTag()) {
					ps.setLong(paramIdx++, tag.getId());
				}
			}
			ps.setInt(paramIdx++, maxRow);
			ps.setInt(paramIdx++, minRow);

			rs = ps.executeQuery();
			while (rs.next()) {
				n = newsMapper.construct(rs);
				news.add(n);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			ResourceManager.closeResources(dataSource, conn, ps, rs);
		}
		return news;
	}

	/**
	 * Fetch news by id
	 * 
	 * @param id
	 *            news id
	 * @return News transfer object
	 * @throws DAOException
	 *             exception that handles SQLException from database.
	 */

	@Override
	public NewsTO readById(long id) throws DAOException {
		Connection conn = DataSourceUtils.getConnection(dataSource);
		PreparedStatement ps = null;
		ResultSet rs = null;
		NewsTO news = null;
		try {
			ps = conn.prepareStatement(QueryBuilder.construct(READ_BY_ID));
			ps.setLong(1, id);
			rs = ps.executeQuery();
			if (rs.next()) {
				news = newsMapper.construct(rs);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			ResourceManager.closeResources(dataSource, conn, ps, rs);
		}
		return news;
	}

	/**
	 * Fetch next and previous news id for article with {@code id}
	 * 
	 * @param id
	 *            news id
	 * @param filter
	 *            filtration info handler class
	 * @return long[] where [0] element is previous news id and [1] element is
	 *         next news id
	 * @throws DAOException
	 *             exception that handles SQLException from database.
	 */
	@Override
	public long[] readNextPrev(long id, Filter filter) throws DAOException {
		Connection conn = DataSourceUtils.getConnection(dataSource);
		PreparedStatement ps = null;
		ResultSet rs = null;
		long[] result = new long[2];
		try {
			ps = conn.prepareStatement(QueryBuilder.constructNextPrev(
					READ_NEWS_ID, filter));
			int paramIdx = 1;
			if (filter.getAuthor() != null) {
				ps.setLong(paramIdx++, filter.getAuthor().getId());
			}
			if (filter.getTag() != null) {
				for (Tag tag : filter.getTag()) {
					ps.setLong(paramIdx++, tag.getId());
				}
			}
			ps.setLong(paramIdx++, id);
			rs = ps.executeQuery();
			if (rs.next()) {
				result[0] = rs.getLong("id_prev");
				result[1] = rs.getLong("id_aft");
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			ResourceManager.closeResources(dataSource, conn, ps, rs);
		}
		return result;
	}

	/**
	 * Updates table {@code News} according to {@code obj} contents
	 * 
	 * @param obj
	 *            Entity transfer object
	 * @return number of updated rows
	 * @throws DAOException
	 *             exception that handles SQLException from database
	 */

	@Override
	public long update(NewsTO news) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = dataSource.getConnection();
			ps = conn.prepareStatement(UPDATE);
			ps.setString(1, news.getShortText());
			ps.setString(2, news.getFullText());
			ps.setString(3, news.getTitle());
			//ps.setTimestamp(4, new Timestamp(news.getCreationDate().getTime()));
			ps.setDate(4, new Date(news.getModificationDate().getTime()));
			ps.setLong(5, news.getId());
			return ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			ResourceManager.closeResources(dataSource, conn, ps);
		}
	}

	/**
	 * 
	 * Deletes all rows with id from {@code id} array
	 * 
	 * @param id
	 *            array of rows id's
	 * @throws DAOException
	 *             exception that handles SQLException from database.
	 */
	@Override
	public void delete(long[] id) throws DAOException {
		Connection conn = null;
		Statement st = null;
		String news_command = createDeleteCommand(DELETE, id);
		String news_aut_command = createDeleteCommand(DELETE_NEWS_AUTHOR, id);
		String news_tag_command = createDeleteCommand(DELETE_NEWS_TAG, id);
		String news_comments = createDeleteCommand(DELETE_COMMENTS, id);
		try {
			conn = dataSource.getConnection();
			st = conn.createStatement();
			st.addBatch(news_tag_command);
			st.addBatch(news_aut_command);
			st.addBatch(news_comments);
			st.addBatch(news_command);
			st.executeBatch();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			ResourceManager.closeResources(dataSource, conn, st);
		}

	}

	@Override
	public void delete(long id) throws DAOException {
		long[] delete = { id };
		delete(delete);
	}

	/**
	 * Method for sql delete command creation
	 * 
	 * @param command
	 *            SQL command ended with 'IN'
	 * @param id
	 *            id's array
	 * @return completed SQL command
	 */
	private String createDeleteCommand(String command, long[] id) {
		StringBuffer sb = new StringBuffer(command);
		sb.append("(");
		for (int i = 0; i < id.length; i++) {
			sb.append(id[i]);
			if (i != (id.length - 1)) {
				sb.append(", ");
			} else {
				sb.append(")");
			}
		}
		return sb.toString();
	}

	@Override
	public int countNews(Filter filter) throws DAOException {
		Connection conn = DataSourceUtils.getConnection(dataSource);
		// TODO check is right statement class used
		PreparedStatement ps = null;
		ResultSet rs = null;
		Set<Long> result = new HashSet<>();
		String command = QueryBuilder.constructCounter(filter);
		try {
			ps = conn.prepareStatement(command);
			// TODO bad
			int paramIdx = 1;
			if (filter.getAuthor() != null) {
				ps.setLong(paramIdx++, filter.getAuthor().getId());
			}
			if (filter.getTag() != null) {
				for (Tag tag : filter.getTag()) {
					ps.setLong(paramIdx++, tag.getId());
				}
			}
			rs = ps.executeQuery();
			while (rs.next()) {
				result.add(rs.getLong(1));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			ResourceManager.closeResources(dataSource, conn, ps, rs);
		}
		return result.size();
	}
}
