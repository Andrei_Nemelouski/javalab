package com.epam.news.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.epam.news.db.dao.AuthorDAO;
import com.epam.news.db.dao.NewsDAO;
import com.epam.news.db.dao.TagDAO;
import com.epam.news.entity.dto.NewsTO;
import com.epam.news.entity.representation.Filter;
import com.epam.news.exceptions.DAOException;
import com.epam.news.exceptions.ServiceException;
import com.epam.news.service.INewsService;

/**
 * Service class for operations with {@code News}
 * 
 * @author Andrei_Nemelouski
 *
 */
public class NewsService implements INewsService {

	@Autowired
	@Qualifier("newsDao")
	private NewsDAO newsDao;
	@Autowired
	@Qualifier("authorDao")
	private AuthorDAO authorDao;
	@Autowired
	@Qualifier("tagDao")
	private TagDAO tagDao;

	/**
	 * Add News with author and tags using appropriate DAO classes
	 * 
	 * @param dto
	 *            Service layer transfer object
	 * @return ServiceDTO completes with id's of added rows
	 * @throws ServiceException
	 *             wrapper exception for exceptions from DAO layer
	 */
	@Override
	public long create(NewsTO nto) throws ServiceException {
		try {
			return newsDao.create(nto);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	/**
	 * Return's all news from database
	 * 
	 * @return List<NewsTO> list of all news
	 * @throws ServiceException
	 *             wrapper exception for exceptions from DAO layer
	 */
	@Override
	public List<NewsTO> read() throws ServiceException {
		List<NewsTO> newsList = null;
		try {
			newsList = newsDao.read();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return newsList;

	}
	
	@Override
	public List<NewsTO> read(int newsPerPage, int currentPage, Filter filter) throws ServiceException {
		List<NewsTO> newsList = null;
		try {
			newsList = newsDao.read(newsPerPage, currentPage,filter);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return newsList;
	}
	
	@Override
	public NewsTO readById(long id) throws ServiceException {
		try {
			return newsDao.readById(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public long[] readNextPrev(long id, Filter filter) throws ServiceException {
		try {
			return newsDao.readNextPrev(id, filter);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * Change news according to information received in {@code NewsTO}
	 * 
	 * @param nto
	 *            News transfer object
	 * @return number of updated rows
	 * @throws ServiceException
	 *             wrapper exception for exceptions from DAO layer
	 */
	@Override
	public void update(NewsTO nto) throws ServiceException {
		try {
			newsDao.update(nto);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	@Override
	public long createOrUpdate(NewsTO obj, long authorId, long[] tagId)
			throws ServiceException {
		long result = obj.getId();
		try {
			if (result == 0) {
				Date creationDate = obj.getModificationDate();
				obj.setCreationDate(creationDate);
				result = newsDao.create(obj);
			} else {
				newsDao.update(obj);
			}
			authorDao.bindToNews(result, authorId);
			if (tagId != null) {
				tagDao.bindToNews(result, tagId);
			}

		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return result;
	}

	/**
	 * Delete news according to id's array
	 * 
	 * @param delete
	 *            array of id's to delete
	 * @throws ServiceException
	 *             wrapper exception for exceptions from DAO layer
	 */
	@Override
	public void delete(long[] delete) throws ServiceException {
		try {
			newsDao.delete(delete);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	@Override
	public void delete(long delete) throws ServiceException {
		try {
			newsDao.delete(delete);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}		
	}

	@Override
	public int countNews(Filter filter) throws ServiceException {
		try {
			return newsDao.countNews(filter);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
}
