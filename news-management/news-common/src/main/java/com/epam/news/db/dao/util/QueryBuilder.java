package com.epam.news.db.dao.util;

import org.apache.log4j.Logger;

import com.epam.news.entity.representation.Filter;

public class QueryBuilder {

	private static Logger logger = Logger.getLogger(QueryBuilder.class);

	private static final String READ_BY_AUTHOR_ID = ", News_Author na WHERE n.news_id=na.news_id AND na.author_id=?";
	private static final String READ_BY_TAG_ID = ", News_Tag nt WHERE n.news_id=nt.news_id AND nt.tag_id IN";
	private static final String READ_BY_TAG_AND_AUTHOR_ID = ", News_Author na, News_Tag nt WHERE n.news_id=na.news_id AND n.news_id=nt.news_id And na.author_id=? And nt.tag_id IN";
	private static final String COUNT_NEWS = "SELECT n.news_id FROM News n";
	private static final String COUNT_COMMENTS_BEGIN = " SELECT a.news_id, a.short_text, a.full_text, a.title, a.creation_date, a.modification_date, c.cntd"
			+ " from (";
	private static final String COUNT_COMMENTS_END = ") a"
			+ " LEFT JOIN (SELECT news_id, COUNT(*) cntd FROM Comments GROUP BY news_id) c ON a.news_id = c.news_id order by cntd Desc NULLS LAST";
	private static final String READ_FIXED_NUMBER_BEGIN = "select * from ("
			+ " select a.*, ROWNUM rnum from ( ";
	private static final String READ_FIXED_NUMBER_END = " ) a where rownum <= ?) where rnum >= ?";
	private static final String NEXT_PREV_BEGIN = "SELECT e.news_id, e.id_prev, e.id_aft from ( "
			+ " SELECT b.news_id, LEAD(b.news_id, 1, 0) OVER (ORDER BY b.cntd NULLS LAST) AS id_aft, "
			+ " LAG(b.news_id, 1, 0) OVER (ORDER BY b.cntd NULLS LAST) AS id_prev  "
			+ " FROM (SELECT a.news_id, c.cntd from (";
	private static final String NEXT_PREV_END = ") a "
			+ "LEFT JOIN (SELECT news_id, COUNT(*) cntd FROM Comments GROUP BY news_id) c ON a.news_id = c.news_id order by cntd Desc NULLS LAST) b "
			+ " ) e WHERE e.news_id=?";
	private static final String TAG_BIND_BEGIN = "DECLARE " 
			+"TYPE id_Table IS TABLE OF NUMBER(20) "
			+"INDEX BY BINARY_INTEGER; "
			+"tag_id_Table id_Table; "
		  +"id_cnt BINARY_INTEGER; "
		  +"newsId NUMBER(20) := ?; "
		  +"cur_state NUMBER(20); "
		  +"BEGIN ";
		/*tag_id_Table(1) := 1;
		tag_id_Table(3) := 122;
		tag_id_Table(2) := 3;*/
	private static final String TAG_BIND_END = 
		" delete from news_tag where news_tag.news_id=newsId; "
		+"id_cnt := tag_id_Table.FIRST; "
		+"LOOP "
		+"begin "
		+"if(tag_id_Table.exists(id_cnt)) then "
		+"select news_id into cur_state from news_tag where news_tag.tag_id=tag_id_Table(id_cnt) and news_id=newsId; "
		+"end if; "
		+"exception "
		+"WHEN NO_DATA_FOUND THEN "
		        +"insert into news_tag(news_id,tag_id) values(newsId, tag_id_Table(id_cnt)); "
		+"EXIT when id_cnt = tag_id_Table.LAST; "
		+"id_cnt := tag_id_Table.NEXT(id_cnt); "
		+"end; " 
		+"END LOOP; "
		+"END;";

	public static String construct(String base) {
		return construct(base, null);
	}

	public static String construct(String base, Filter filter) {
		StringBuilder result = new StringBuilder();
		// TODO bad
		if (filter != null) {
			result.append(READ_FIXED_NUMBER_BEGIN);
			result.append(COUNT_COMMENTS_BEGIN);
			result.append(base);
			checkAuthorTagSorting(result, filter);
			result.append(COUNT_COMMENTS_END);
			result.append(READ_FIXED_NUMBER_END);
		} else {
			result.append(COUNT_COMMENTS_BEGIN);
			result.append(base);
			result.append(COUNT_COMMENTS_END);
		}
		logger.info(result.toString());
		return result.toString();
	}

	public static String constructNextPrev(String base, Filter filter) {
		StringBuilder result = new StringBuilder();
		result.append(NEXT_PREV_BEGIN);
		result.append(base);
		checkAuthorTagSorting(result, filter);
		result.append(NEXT_PREV_END);
		logger.info(result.toString());
		return result.toString();
	}

	public static String constructCounter(Filter filter) {
		StringBuilder result = new StringBuilder();
		result.append(COUNT_NEWS);
		checkAuthorTagSorting(result, filter);
		logger.info(result.toString());
		return result.toString();

	}
	
	public static String constructTagBindCallable(long[] tagId){
		StringBuilder sb = new StringBuilder(TAG_BIND_BEGIN);
		for(int i=0; i<tagId.length;i++){
			if(tagId[i]!=0){
				sb.append("tag_id_Table(");
				sb.append(i);
				sb.append(") := ?; ");
			}			
		}
		sb.append(TAG_BIND_END);
		logger.info(sb.toString());
		return sb.toString();
	}

	private static void checkAuthorTagSorting(StringBuilder in, Filter filter) {
		if (filter.getAuthor() != null && filter.getTag() != null) {
			in.append(READ_BY_TAG_AND_AUTHOR_ID);
			in.append("(");
			int length = filter.getTag().size();
			for (int i = 0; i < length; i++) {
				in.append("?");
				if (i != (length - 1)) {
					in.append(", ");
				} else {
					in.append(")");
				}
			}

		} else {
			if (filter.getAuthor() != null) {
				in.append(READ_BY_AUTHOR_ID);
			} else {
				if (filter.getTag() != null) {
					in.append(READ_BY_TAG_ID);
					in.append("(");
					int length = filter.getTag().size();
					for (int i = 0; i < length; i++) {
						in.append("?");
						if (i != (length - 1)) {
							in.append(", ");
						} else {
							in.append(")");
						}
					}
				}
			}

		}
	}

}
