package com.epam.news.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.epam.news.entity.Author;

public class AuthorMapper implements IMapper<Author> {

	@Override
	public <F extends ResultSet> Author construct(F from) throws SQLException {
		Author aut = new Author();
		aut.setId(from.getInt("author_id"));
		aut.setName(from.getString("author_name"));		
		return aut;
	}

}
