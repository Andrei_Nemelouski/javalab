package com.epam.news.db.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.epam.news.db.dao.UserDAO;
import com.epam.news.db.dao.util.ResourceManager;
import com.epam.news.entity.dto.UserTO;
import com.epam.news.exceptions.DAOException;
import com.epam.news.mapper.IMapper;

public class JDBCUserDAO implements UserDAO {

	private String READ_BY_LOGIN = "SELECT u.user_name, u.password, r.role_name FROM Users u, Roles r WHERE u.login = ? AND u.user_id = r.user_id";
	@Autowired
	@Qualifier("dataSource")
	private DataSource dataSource;
	@Autowired
	private IMapper<UserTO> userMapper;

	@Override
	public UserTO readByLogin(String login) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		UserTO user = null;
		try {
			conn = dataSource.getConnection();
			ps = conn.prepareStatement(READ_BY_LOGIN);
			ps.setString(1, login);
			rs = ps.executeQuery();
			if (rs.next()) {
				user = userMapper.construct(rs);
			}
			if (user != null) {
				user.setLogin(login);
			}
			return user;
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			ResourceManager.closeResources(dataSource, conn, ps, rs);
		}
	}

	@Override
	public long create(UserTO obj) throws DAOException {
		return 0;
	}

	@Override
	public List<UserTO> read() throws DAOException {
		return null;
	}

	@Override
	public long update(UserTO obj) throws DAOException {
		throw new UnsupportedOperationException();
	}

	@Override
	public void delete(long id) throws DAOException {
		throw new UnsupportedOperationException();

	}

	@Override
	public UserTO readById(long id) throws DAOException {
		throw new UnsupportedOperationException();
	}
}
