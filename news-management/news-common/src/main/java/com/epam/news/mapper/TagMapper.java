package com.epam.news.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.epam.news.entity.Tag;

public class TagMapper implements IMapper<Tag> {

	@Override
	public <F extends ResultSet> Tag construct(F from) throws SQLException {
		Tag tag = new Tag();
		tag.setId(from.getLong("tag_id"));
		tag.setTagName(from.getString("tag_name"));
		return tag;
	}

}
