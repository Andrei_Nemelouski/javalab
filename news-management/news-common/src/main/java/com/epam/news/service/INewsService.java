package com.epam.news.service;

import java.util.List;

import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.epam.news.entity.dto.NewsTO;
import com.epam.news.entity.representation.Filter;
import com.epam.news.exceptions.ServiceException;

@Transactional(readOnly = true, propagation = Propagation.SUPPORTS, isolation = Isolation.READ_COMMITTED, rollbackFor = {
		ServiceException.class, RuntimeException.class })
public interface INewsService extends IService<NewsTO> {

	/**
	 * Read news accoring to filtration, current page and news number per page.
	 * 
	 * @param newsPerPage
	 *            number of news presented on single page
	 * @param currentPage
	 *            current page number
	 * @param filter
	 *            filtration info handler
	 * @return List of news transfer objects
	 * @throws ServiceException
	 *             exception that handles exceptions from DAO layer.
	 */
	List<NewsTO> read(int newsPerPage, int currentPage, Filter filter)
			throws ServiceException;

	/**
	 * Fetch next and previous news id for article with {@code id}
	 * 
	 * @param id
	 *            news id
	 * @param filter
	 *            filtration info handler class
	 * @return long[] where [0] element is previous news id and [1] element is
	 *         next news id
	 * @throws ServiceException
	 *             exception that handles exceptions from DAO layer.
	 */
	long[] readNextPrev(long id, Filter filter) throws ServiceException;

	/**
	 * Count all news according to filtration info in {@code Filter}
	 * 
	 * @param filter
	 *            filtration info handler
	 * @return number of news
	 * @throws ServiceException
	 *             exception that handles exceptions from DAO layer.
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	int countNews(Filter filter) throws ServiceException;

	/**
	 * Delete news according to id array {@code delete}
	 * 
	 * @param delete
	 *            array of news id
	 * @throws ServiceException
	 *             exception that handles exceptions from DAO layer.
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	void delete(long[] delete) throws ServiceException;

	/**
	 * Create new or update existing news record according to NewsTO {@code obj}
	 * , author id {@code author id} and tag id's array {@code tagId}.
	 * 
	 * @param obj
	 *            news transfer object
	 * @param authorId
	 *            news author Id
	 * @param tagId
	 *            news tags id array.
	 * @return created record id or 0 if record was updates
	 * @throws ServiceException
	 *             exception that handles exceptions from DAO layer.
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	long createOrUpdate(NewsTO obj, long authorId, long[] tagId)
			throws ServiceException;

}
