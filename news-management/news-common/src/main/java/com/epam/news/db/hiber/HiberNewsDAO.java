package com.epam.news.db.hiber;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.epam.news.db.dao.NewsDAO;
import com.epam.news.entity.dto.NewsTO;
import com.epam.news.entity.representation.Filter;
import com.epam.news.exceptions.DAOException;

public class HiberNewsDAO implements NewsDAO {
	
    private SessionFactory factory;
	
	{
		try {
			factory = new Configuration().configure()
					.buildSessionFactory();
		} catch (Throwable e) {
			throw new ExceptionInInitializerError(e);
		}
	}

	@Override
	public long create(NewsTO obj) throws DAOException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<NewsTO> read() throws DAOException {
		Session s = factory.getCurrentSession();
		List<NewsTO> n;
		Transaction t = s.beginTransaction();
		n = s.getNamedQuery("findAll").list();
		t.commit();
		return n;
		
	}

	@Override
	public NewsTO readById(long id) throws DAOException {
		Session s = factory.getCurrentSession();
		NewsTO n;
		Transaction t = s.beginTransaction();
		Query q = s.getNamedQuery("find");
		q.setParameter("news_id", id);
		
		n = (NewsTO) q.uniqueResult();
		t.commit();
		return n;
		
	}

	@Override
	public long update(NewsTO obj) throws DAOException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void delete(long id) throws DAOException {
		// TODO Auto-generated method stub

	}

	@Override
	public List<NewsTO> read(int newsPerPage, int currentPage, Filter filter)
			throws DAOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long[] readNextPrev(long id, Filter filter) throws DAOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int countNews(Filter filter) throws DAOException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void delete(long[] delete) throws DAOException {
		// TODO Auto-generated method stub

	}
	
	private void destroyDao(){
		factory.close();
	}

}
