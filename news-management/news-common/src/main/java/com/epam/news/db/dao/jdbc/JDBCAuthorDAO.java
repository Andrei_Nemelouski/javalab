package com.epam.news.db.dao.jdbc;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.news.db.dao.AuthorDAO;
import com.epam.news.db.dao.util.ResourceManager;
import com.epam.news.entity.Author;
import com.epam.news.exceptions.DAOException;
import com.epam.news.mapper.IMapper;

/**
 * DAO class for operations with Authors table.
 * 
 * @author Andrei_Nemelouski
 *
 */
public class JDBCAuthorDAO implements AuthorDAO {

	private final static String READ = "SELECT author.author_id, author.author_name FROM Author WHERE author.expired IS NULL";
	private final static String READ_BY_NEWS_ID = "SELECT a.author_id, a.author_name FROM Author a INNER JOIN news_author na ON a.author_id=na.author_id WHERE na.news_id=?";
	private final static String READ_BY_ID = "SELECT a.author_id, a.author_name FROM Author a WHERE a.author_id=? AND a.expired IS NULL";
	private final static String UPDATE = "UPDATE Author SET author_name=? WHERE author_id=?";
	private final static String SET_EXPIRED = "UPDATE Author SET expired=? WHERE author_id=? ";
	private final static String CREATE = "declare "
			+ "aut_name NVARCHAR2(20) := ?; "
			+ "cur_state NVARCHAR2(20); "
			+ "resultId NUMBER(10); "
			+ "begin "
			+ "select author.author_name INTO cur_state FROM author WHERE author_name=aut_name; "
			+ "exception "
			+ "WHEN NO_DATA_FOUND THEN " 
			+ "cur_state := null; "
			+ "if (cur_state is NULL) then "
			+ "begin "
			+ "insert into author(author_id, author_name) values (author_id_seq.nextval, aut_name); "
			+ "end; " + "end if; " 
			+ "select author.author_id INTO resultId FROM author WHERE author_name=aut_name; "
			+ "? := resultId; "			
			+ "end;";
	private final String BIND = "declare " 
			+ "newsId NUMBER(20) := ?; "  
			+ "authorId NUMBER(20) := ?; " 
			+ "cur_state NUMBER(20); " 
			+ "begin " 
			+ "select news_author.author_id INTO cur_state FROM news_author WHERE news_id = newsId; "
			+ "exception "
			+ "WHEN NO_DATA_FOUND THEN "
			+ "cur_state := null; "
			+ "if (cur_state is NULL) then "
			+ "insert into NEWS_AUTHOR(NEWS_ID, AUTHOR_ID) values (newsId, authorId); "
			+ "else " 
			+ "update news_author Set news_author.author_id=authorId where news_author.news_id=newsId; "
			+ "end if; " 
			+ "end; ";		
	@Autowired
	@Qualifier("dataSource")
	private DataSource dataSource;
	@Autowired
	private IMapper<Author> authorMapper;

	/**
	 * Creates new record in the {@code Author, News_Author} table
	 * 
	 * @param obj
	 *            Entity transfer object
	 * @return id of created {@code Author}
	 * @throws DAOException
	 *             exception that handles SQLException from database
	 */
	@Override
	public long create(Author obj) throws DAOException {
		Connection conn = DataSourceUtils.getConnection(dataSource);
		CallableStatement cs = null;
		long result = 0;
		try {
			cs = conn.prepareCall(CREATE);
			cs.setString(1, obj.getName());
			cs.registerOutParameter(2, Types.INTEGER);
			cs.execute();
			result = cs.getInt(2);
			} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			ResourceManager.closeResources(dataSource, conn, cs);
		}
		return result;
	}
	
	@Override
	public void bindToNews(long id, long authorId) throws DAOException {
		Connection conn = DataSourceUtils.getConnection(dataSource);
		CallableStatement cs = null;
		try {
			cs = conn.prepareCall(BIND);
			cs.setLong(1, id);
			cs.setLong(2, authorId);
			cs.execute();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			ResourceManager.closeResources(dataSource, conn, cs);
		}		
	}

	/**
	 * Returns all records from {@code Author} table
	 * 
	 * @return {@code List} of entities
	 * @throws DAOException
	 *             exception that handles SQLException from database
	 */
	@Override
	public List<Author> read() throws DAOException {
		Connection conn = DataSourceUtils.getConnection(dataSource);
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Author> list = new ArrayList<>();
		try {
			ps = conn.prepareStatement(READ);
			rs = ps.executeQuery();
			while (rs.next()) {
				list.add(authorMapper.construct(rs));
			}
			return list;
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			ResourceManager.closeResources(dataSource, conn, ps, rs);
		}
	}
	
	@Override
	public Author readById(long id) throws DAOException {
		Connection conn = DataSourceUtils.getConnection(dataSource);
		PreparedStatement ps = null;
		ResultSet rs = null;
		Author aut = null;
		try {
			ps = conn.prepareStatement(READ_BY_ID);
			ps.setLong(1, id);
			rs = ps.executeQuery();
			if (rs.next()) {
				aut = authorMapper.construct(rs);
			}
			return aut;
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			ResourceManager.closeResources(dataSource, conn, ps, rs);
		}
	}
	
	@Override
	public Author readByNewsId(long id) throws DAOException {
		Connection conn = DataSourceUtils.getConnection(dataSource);
		PreparedStatement ps = null;
		ResultSet rs = null;
		Author aut = null;
		try {
			ps = conn.prepareStatement(READ_BY_NEWS_ID);
			ps.setLong(1, id);
			rs = ps.executeQuery();
			if (rs.next()) {
				aut = (authorMapper.construct(rs));
			}
			return aut;
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			ResourceManager.closeResources(dataSource, conn, ps, rs);
		}
	}
	
	/**
	 * Updates table {@code Author} according to {@code obj} contents
	 * 
	 * @param obj
	 *            Entity transfer object
	 * @return number of updated rows
	 * @throws DAOException
	 *             exception that handles SQLException from database
	 */
	@Override
	public long update(Author obj) throws DAOException {
		Connection conn = DataSourceUtils.getConnection(dataSource);
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(UPDATE);
			ps.setString(1, obj.getName());
			ps.setLong(2, obj.getId());
			return ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			ResourceManager.closeResources(dataSource, conn, ps);
		}

	}

	/**
	 * 
	 * Deletes all rows with id from {@code id} array
	 * 
	 * @param id
	 *            array of rows id's
	 * @throws DAOException
	 *             exception that handles SQLException from database
	 */
	@Override
	public void delete(long id) throws DAOException {
		
	}

	@Override
	public void setExpired(long id) throws DAOException {
		// TODO Auto-generated method stub
		Connection conn = DataSourceUtils.getConnection(dataSource);
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(SET_EXPIRED);
			ps.setTimestamp(1, new Timestamp(System.currentTimeMillis()));
			ps.setLong(2, id);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			ResourceManager.closeResources(dataSource, conn, ps);
		}
		
	}		
}
