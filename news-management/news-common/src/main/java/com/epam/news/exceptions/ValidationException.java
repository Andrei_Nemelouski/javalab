package com.epam.news.exceptions;

public class ValidationException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8857901708781716830L;
	private String message;
	
	public ValidationException(String message){
		this.message = message;
	}
	
	public String getMessage(){
		return message;
	}

}
