package com.epam.news.db.dao;

import java.util.List;

import com.epam.news.entity.Comment;
import com.epam.news.exceptions.DAOException;

/**
 * DAO interface for operations with Comments table
 * @author Andrei_Nemelouski
 *
 */
public interface CommentDAO extends Dao<Comment> {
	
	/**
	 * Read {@code Comments} by News id.
	 * 
	 * @param id
	 *            News id
	 * @return {@code List<Comment>}
	 * @throws DAOException
	 *             exception that handles SQLException from database and
	 *             PoolException from pool.
	 */
	List<Comment> readByNewsId(long id) throws DAOException;

}
