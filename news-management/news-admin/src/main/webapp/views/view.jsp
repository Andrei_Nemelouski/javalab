<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="pattern">
	<s:message code="date.pattern" />
</c:set>
<a href="/NewsManagement/admin"><s:message code="view.back" /></a>
<article class="full-article">
	<div class="article-head">
		<h3>
			<c:out value="${page.news[0].title}"></c:out>
		</h3>
		<span class="article-author">(<s:message code="content.author" />
			<c:out value="${page.news[0].author.name}"></c:out>)
		</span> <span class="article-head-date"><fmt:formatDate
				pattern="${pattern}" value="${page.news[0].creationDate}" /></span>
	</div>
	<div class="article-content">
		<p>
			<c:out value="${page.news[0].fullText}"></c:out>
		</p>
	</div>
	<div class="article-comments">
		<c:forEach var="comment" items="${page.news[0].comments}">
			<div class="comment">
				<span class="comment-date"><fmt:formatDate
						pattern="${pattern}" value="${comment.creationDate}" /></span>
				<div class="comment-text">
					<span><c:out value="${comment.text}"></c:out></span>
					<sf:form method="POST"
						action="/NewsManagement/admin/article/comment/delete"
						modelAttribute="comment">
						<sf:hidden path="id" value="${comment.id}" />
						<sf:hidden path="newsId" value="${page.news[0].id}" />
						<input type="submit" value="X" />
					</sf:form>
				</div>
			</div>
		</c:forEach>
		<div class="comment-input-wrapper">
			<sf:form method="POST" action="/NewsManagement/admin/article"
				modelAttribute="comment">
				<sf:errors path="text"/>
				<sf:textarea path="text" rows="4" cols="50" id="comment_text" />
				<sf:hidden path="newsId" value="${page.news[0].id}" />
				<input type="submit" value="<s:message code="view.comment"/>" />
			</sf:form>
		</div>
	</div>
</article>
<c:if test="${previousNewsId != 0}">
	<a href="/NewsManagement/admin/article/previous/${previousNewsId}"><s:message
			code="view.prev" /></a>
</c:if>
<c:if test="${nextNewsId != 0}">
	<a href="/NewsManagement/admin/article/next/${nextNewsId}"><s:message
			code="view.next" /></a>
</c:if>