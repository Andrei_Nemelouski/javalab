<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<style type="text/css">
.menu .author-editor a {
	color: orange;
}
</style>
<div class="author-editor">
	<c:forEach var="author" items="${authors}">
		<div class="author-form">
			<sf:form action="/NewsManagement/admin/authorEditor" method="POST"
				modelAttribute="author">
				<sf:hidden path="id" value="${author.id}" />
				<label><s:message code="editor.author.title" />:</label>
				<sf:input path="name" value="${author.name}" disabled="true" />
				<a class="edit"><s:message code="editor.edit" /></a>
				<div class="edit-block">
					<input type="submit" value="<s:message code="editor.update"/>">
					<a href="/NewsManagement/admin/authorEditor/expire?id=${author.id}"><s:message
							code="editor.expire" /></a>
				</div>
			</sf:form>
		</div>
	</c:forEach>

	<div class="author-creation-form">
		<sf:form action="/NewsManagement/admin/authorEditor" method="POST"
			modelAttribute="author">
			<label><s:message code="editor.author.add" />:</label>
			<sf:input id="save" path="name" />
			<input type="submit" value="<s:message code="editor.save"/>">
			<sf:errors path="name"/>
		</sf:form>
	</div>

</div>