<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="login-wrapper">
	<form method="post" action="<c:url value='j_spring_security_check'/>">
		<div class="login-input-wrapper">
			<label for="login"><s:message code="login.login" /></label><input
				id="login" name="j_username" type="text" />
		</div>
		<div class="password-input-wrapper">
			<label for="password"><s:message code="login.pass" /></label><input
				id="password" name="j_password" type="password" />
		</div>
		<%-- <small><a
							href="/account/resend_password">Forgot?</a></small></td> --%>
		<!-- <input id="remember_me" name="_spring_security_remember_me" type="checkbox" /> Флажок "запомнить меня"
						<label for="remember_me" class="inline">Remember me</label> -->
		<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />
			<span class="login-message">
			<c:if test="${not empty error}">
			<s:message code="login.fail" />
			</c:if>
			</span>
			 <input type="submit"
			value="<s:message code="login.apply" />" />

	</form>
</div>