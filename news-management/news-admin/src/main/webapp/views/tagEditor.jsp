<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<style type="text/css">
.menu .tag-editor a {
	color: orange;
}
</style>
<div class="tag-editor">
	<c:forEach var="tag" items="${tags}">
		<div class="tag-form">
			<sf:form action="/NewsManagement/admin/tagEditor" method="POST"
				modelAttribute="tag">
				<sf:hidden path="id" value="${tag.id}" />
				
				<label><s:message code="editor.tag.title" />:</label><sf:input
						id="name" path="tagName" value="${tag.tagName}" disabled="true"/>
				<a class="edit"><s:message code="editor.edit" /></a>
				<div class="edit-block">
					<input type="submit" value="<s:message code="editor.update"/>">
					<a href="/NewsManagement/admin/tagEditor/delete?id=${tag.id}"><s:message
							code="editor.delete" /></a>
				</div>
			</sf:form>
		</div>
	</c:forEach>
	<div class="tag-creation-form">
		<sf:form action="/NewsManagement/admin/tagEditor" method="POST"
			modelAttribute="tag">
			<label><s:message code="editor.tag.add" />:</label><sf:input
					id="save" path="tagName" />
			<input type="submit" value="<s:message code="editor.save"/>">
			<sf:errors path="tagName"/>			
		</sf:form>
	</div>

</div>
