<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>


<div class="error-message-wrapper">
	<span class="error-title"><s:message code="error.title"/></span> 
	<span class="error-message">
		<s:message code="error.message"/>
	</span>
	<img src="/NewsManagement/admin/resources/img/error-medved.jpg" alt="error"/>
</div>