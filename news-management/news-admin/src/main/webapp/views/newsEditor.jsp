<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<style type="text/css">
.menu .news-editor a {
	color: orange;
}
</style>
<div class="news-editor-wrapper">
	<sf:form action="/NewsManagement/admin/newsEditor" method="POST"
		modelAttribute="news">
		<div class="news-title">
			<label><s:message code="editor.news.title" />:</label>
			<sf:input path="title"></sf:input>
			<sf:errors path="title" />
		</div>
		<div class="news-date">
			<label><s:message code="editor.news.date" />:</label>
			<sf:input path="modificationDate"></sf:input>
			<sf:errors path="modificationDate" />
		</div>
		<div class="news-brief">
			<label><s:message code="editor.news.brief" />:</label>
			<sf:textarea path="shortText" rows="2" cols="50"></sf:textarea>
			<sf:errors path="shortText" />
		</div>
		<div class="news-content">
			<label><s:message code="editor.news.content" />:</label>
			<sf:textarea path="fullText" rows="4" cols="50"></sf:textarea>
			<sf:errors path="fullText" />
		</div>
		<div class="news-editor-filter">
			<c:if test="${not empty author_msg }">
				<c:out value="${author-msg}" />
			</c:if>
			<select name="author-id" class="author-filter">
				<c:choose>
					<c:when test="${not empty news.author}">
						<option value="${news.author.id}" selected><c:out
								value="${news.author.name}"></c:out></option>
					</c:when>
					<c:otherwise>
						<option value="0" selected><s:message
								code="filter.author" /></option>
					</c:otherwise>
				</c:choose>
				<c:forEach var="author" items="${authors}">
					<option value="${author.id}"><c:out value="${author.name}"></c:out></option>
				</c:forEach>
			</select>
			<div class="multiselect">
				<div class="selectBox">
					<select>
						<option><s:message code="filter.tag" /></option>
					</select>
					<div class="overSelect"></div>
				</div>
				<div id="checkboxes" class="checkboxes">
					<c:forEach var="tag" items="${news.tag}">
						<label><input type="checkbox" name="tag-id"
							value="${tag.id}" checked /> <c:out value="${tag.tagName}"></c:out></label>
					</c:forEach>
					<c:forEach var="tag" items="${tags}">
						<label><input type="checkbox" name="tag-id"
							value="${tag.id}" /> <c:out value="${tag.tagName}"></c:out></label>
					</c:forEach>
				</div>
			</div>
		</div>
		<sf:hidden path="id" />
		<div class="submit-wrapper">
			<input type="submit" value="<s:message code="editor.save"/>" />
		</div>
	</sf:form>

</div>