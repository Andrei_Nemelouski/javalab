<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet"
	href="/NewsManagement/admin/resources/css/normalize.css">
<link rel="stylesheet" href="/NewsManagement/admin/resources/css/font-awesome.min.css" />
<link rel="stylesheet" href="/NewsManagement/admin/resources/css/style.css">
<script src="/NewsManagement/admin/resources/js/jquery-2.1.3.min.js" type="text/javascript"></script>
<script src="/NewsManagement/admin/resources/js/main.js" type="text/javascript"></script>
<title><s:message code="header.title"/></title>
</head>
<body>
	<header>
		<tiles:insertAttribute name="header" />
	</header>
	<main>
	<aside>
		<tiles:insertAttribute name="aside" />
	</aside>
	<div class="content-block">
		<tiles:insertAttribute name="body" />
	</div>
	<div class="footer">
		<tiles:insertAttribute name="footer" />
	</div>
	</main>
</body>
</html>