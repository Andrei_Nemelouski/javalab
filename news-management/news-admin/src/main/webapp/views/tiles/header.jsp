<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<div class="header-text">
	<a href="/NewsManagement/admin"> <s:message code="header.title" />
	</a>
</div>
<div class="credentials-wrapper">
	<sec:authorize access="fullyAuthenticated"> 
		<p>
			<s:message code="header.greeting" />
			<sec:authentication property="principal.username" />			
		</p>
	</sec:authorize>
	<form method="post"
		action="/NewsManagement/admin/j_spring_security_logout">
		<input type="submit" value="<s:message code="header.logout"/>" /> <input
			type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	</form>
</div>
<div class="header-links">
	<a href="?lang=en">En</a> <a href="?lang=ru">Ru</a>
</div>

