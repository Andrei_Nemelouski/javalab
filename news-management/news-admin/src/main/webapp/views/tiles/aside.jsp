<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<div class="menu">
	<p class="list">
		<span class="fa fa-arrow-circle-o-right"></span><a href="/NewsManagement/admin/list"><s:message code="aside.list"/></a>
	</p>
	<p class="news-editor">
		<span class="fa fa-arrow-circle-o-right"></span><a href="/NewsManagement/admin/newsEditor/0"><s:message code="aside.news"/></a>
	</p>
	<p class="author-editor">
		<span class="fa fa-arrow-circle-o-right"></span><a href="/NewsManagement/admin/authorEditor"><s:message code="aside.author"/></a>
	</p>
	<p class="tag-editor">
		<span class="fa fa-arrow-circle-o-right"></span><a href="/NewsManagement/admin/tagEditor"><s:message code="aside.tag"/></a>
	</p>
</div>