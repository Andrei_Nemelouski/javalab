$(document)
		.ready(
				function() {
					var expanded = false;
					var edit_expanded = false;
					$('.delete')
							.on(
									'click',
									function() {
										var field = $('.delete-check');
										for (var loop = 0; loop < field.length; loop++) {
											if (field[loop].checked) {
												return confirm("Are you sure you want to delete selected items?");
											}
										}
										alert("At least one record should be selected! Careful with this axe, Eugene!");
										return false;

									});

					$('.selectBox').on('click', function() {
						if (!expanded) {
							$('#checkboxes').css('display', 'block');
							expanded = true;
						} else {
							$('#checkboxes').css('display', 'none');
							expanded = false;
						}
					});
					
					$('.edit').on('click', function(){
						$(document).find('.edit').css('display', 'inline-block');
						$(document).find('.edit-block').hide();
						$(document).find('input#name').attr('disabled','true');						
						$(this).hide();
						$(this).next().css('display', 'inline-block');
						$(this).prev('input#name').removeAttr('disabled');
						
						
						});
				});
