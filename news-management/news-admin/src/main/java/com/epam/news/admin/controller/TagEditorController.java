package com.epam.news.admin.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.epam.news.entity.Tag;
import com.epam.news.exceptions.ServiceException;
import com.epam.news.service.IPageService;
import com.epam.news.service.ITagService;

@Controller
@RequestMapping({ "/tagEditor" })
public class TagEditorController {

	@Autowired
	IPageService pageService;
	@Autowired
	ITagService tagService;

	@RequestMapping(method = RequestMethod.GET)
	public String showPage(Model model) throws ServiceException {
		fillModel(model);
		return "tagEditor";
	}

	@RequestMapping(method = RequestMethod.POST)
	public String save(@Valid Tag tag, BindingResult result, Model model)
			throws ServiceException {
		if (result.hasErrors()) {
			fillModel(model);
			return "tagEditor";
		}
		tagService.createOrUpdate(tag);
		fillModel(model);
		return "tagEditor";
	}

	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public String delete(long id, Model model) throws ServiceException {
		tagService.delete(id);
		fillModel(model);
		return "tagEditor";
	}
	
	private void fillModel(Model model) throws ServiceException{
		List<Tag> tags = tagService.read();
		Tag t = new Tag();
		model.addAttribute("tag", t);
		model.addAttribute("tags", tags);
	}

}
