package com.epam.news.admin.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.news.entity.Author;
import com.epam.news.entity.Tag;
import com.epam.news.entity.dto.NewsTO;
import com.epam.news.exceptions.ServiceException;
import com.epam.news.service.IAuthorService;
import com.epam.news.service.INewsService;
import com.epam.news.service.IPageService;
import com.epam.news.service.ITagService;

@Controller
@RequestMapping({ "/newsEditor" })
public class NewsEditorController {

	@Autowired
	IPageService pageService;
	@Autowired
	INewsService newsService;
	@Autowired
	IAuthorService authorService;
	@Autowired
	ITagService tagService;

	@InitBinder
	public void initBinder(HttpServletRequest request, WebDataBinder binder) {
		DateFormat df = null;
		Locale l = LocaleContextHolder.getLocale();
		if (l.toString().startsWith("ru")) {
			df = new SimpleDateFormat("dd/MM/yyyy");
		} else {
			df = new SimpleDateFormat("MM/dd/yyyy");
		}
		CustomDateEditor editor = new CustomDateEditor(df, false);
		binder.registerCustomEditor(Date.class, editor);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public String showPage(@PathVariable long id, Model model)
			throws ServiceException {
		NewsTO news = pageService.readNewsEditor(id);
		fillModel(model, news);
		return "newsEditor";
	}

	@RequestMapping(method = RequestMethod.POST)
	public String save(HttpServletRequest request, @Valid NewsTO news,
			BindingResult result, @RequestParam("author-id") long authorId,
			@RequestParam(value = "tag-id", required = false) long[] tagId,
			Model model) throws ServiceException {
		long news_id = news.getId();
		if (authorId == 0) {
			return "redirect:/newsEditor/" + news_id;
		}
		if (result.hasErrors()) {
			return "redirect:/newsEditor/" + news_id;
		}
		if (news.getId() != 0) {
			Date t = news.getModificationDate();
			news.setCreationDate(t);
		}
		long resultId = newsService.createOrUpdate(news, authorId, tagId);
		return "redirect:/article/" + resultId;
	}

	public void fillModel(Model model, NewsTO news) throws ServiceException {
		List<Tag> tags = tagService.read();
		List<Author> authors = authorService.read();
		if (news.getId() != 0) {
			authors.remove(news.getAuthor());
		}
		if (news.getTag() != null) {
			tags.removeAll(news.getTag());
		}
		model.addAttribute("news", news);
		model.addAttribute("authors", authors);
		model.addAttribute("tags", tags);

	}

}
