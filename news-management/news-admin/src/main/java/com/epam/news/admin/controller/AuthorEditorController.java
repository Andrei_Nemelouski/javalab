package com.epam.news.admin.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.epam.news.entity.Author;
import com.epam.news.exceptions.ServiceException;
import com.epam.news.service.IAuthorService;
import com.epam.news.service.IPageService;

@Controller
@RequestMapping({ "/authorEditor" })
public class AuthorEditorController {

	@Autowired
	IPageService pageService;
	@Autowired
	IAuthorService authorService;

	@RequestMapping(method = RequestMethod.GET)
	public String showPage(Model model) throws ServiceException {
		fillModel(model);
		return "authorEditor";
	}

	@RequestMapping(method = RequestMethod.POST)
	public String save(@Valid Author author, BindingResult result, Model model)
			throws ServiceException {
		if (result.hasErrors()) {
			fillModel(model);
			return "authorEditor";
		}
		authorService.createOrUpdate(author);
		fillModel(model);
		return "authorEditor";
	}

	@RequestMapping(value = "/expire", method = RequestMethod.GET)
	public String expire(int id, Model model) throws ServiceException {
		authorService.setExpired(id);
		fillModel(model);
		return "authorEditor";
	}

	private void fillModel(Model model) throws ServiceException {
		List<Author> authors = authorService.read();
		model.addAttribute("authors", authors);
		model.addAttribute("author", new Author());
	}

}
