package com.epam.news.admin.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.epam.news.admin.utils.FilterUtils;
import com.epam.news.entity.Comment;
import com.epam.news.entity.representation.Filter;
import com.epam.news.entity.representation.Page;
import com.epam.news.exceptions.ServiceException;
import com.epam.news.service.ICommentService;
import com.epam.news.service.INewsService;
import com.epam.news.service.IPageService;

@Controller
@RequestMapping({ "/article" })
public class NewsViewController {

	@Autowired
	IPageService pageService;
	@Autowired
	INewsService newsService;
	@Autowired
	ICommentService commentService;

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public String showHomepage(HttpServletRequest request,
			@PathVariable int id, Model model) throws ServiceException {
		HttpSession session = request.getSession();
		Filter filter = FilterUtils.populateFilter(session);
		Page page = pageService.readNewsPage(id, filter);
		fillModel(model, page);
		populateNextPrevId(model, filter, id);
		session.setAttribute("filter", filter);
		return "view";
	}

	@RequestMapping(method = RequestMethod.POST)
	public String addComment(HttpServletRequest request,
			@Valid Comment comment, BindingResult result, Model model)
			throws ServiceException {
		long news_id = comment.getNewsId();
		HttpSession session = request.getSession();
		Filter filter = FilterUtils.populateFilter(session);
		if (result.hasErrors()) {
			Page page = pageService.readNewsPage(news_id, filter);
			fillModel(model, page);
			populateNextPrevId(model, filter, news_id);
			return "view";
		}
		comment.setCreationDate(new Date(System.currentTimeMillis()));
		commentService.create(comment);
		Page page = pageService.readNewsPage(news_id, filter);
		fillModel(model, page);
		populateNextPrevId(model, filter, news_id);
		return "redirect:/article/" + news_id;

	}

	@RequestMapping(value = { "/previous/{id}", "/next/{id}" }, method = RequestMethod.GET)
	public String nextPrevious(HttpServletRequest request,
			@PathVariable int id, Model model) throws ServiceException {
		HttpSession session = request.getSession();
		Filter filter = FilterUtils.populateFilter(session);
		Page page = pageService.readNewsPage(id, filter);
		fillModel(model, page);
		populateNextPrevId(model, filter, id);
		session.setAttribute("filter", filter);
		return "view";

	}

	@RequestMapping(value = "/comment/delete", method = RequestMethod.POST)
	public String deleteComment(HttpServletRequest request, Comment comment,
			Model model) throws ServiceException {
		long commentId = comment.getId();
		long newsId = comment.getNewsId();
		commentService.delete(commentId);
		HttpSession session = request.getSession();
		Filter filter = FilterUtils.populateFilter(session);
		Page page = pageService.readNewsPage(newsId, filter);
		fillModel(model, page);
		populateNextPrevId(model, filter, newsId);
		session.setAttribute("filter", filter);
		return "redirect:/article/" + newsId;
	}

	private void fillModel(Model model, Page page) {
		model.addAttribute("page", page);
		model.addAttribute("comment", new Comment());
	}

	private void populateNextPrevId(Model model, Filter filter, long id)
			throws ServiceException {
		long[] nextPrev = newsService.readNextPrev(id, filter);
		model.addAttribute("nextNewsId", nextPrev[1]);
		model.addAttribute("previousNewsId", nextPrev[0]);
	}

}
