package com.epam.news.admin.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.news.admin.utils.FilterUtils;
import com.epam.news.entity.Author;
import com.epam.news.entity.Tag;
import com.epam.news.entity.representation.Filter;
import com.epam.news.entity.representation.Page;
import com.epam.news.exceptions.ServiceException;
import com.epam.news.service.IAuthorService;
import com.epam.news.service.INewsService;
import com.epam.news.service.IPageService;
import com.epam.news.service.ITagService;

@Controller
@RequestMapping({ "/list" })
public class NewsListController {

	@Autowired
	IPageService pageService;
	@Autowired
	INewsService newsService;
	@Autowired
	IAuthorService authorService;
	@Autowired
	ITagService tagService;
	private final int DEFAULT_NEWS_PER_PAGE = 3;
	private final int DEFAULT_PAGE_NUMBER = 1;

	@RequestMapping(method = RequestMethod.GET)
	public String showHomepage(HttpServletRequest request, Model model)
			throws ServiceException {
		HttpSession session = request.getSession();
		Filter filter = FilterUtils.populateFilter(session);
		Page page = pageService.readNewsList(DEFAULT_NEWS_PER_PAGE,
				DEFAULT_PAGE_NUMBER, filter);
		fillModel(model, page, filter);
		session.setAttribute("filter", filter);
		return "home";
	}

	@RequestMapping(value = "/{pageNumber}", method = RequestMethod.GET)
	public String nextPage(@PathVariable int pageNumber,
			HttpServletRequest request, Model model) throws ServiceException {
		HttpSession session = request.getSession();
		Filter filter = FilterUtils.populateFilter(session);
		Page page = pageService.readNewsList(DEFAULT_NEWS_PER_PAGE, pageNumber,
				filter);
		fillModel(model, page, filter);
		session.setAttribute("filter", filter);
		return "home";
	}

	@RequestMapping(value = "/filter", method = RequestMethod.POST)
	public String filter(
			@RequestParam(value = "authorId", required = false, defaultValue = "0") long authorId,
			@RequestParam(value = "tagId", required = false) long[] tagId,
			HttpServletRequest request, Model model) throws ServiceException {
		HttpSession session = request.getSession();
		Filter filter = FilterUtils.populateFilter(session, authorId, tagId);
		session.setAttribute("filter", filter);
		return "redirect:/list";
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public String delete(@RequestParam("news-id") long[] newsId,
			HttpServletRequest request, Model model) throws ServiceException {
		if (newsId.length != 0) {
			newsService.delete(newsId);
		}
		return "redirect:/list";
	}

	private void fillModel(Model model, Page page, Filter filter)
			throws ServiceException {
		List<Tag> tags = tagService.read();
		List<Author> authors = authorService.read();
		if (filter.getAuthor() != null) {
			authors.remove(filter.getAuthor());
		}
		if (filter.getTag() != null) {
			tags.removeAll(filter.getTag());
		}
		model.addAttribute("page", page);
		model.addAttribute("authors", authors);
		model.addAttribute("tags", tags);
	}

}
